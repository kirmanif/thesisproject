from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import accuracy_score

plt.rc("font", size=14)

filename = "data/featuresYP.csv"
df = pd.read_csv(filename, header=None)
X = df.iloc[:, 0:36]
y = df.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))

X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]
X_train = X_train.astype('float')
y_train = y_train.astype('int')
print(X_train.shape)

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.iloc[:, col_test1[0:6]]
y_testXG = df.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.iloc[:, col_test2[0:6]]
y_testRFE = df.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.iloc[:, col_test3[0:6]]
y_testUN = df.iloc[:, 36]

penalty = ['l1', 'l2']

# solver = ['newton-cg', 'lbfgs', 'liblinear']

C = [35]
# C = list(range(30, 40, 1))

class_weight = ['balanced']

# multi_class = ['ovr', 'multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight)

# Logistic Regression Model Fitting
X_train1 = df_train.iloc[:, col_test3[0:6]]

CV_rfc = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc.fit(X_train1, y_train)
print(CV_rfc.best_params_)
# y_pred = CV_rfc.predict(X_testUN)
y_pred = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10)
# View best hyperparameters
print('Best Penalty:', CV_rfc.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
    CV_rfc.score(X_testUN, y_testUN)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, y_pred)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, y_pred))
print('Overall AUC : ', roc_auc_score(y_testUN, y_pred))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finish running logistic model (Univariate FS)\n\n\n")

# Logistic Regression Model Fitting
X_trainRFE = df_train.iloc[:, col_test2[0:6]]
CV_rfc2 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc2.fit(X_trainRFE, y_train)
print(CV_rfc2.best_params_)
# y_predRFE = CV_rfc2.predict(X_testRFE)
y_predRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10)
print('Best Penalty:', CV_rfc2.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc2.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
    CV_rfc2.score(X_testRFE, y_testRFE)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, y_predRFE))
print('Overall AUC : ', roc_auc_score(y_testRFE, y_predRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finished running logistic model (RFE)\n\n\n")

X_trainXG = df_train.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

# train model
# selection_model = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
CV_rfc3 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc3.fit(X_trainXG, y_train)
print(CV_rfc3.best_params_)

print('Best Penalty:', CV_rfc3.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc3.best_estimator_.get_params()['C'])
# eval model
# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Overall AUC : ', roc_auc_score(y_testXG, y_pred))
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_testXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Logistic Regression trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/logisticRegression/logisticRegressionROCFullTestUnbalYP')
# plt.show()

X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]
X_train = X_train.astype('float')
y_train = y_train.astype('int')
print(X_train.shape)

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.iloc[:, col_test1[0:6]]
y_testXG = df.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.iloc[:, col_test2[0:6]]
y_testRFE = df.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.iloc[:, col_test3[0:6]]
y_testUN = df.iloc[:, 36]

penalty = ['l1', 'l2']

# solver = ['newton-cg', 'lbfgs', 'liblinear']

C = [35]
# C = list(range(30, 40, 1))

class_weight = ['balanced']

# multi_class = ['ovr', 'multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight)

# Logistic Regression Model Fitting
X_train1 = df_train_balanced.iloc[:, col_test3[0:6]]

CV_rfc = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc.fit(X_train1, y_train)
print(CV_rfc.best_params_)
# y_pred = CV_rfc.predict(X_testUN)
y_pred = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10)
# View best hyperparameters
print('Best Penalty:', CV_rfc.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
    CV_rfc.score(X_testUN, y_testUN)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, y_pred)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, y_pred))
print('Overall AUC : ', roc_auc_score(y_testUN, y_pred))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finish running logistic model (Univariate FS)\n\n\n")

# Logistic Regression Model Fitting
X_trainRFE = df_train_balanced.iloc[:, col_test2[0:6]]
CV_rfc2 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc2.fit(X_trainRFE, y_train)
print(CV_rfc2.best_params_)
# y_predRFE = CV_rfc2.predict(X_testRFE)
y_predRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10)
print('Best Penalty:', CV_rfc2.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc2.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
    CV_rfc2.score(X_testRFE, y_testRFE)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, y_predRFE))
print('Overall AUC : ', roc_auc_score(y_testRFE, y_predRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finished running logistic model (RFE)\n\n\n")

X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

# train model
# selection_model = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
CV_rfc3 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc3.fit(X_trainXG, y_train)
print(CV_rfc3.best_params_)

print('Best Penalty:', CV_rfc3.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc3.best_estimator_.get_params()['C'])
# eval model
# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Overall AUC : ', roc_auc_score(y_testXG, y_pred))
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_testXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Logistic Regression trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/logisticRegression/logisticRegressionROCFullTestYP')
# plt.show()

X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]
X_train = X_train.astype('float')
y_train = y_train.astype('int')
print(X_train.shape)

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.iloc[:, col_test1[0:6]]
y_testXG = df_test.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.iloc[:, col_test2[0:6]]
y_testRFE = df_test.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.iloc[:, col_test3[0:6]]
y_testUN = df_test.iloc[:, 36]

penalty = ['l1', 'l2']

# solver = ['newton-cg', 'lbfgs', 'liblinear']

C = [35]
# C = list(range(30, 40, 1))

class_weight = ['balanced']

# multi_class = ['ovr', 'multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight)

# Logistic Regression Model Fitting
X_train1 = df_train_balanced.iloc[:, col_test3[0:6]]

CV_rfc = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc.fit(X_train1, y_train)
print(CV_rfc.best_params_)
# y_pred = CV_rfc.predict(X_testUN)
y_pred = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10)
# View best hyperparameters
print('Best Penalty:', CV_rfc.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
    CV_rfc.score(X_testUN, y_testUN)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, y_pred)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, y_pred))
print('Overall AUC : ', roc_auc_score(y_testUN, y_pred))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finish running logistic model (Univariate FS)\n\n\n")

# Logistic Regression Model Fitting
X_trainRFE = df_train_balanced.iloc[:, col_test2[0:6]]
CV_rfc2 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc2.fit(X_trainRFE, y_train)
print(CV_rfc2.best_params_)
# y_predRFE = CV_rfc2.predict(X_testRFE)
y_predRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10)
print('Best Penalty:', CV_rfc2.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc2.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
    CV_rfc2.score(X_testRFE, y_testRFE)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, y_predRFE))
print('Overall AUC : ', roc_auc_score(y_testRFE, y_predRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finished running logistic model (RFE)\n\n\n")

X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

# train model
# selection_model = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
CV_rfc3 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc3.fit(X_trainXG, y_train)
print(CV_rfc3.best_params_)

print('Best Penalty:', CV_rfc3.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc3.best_estimator_.get_params()['C'])
# eval model
# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Overall AUC : ', roc_auc_score(y_testXG, y_pred))
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_testXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Logistic Regression trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/logisticRegression/logisticRegressionROC20TestYP')
# plt.show()

X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]
X_train = X_train.astype('float')
y_train = y_train.astype('int')
print(X_train.shape)

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.iloc[:, col_test1[0:6]]
y_testXG = df_test.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.iloc[:, col_test2[0:6]]
y_testRFE = df_test.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.iloc[:, col_test3[0:6]]
y_testUN = df_test.iloc[:, 36]

penalty = ['l1', 'l2']

# solver = ['newton-cg', 'lbfgs', 'liblinear']

C = [35]
# C = list(range(30, 40, 1))

class_weight = ['balanced']

# multi_class = ['ovr', 'multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight)

# Logistic Regression Model Fitting
X_train1 = df_train.iloc[:, col_test3[0:6]]

CV_rfc = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc.fit(X_train1, y_train)
print(CV_rfc.best_params_)
# y_pred = CV_rfc.predict(X_testUN)
y_pred = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10)
# View best hyperparameters
print('Best Penalty:', CV_rfc.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
    CV_rfc.score(X_testUN, y_testUN)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, y_pred)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, y_pred))
print('Overall AUC : ', roc_auc_score(y_testUN, y_pred))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finish running logistic model (Univariate FS)\n\n\n")

# Logistic Regression Model Fitting
X_trainRFE = df_train.iloc[:, col_test2[0:6]]
CV_rfc2 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc2.fit(X_trainRFE, y_train)
print(CV_rfc2.best_params_)
# y_predRFE = CV_rfc2.predict(X_testRFE)
y_predRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10)
print('Best Penalty:', CV_rfc2.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc2.best_estimator_.get_params()['C'])
print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
    CV_rfc2.score(X_testRFE, y_testRFE)))

from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, y_predRFE))
print('Overall AUC : ', roc_auc_score(y_testRFE, y_predRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadMetric : ', sensitivity * specificity)
print("Finished running logistic model (RFE)\n\n\n")

X_trainXG = df_train.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

# train model
# selection_model = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
CV_rfc3 = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
CV_rfc3.fit(X_trainXG, y_train)
print(CV_rfc3.best_params_)

print('Best Penalty:', CV_rfc3.best_estimator_.get_params()['penalty'])
print('Best C:', CV_rfc3.best_estimator_.get_params()['C'])
# eval model
# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Overall AUC : ', roc_auc_score(y_testXG, y_pred))
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_testXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=10, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=10, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=10, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Logistic Regression trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/logisticRegression/logisticRegressionROC20TestUnbalYP')
# plt.show()





