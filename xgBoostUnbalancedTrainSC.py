import math as mt

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from matplotlib import pyplot
from numpy import sort
from sklearn.feature_selection import SelectFromModel, f_classif
from sklearn.feature_selection import SelectKBest, chi2, RFE
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_predict
from xgboost import XGBClassifier
from xgboost import plot_importance

plt.rc("font", size=14)

filename = "data/featuresYPYS_NotScaled.csv"
dfFeat1 = pd.read_csv(filename, header=None)

df_OneFeat1 = dfFeat1.loc[dfFeat1[36] == 1]
df_MinusOne1Feat1 = dfFeat1.loc[dfFeat1[36] == -1]
df_train_OneFeat1 = df_OneFeat1.sample(n=mt.floor(len(df_OneFeat1.index) / 1))
df_train_MinusOneFeat1 = df_MinusOne1Feat1.sample(
    n=mt.floor(len(df_train_OneFeat1.index) * (len(df_MinusOne1Feat1.index) / len(df_OneFeat1.index))))
df_train_unbalancedFeat1 = df_train_OneFeat1.append(df_train_MinusOneFeat1)

# filename = "data/featuresYP.csv"
# dfFeat2 = pd.read_csv(filename, header=None)

# df_OneFeat2 = dfFeat2.loc[dfFeat2[36] == 1]
# df_MinusOne1Feat2 = dfFeat2.loc[dfFeat2[36] == -1]
# df_train_OneFeat2 = df_OneFeat2.sample(n=mt.floor(len(df_OneFeat2.index) / 1))
# df_train_MinusOneFeat2 = df_MinusOne1Feat2.sample(
#    n=mt.floor(len(df_train_OneFeat2.index) * (len(df_MinusOne1Feat2.index) / len(df_OneFeat2.index))))
# df_train_unbalancedFeat2 = df_train_OneFeat2.append(df_train_MinusOneFeat2)

filename1 = "data/featuresYS_NotScaled.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYP_NotScaled.csv"
df2 = pd.read_csv(filename2, header=None)
filename3 = "data/featuresBS50_NotScaled.csv"
df3 = pd.read_csv(filename3, header=None)
print(df1.shape)
print(df2.shape)
print(df3.shape)

df1[36] = df1[36].map({1: 1, -1: 0})
df2[36] = df2[36].map({1: 1, -1: 0})
df3[36] = df3[36].map({1: 1, -1: 0})

df_One = df1.loc[df1[36] == 1]
df_Zero1 = df1.loc[df1[36] == 0]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 1.25))
print(len(df_train_One))
df_train_MinusOne = df_Zero1.sample(n=mt.floor(len(df_train_One.index) * (len(df_Zero1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat1 = df_train_unbalancedFeat1.iloc[:, 0:36]
y_trainFeat1 = df_train_unbalancedFeat1.iloc[:, 36]

X_trainFeat1 = X_trainFeat1.astype('float')
y_trainFeat1 = y_trainFeat1.astype('int')

# X_trainFeat2 = df_train_unbalancedFeat2.iloc[:, 0:36]
# y_trainFeat2 = df_train_unbalancedFeat2.iloc[:, 36]

# X_trainFeat2 = X_trainFeat2.astype('float')
# y_trainFeat2 = y_trainFeat2.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test1 = df1.iloc[:, 0:36]
y_test1 = df1.iloc[:, 36]

X_test2 = df2.iloc[:, 0:36]
y_test2 = df2.iloc[:, 36]

X_test3 = df3.iloc[:, 0:36]
y_test3 = df3.iloc[:, 36]

# (len(df_Zero1.index) / len(df_One.index))

max_depth = [3]
learning_rate = [0.08]
n_estimators = [236]
silent = ['True']
objective = ['binary:logistic']
booster = ['gbtree']
n_jobs = [-1]
nthread = [4]
gamma = [75]
min_child_weight = [3]
max_delta_step = [5]
subsample = [0.5]
colsample_bytree = [1]
colsample_bylevel = [1]
reg_alpha = [1]
reg_lambda = [1]
scale_pos_weight = [len(df_Zero1.index) / len(df_One.index)]
base_score = [0.25]
random_state = [7777]
param_grid = dict(max_depth=max_depth, learning_rate=learning_rate, n_estimators=n_estimators, silent=silent,
                  objective=objective, booster=booster, n_jobs=n_jobs, nthread=nthread, gamma=gamma,
                  min_child_weight=min_child_weight, max_delta_step=max_delta_step, subsample=subsample,
                  colsample_bytree=colsample_bytree, colsample_bylevel=colsample_bylevel, reg_alpha=reg_alpha,
                  reg_lambda=reg_lambda, scale_pos_weight=scale_pos_weight, base_score=base_score,
                  random_state=random_state)

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')
#
# for b in range(6, 7):
#     model = LogisticRegression()
#     rfe = RFE(model, b)
#     fitRFE = rfe.fit(X_trainFeat1, y_trainFeat1)
#     print("Num Features: %d" % (fitRFE.n_features_,))
#     print("Selected Features: %s" % (fitRFE.support_,))
#     print("Feature Ranking: %s" % (fitRFE.ranking_,))
#     arrRFE = fitRFE.get_support(indices=False)
#     # print(arr)
#     featuresRFE = fitRFE.transform(X_trainFeat1)
#     # print(features[0:5,:])
#     col_test = arrRFE.argsort()[-b:][::-1]
#     print(col_test)
#     X_test1 = df1.loc[:, col_test[0:b]].values
#     X_test1 = pd.DataFrame(X_test1)
#     y_test1 = df1.loc[:, 36].values
#
#     X_test2 = df2.loc[:, col_test[0:b]].values
#     X_test2 = pd.DataFrame(X_test2)
#     y_test2 = df2.loc[:, 36].values
#
#     X_test3 = df3.loc[:, col_test[0:b]].values
#     X_test3 = pd.DataFrame(X_test3)
#     y_test3 = df3.loc[:, 36].values
#
#     X_train1 = df_train_unbalanced.loc[:, col_test[0:b]].values
#     X_train1 = pd.DataFrame(X_train1)
#
#     trained_model = XGBClassifier()
#
#     CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
#     CV_rfc.fit(X_train1, y_train)
#     print(CV_rfc.best_params_)
#     predictions1 = cross_val_predict(CV_rfc, X_test1, y_test1, cv=5)
#
#     # Create Confusion Matrix
#     from sklearn.metrics import confusion_matrix
#     from sklearn.metrics import classification_report
#
#     confusion_matrix = confusion_matrix(y_test1, predictions1)
#     print(confusion_matrix)
#     # Compute precision, recall, F-measure and support
#     print(classification_report(y_test1, predictions1))
#     print('Overall AUC:', roc_auc_score(y_test1, predictions1))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     print('specificity : ', specificity)
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('sensitivity : ', sensitivity)
#     print('fawadmetric : ', sensitivity * specificity * 100.0)
#
#     # predictions2 = cross_val_predict(CV_rfc, X_test2, y_test2, cv=5)
#     predictions2 = CV_rfc.predict(X_test2)
#
#     # Create Confusion Matrix
#     from sklearn.metrics import confusion_matrix
#     from sklearn.metrics import classification_report
#
#     confusion_matrix = confusion_matrix(y_test2, predictions2)
#     print(confusion_matrix)
#     # Compute precision, recall, F-measure and support
#     print(classification_report(y_test2, predictions2))
#     print('Overall AUC:', roc_auc_score(y_test2, predictions2))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     print('specificity : ', specificity)
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('sensitivity : ', sensitivity)
#     print('fawadmetric : ', sensitivity * specificity * 100.0)
#
#     # predictions3 = cross_val_predict(CV_rfc, X_test3, y_test3, cv=5)
#     predictions3 = CV_rfc.predict(X_test3)
#
#     # Create Confusion Matrix
#     from sklearn.metrics import confusion_matrix
#     from sklearn.metrics import classification_report
#
#     confusion_matrix = confusion_matrix(y_test3, predictions3)
#     print(confusion_matrix)
#     # Compute precision, recall, F-measure and support
#     print(classification_report(y_test3, predictions3))
#     print('Overall AUC:', roc_auc_score(y_test3, predictions3))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     print('specificity : ', specificity)
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('sensitivity : ', sensitivity)
#     print('fawadmetric : ', sensitivity * specificity * 100.0)
#     # ROC Curve
#     xf1_roc_auc = roc_auc_score(y_test1, predictions1)
#     xf2_roc_auc = roc_auc_score(y_test2, predictions2)
#     xf3_roc_auc = roc_auc_score(y_test3, predictions3)
#     fpr1, tpr1, thresholds1 = roc_curve(y_test1,
#                                         cross_val_predict(CV_rfc, X_test1, y_test1, method='predict_proba', cv=5)[:, 1])
#     fpr2, tpr2, thresholds2 = roc_curve(y_test2,
#                                         CV_rfc.predict_proba(X_test2)[:, 1])
#     fpr3, tpr3, thresholds3 = roc_curve(y_test3,
#                                         CV_rfc.predict_proba(X_test3)[:, 1])
#     plt.figure()
#     plt.plot(fpr1, tpr1, linestyle=':', color='green', label='SC (area = %0.2f)' % xf1_roc_auc)
#     plt.plot(fpr2, tpr2, linestyle='-.', color='purple', label='YP (area = %0.2f)' % xf2_roc_auc)
#     plt.plot(fpr3, tpr3, linestyle='--', color='blue', label='BS (area = %0.2f)' % xf3_roc_auc)
#     plt.plot([0, 1], [0, 1], 'r--')
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     plt.title('XGBoost trained on SC using RFE')
#     plt.legend(loc="lower right")
#     plt.savefig('Pictures/xgBoost/test2/xgBoostROCRFESC%d' % b)
#     print("Finished running XGBoost for feature no. (RFE) %d\n\n\n" % b)
#
# for a in range(6, 7):
#     print("For feature no. (Univariate FS) %d" % a)
#
#     # feature selection Univariate
#     X_new = SelectKBest(f_classif, k=a)
#     fit = X_new.fit(X_trainFeat1, y_trainFeat1)
#     np.set_printoptions(precision=3)
#     arr = fit.scores_
#     # print(arr)
#     features = fit.transform(X_trainFeat1)
#     # print(features[0:5,:])
#     col_test = arr.argsort()[-a:][::-1]
#     print(col_test)
#     X_test1 = df1.ix[:, col_test[0:a]].values
#     X_test1 = pd.DataFrame(X_test1)
#     y_test1 = df1.ix[:, 36].values
#
#     X_test2 = df2.ix[:, col_test[0:a]].values
#     X_test2 = pd.DataFrame(X_test2)
#     y_test2 = df2.ix[:, 36].values
#
#     X_test3 = df3.ix[:, col_test[0:a]].values
#     X_test3 = pd.DataFrame(X_test3)
#     y_test3 = df3.ix[:, 36].values
#
#     X_train1 = df_train_unbalanced.ix[:, col_test[0:a]].values
#     X_train1 = pd.DataFrame(X_train1)
#
#     dTrain = xgb.DMatrix(X_train1, label=y_train)
#
#     selection_model = XGBClassifier()
#     CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
#     CV_rfc.fit(X_train1, y_train)
#     # eval model
#     predictions1 = cross_val_predict(CV_rfc, X_test1, y_test1, cv=5)
#
#     # Create Confusion Matrix
#     from sklearn.metrics import confusion_matrix
#     from sklearn.metrics import classification_report
#
#     confusion_matrix = confusion_matrix(y_test1, predictions1)
#     print(confusion_matrix)
#     # Compute precision, recall, F-measure and support
#     print(classification_report(y_test1, predictions1))
#     print('Overall AUC:', roc_auc_score(y_test1, predictions1))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     print('specificity : ', specificity)
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('sensitivity : ', sensitivity)
#     print('fawadmetric : ', sensitivity * specificity * 100.0)
#
#     # predictions2 = cross_val_predict(CV_rfc, X_test2, y_test2, cv=5)
#     predictions2 = CV_rfc.predict(X_test2)
#
#     # Create Confusion Matrix
#     from sklearn.metrics import confusion_matrix
#     from sklearn.metrics import classification_report
#
#     confusion_matrix = confusion_matrix(y_test2, predictions2)
#     print(confusion_matrix)
#     # Compute precision, recall, F-measure and support
#     print(classification_report(y_test2, predictions2))
#     print('Overall AUC:', roc_auc_score(y_test2, predictions2))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     print('specificity : ', specificity)
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('sensitivity : ', sensitivity)
#     print('fawadmetric : ', sensitivity * specificity * 100.0)
#
#     # predictions3 = cross_val_predict(CV_rfc, X_test3, y_test3, cv=5)
#     predictions3 = CV_rfc.predict(X_test3)
#
#     # Create Confusion Matrix
#     from sklearn.metrics import confusion_matrix
#     from sklearn.metrics import classification_report
#
#     confusion_matrix = confusion_matrix(y_test3, predictions3)
#     print(confusion_matrix)
#     # Compute precision, recall, F-measure and support
#     print(classification_report(y_test3, predictions3))
#     print('Overall AUC:', roc_auc_score(y_test3, predictions3))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     print('specificity : ', specificity)
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('sensitivity : ', sensitivity)
#     print('fawadmetric : ', sensitivity * specificity * 100.0)
#     # ROC Curve
#     xf1_roc_auc = roc_auc_score(y_test1, predictions1)
#     xf2_roc_auc = roc_auc_score(y_test2, predictions2)
#     xf3_roc_auc = roc_auc_score(y_test3, predictions3)
#     fpr1, tpr1, thresholds1 = roc_curve(y_test1,
#                                         cross_val_predict(CV_rfc, X_test1, y_test1, method='predict_proba', cv=5)[:, 1])
#     fpr2, tpr2, thresholds2 = roc_curve(y_test2,
#                                         CV_rfc.predict_proba(X_test2)[:, 1])
#     fpr3, tpr3, thresholds3 = roc_curve(y_test3,
#                                         CV_rfc.predict_proba(X_test3)[:, 1])
#     plt.figure()
#     plt.plot(fpr1, tpr1, linestyle=':', color='green', label='SC (area = %0.2f)' % xf1_roc_auc)
#     plt.plot(fpr2, tpr2, linestyle='-.', color='purple', label='YP (area = %0.2f)' % xf2_roc_auc)
#     plt.plot(fpr3, tpr3, linestyle='--', color='blue', label='BS (area = %0.2f)' % xf3_roc_auc)
#     plt.plot([0, 1], [0, 1], 'r--')
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     plt.title('XGBoost trained on SC using Univariate')
#     plt.legend(loc="lower right")
#     plt.savefig('Pictures/xgBoost/test2/xgBoostROCUNSC%d' % a)
#     print("Finished running xgBoost for feature no. (Univariate FS) %d\n\n\n" % a)

# # fit model no training data
# model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                       booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                       subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                       scale_pos_weight=1, base_score=0.25,
#                       random_state=7777, seed=None, missing=None)
# model.fit(X_trainFeat1, y_trainFeat1)
# # feature importance
# featureImportance1 = model.feature_importances_
# print(featureImportance1)
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# plot_importance(model)
# thresholds = sort(model.feature_importances_)
# thresholds[::-1].sort()
# # print(thresholds)
# for thresh in thresholds:
#     # select features using threshold
#     selection = SelectFromModel(model, threshold=thresh, prefit=True)
#     select_X_train = selection.transform(X_train)
#     dTrain = xgb.DMatrix(select_X_train, label=y_train)
#     clf = XGBClassifier()
#
#     clf_grid = GridSearchCV(estimator=clf, param_grid=param_grid, scoring='recall', cv=5)
#     clf_grid.fit(select_X_train, y_train)
#     # xgb_param = clf_grid.best_params_
#     #
#     # # xgb_param = clf.get_xgb_params()
#     # print(xgb_param)
#     #
#     # # selection_model.fit(select_X_train, y_train)
#     # cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#     #                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#     #                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#     # eval model
#     select_X_test1 = selection.transform(X_test1)
#     # print('Best number of trees = {}'.format(cvresult.shape[0]))
#     # clf.set_params(n_estimators=cvresult.shape[0])
#     # print('Fit on the trainingsdata')
#     # # clf.fit(select_X_train, y_train, eval_metric='auc')
#     # print('Predict the probabilities based on features in the test set')
#     # # pred = clf.predict_proba(select_X_test, ntree_limit=cvresult.shape[0])[:, 1]
#     # pred1 = clf_grid.predict(select_X_test1)
#     #
#     # predictions1 = [round(value) for value in pred1]
#     # accuracy = accuracy_score(y_test1, predictions1)
#     # from sklearn.metrics import confusion_matrix
#     #
#     # confusion_matrix = confusion_matrix(y_test1, predictions1)
#     # print(confusion_matrix)
#     # specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     # sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     # print('Overall AUC:', roc_auc_score(y_test1, predictions1))
#     # print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
#     #     thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
#     #     sensitivity * specificity * 100.0))
#
#     select_X_test2 = selection.transform(X_test2)
#     # print('Best number of trees = {}'.format(cvresult.shape[0]))
#     # clf.set_params(n_estimators=cvresult.shape[0])
#     print('Fit on the trainingsdata')
#     # clf.fit(select_X_train, y_train, eval_metric='auc')
#     print('Predict the probabilities based on features in the test set')
#     # pred = clf.predict_proba(select_X_test, ntree_limit=cvresult.shape[0])[:, 1]
#     pred2 = clf_grid.predict(select_X_test2)
#
#     predictions2 = [round(value) for value in pred2]
#     accuracy = accuracy_score(y_test2, predictions2)
#     from sklearn.metrics import confusion_matrix
#
#     confusion_matrix = confusion_matrix(y_test2, predictions2)
#     print(confusion_matrix)
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print('Overall AUC:', roc_auc_score(y_test2, predictions2))
#     print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
#         thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
#         sensitivity * specificity * 100.0))
#     # # ROC Curve
#     # xgb1_roc_auc = roc_auc_score(y_test1, predictions1)
#     # xgb2_roc_auc = roc_auc_score(y_test2, predictions2)
#     # fpr1, tpr1, thresholds1 = roc_curve(y_test1, clf_grid.predict_proba(select_X_test1)[:, 1])
#     # fpr2, tpr2, thresholds2 = roc_curve(y_test2, clf_grid.predict_proba(select_X_test2)[:, 1])
#     # plt.figure()
#     # plt.plot(fpr1, tpr1, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xgb1_roc_auc)
#     # plt.plot(fpr2, tpr2, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xgb2_roc_auc)
#     # plt.plot([0, 1], [0, 1], 'r--')
#     # plt.xlim([0.0, 1.0])
#     # plt.ylim([0.0, 1.05])
#     # plt.xlabel('False Positive Rate')
#     # plt.ylabel('True Positive Rate')
#     # plt.title('XGBoost trained on SC using XGBoost')
#     # plt.legend(loc="lower right")
#     # plt.savefig('Pictures/xgBoost/test/xgBoostROCXGBoostSC%d' % select_X_train.shape[1])
#     # # plt.show()

# col_YPSC = [35, 1, 6, 9, 14, 0]
col_YPSC = [35, 1, 6, 12, 9, 14]
# X_testXG = df.iloc[:, col_test1[0:6]]
# y_testXG = df.iloc[:, 36]

X_trainXG = df_train_unbalanced.ix[:, col_YPSC[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

X_testXG1 = df1.iloc[:, col_YPSC[0:6]].values
X_testXG1 = pd.DataFrame(X_testXG1)
y_testXG1 = df1.iloc[:, 36]
X_testXG2 = df2.iloc[:, col_YPSC[0:6]].values
X_testXG2 = pd.DataFrame(X_testXG2)
y_testXG2 = df2.iloc[:, 36]
X_testXG3 = df3.iloc[:, col_YPSC[0:6]].values
X_testXG3 = pd.DataFrame(X_testXG3)
y_testXG3 = df3.iloc[:, 36]

# # train model
selection_model = XGBClassifier()
CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc.fit(X_trainXG, y_train)
print(CV_rfc.best_params_)
# # eval model
# select_X_test1 = selection.transform(X_testXG1)
y_pred1 = cross_val_predict(CV_rfc, X_testXG1, y_testXG1, cv=5)
# y_pred1 = CV_rfc.predict(X_testXG1)
print('Overall AUC:', roc_auc_score(y_testXG1, y_pred1))
accuracy = accuracy_score(y_testXG1, y_pred1)
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testXG1, y_pred1)
print(confusion_matrix)
print(classification_report(y_testXG1, y_pred1))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# select_X_test2 = selection.transform(X_testXG2)
# y_pred2 = cross_val_predict(CV_rfc, X_testXG2, y_testXG2, cv=5)
y_pred2 = CV_rfc.predict(X_testXG2)
# predictions = [round(value) for value in y_pred2]
print('Overall AUC:', roc_auc_score(y_testXG2, y_pred2))
accuracy = accuracy_score(y_testXG2, y_pred2)
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testXG2, y_pred2)
print(confusion_matrix)
print(classification_report(y_testXG2, y_pred2))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# y_pred3 = cross_val_predict(CV_rfc, X_testXG3, y_testXG3, cv=5)
y_pred3 = CV_rfc.predict(X_testXG3)
# predictions = [round(value) for value in y_pred3]
print('Overall AUC:', roc_auc_score(y_testXG3, y_pred3))
accuracy = accuracy_score(y_testXG3, y_pred3)
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testXG3, y_pred3)
print(confusion_matrix)
print(classification_report(y_testXG3, y_pred3))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))
print("Finish running logistic model for feature no. (XGBoost) %d\n\n\n" % X_trainXG.shape[1])
# ROC Curve
logit1_roc_auc = roc_auc_score(y_testXG1, y_pred1)
logit2_roc_auc = roc_auc_score(y_testXG2, y_pred2)
logit3_roc_auc = roc_auc_score(y_testXG3, y_pred3)
fpr1, tpr1, thresholds1 = roc_curve(y_testXG1,
                                    cross_val_predict(CV_rfc, X_testXG1, y_testXG1, cv=5,
                                                      method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testXG2,
                                    CV_rfc.predict_proba(X_testXG2)[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG3,
                                    CV_rfc.predict_proba(X_testXG3)[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle=':', color='green', label='SC (area = %0.2f)' % logit1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='-.', color='purple', label='YP (area = %0.2f)' % logit2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='--', color='blue', label='BS (area = %0.2f)' % logit3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('XGBoost trained on SC using XGBoost feature importance')
plt.legend(loc="lower right")
plt.savefig('Pictures/xgBoost/test2/xgBoostROCXGBoostSC%d' % X_trainXG.shape[1])
# plt.show()
