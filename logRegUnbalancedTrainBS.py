import math as mt

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from numpy import sort
from sklearn.feature_selection import SelectFromModel, f_classif
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import GridSearchCV, cross_val_predict
from xgboost import XGBClassifier

plt.rc("font", size=14)

filename = "data/featuresYPYS_NotScaled.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresBS50_NotScaled.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYS_NotScaled.csv"
df2 = pd.read_csv(filename2, header=None)
filename3 = "data/featuresYP_NotScaled.csv"
df3 = pd.read_csv(filename3, header=None)
print(df1.shape)
print(df2.shape)
print(df3.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 1.25))
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

penalty = ['l2']

solver = ['liblinear']

C = [1e10]
# C = list(range(30, 40, 2))
# C = np.logspace(1, 8, 8)

class_weight = ['balanced']

# intercept_scaling = [50]

# max_iter = [1e5]

# multi_class = ['multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight, solver=solver)

# feature extraction RFE
from sklearn.feature_selection import RFE

for b in range(6, 7):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X_trainFeat)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE1 = df1.iloc[:, col_test[0:b]]
    y_testRFE1 = df1.iloc[:, 36]

    X_testRFE2 = df2.iloc[:, col_test[0:b]]
    y_testRFE2 = df2.iloc[:, 36]

    X_testRFE3 = df3.iloc[:, col_test[0:b]]
    y_testRFE3 = df3.iloc[:, 36]
    # Logistic Regression Model Fitting
    X_trainRFE = df_train_unbalanced.iloc[:, col_test[0:b]]
    print('\n')

    logreg = GridSearchCV(LogisticRegression(), hyperparameters, scoring='roc_auc', cv=5)
    logreg.fit(X_trainRFE, y_train)
    print(logreg.best_params_)
    y_predRFE1 = cross_val_predict(logreg, X_testRFE1, y_testRFE1, cv=5)
    print('Best Penalty:', logreg.best_estimator_.get_params()['penalty'])
    print('Best C:', logreg.best_estimator_.get_params()['C'])
    print('Overall AUC:', roc_auc_score(y_testRFE1, y_predRFE1))
    print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
        logreg.score(X_testRFE1, y_testRFE1)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testRFE1, y_predRFE1)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE1, y_predRFE1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)

    # y_predRFE2 = cross_val_predict(logreg, X_testRFE2, y_testRFE2, cv=5)
    y_predRFE2 = logreg.predict(X_testRFE2)
    print('Best Penalty:', logreg.best_estimator_.get_params()['penalty'])
    print('Best C:', logreg.best_estimator_.get_params()['C'])
    print('Overall AUC:', roc_auc_score(y_testRFE2, y_predRFE2))
    print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
        logreg.score(X_testRFE2, y_testRFE2)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testRFE2, y_predRFE2)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE2, y_predRFE2))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)

    y_predRFE3 = logreg.predict(X_testRFE3)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_testRFE3, y_predRFE3)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE3, y_predRFE3))
    print('Overall AUC:', roc_auc_score(y_testRFE3, y_predRFE3))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    logit1_roc_auc = roc_auc_score(y_testRFE1, y_predRFE1)
    logit2_roc_auc = roc_auc_score(y_testRFE2, y_predRFE2)
    logit3_roc_auc = roc_auc_score(y_testRFE3, y_predRFE3)
    fpr1, tpr1, thresholds1 = roc_curve(y_testRFE1,
                                        cross_val_predict(logreg, X_testRFE1, y_testRFE1, cv=5, method='predict_proba')[
                                        :, 1])
    fpr2, tpr2, thresholds2 = roc_curve(y_testRFE2,
                                        logreg.predict_proba(X_testRFE2)[:, 1])
    fpr3, tpr3, thresholds3 = roc_curve(y_testRFE3,
                                        logreg.predict_proba(X_testRFE3)[:, 1])
    plt.figure()
    plt.plot(fpr2, tpr2, linestyle=':', color='green', label='SC (area = %0.2f)' % logit2_roc_auc)
    plt.plot(fpr3, tpr3, linestyle='-.', color='purple', label='YP (area = %0.2f)' % logit3_roc_auc)
    plt.plot(fpr1, tpr1, linestyle='--', color='blue', label='BS (area = %0.2f)' % logit1_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Log Reg trained on BS using RFE')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/logisticRegression/test2/LogROCRFEBS_NotScaled%d' % b)
    # plt.show()
    print("Finished running logistic model for feature no. (RFE) %d\n\n\n" % b)

for a in range(6, 7):
    print("For feature no. %d" % a)
    # feature selection Univariate
    X_new = SelectKBest(f_classif, k=a)
    fit = X_new.fit(X_trainFeat, y_trainFeat)
    np.set_printoptions(precision=3)
    arr = fit.scores_
    # print(arr)
    features = fit.transform(X_trainFeat)
    # print(features[0:5,:])
    col_test = arr.argsort()[-a:][::-1]
    print(col_test)

    X_testUN1 = df1.iloc[:, col_test[0:a]]
    y_testUN1 = df1.iloc[:, 36]

    X_testUN2 = df2.iloc[:, col_test[0:a]]
    y_testUN2 = df2.iloc[:, 36]

    X_testUN3 = df3.iloc[:, col_test[0:a]]
    y_testUN3 = df3.iloc[:, 36]
    # Logistic Regression Model Fitting
    X_train1 = df_train_unbalanced.iloc[:, col_test[0:a]]

    clf = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=5)
    clf.fit(X_train1, y_train)
    print(clf.best_params_)
    y_pred1 = cross_val_predict(clf, X_testUN1, y_testUN1, cv=5)
    # View best hyperparameters
    print('Best Penalty:', clf.best_estimator_.get_params()['penalty'])
    print('Best C:', clf.best_estimator_.get_params()['C'])
    print('Overall AUC:', roc_auc_score(y_testUN1, y_pred1))
    print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
        clf.score(X_testUN1, y_testUN1)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testUN1, y_pred1)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testUN1, y_pred1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)

    # y_pred2 = cross_val_predict(clf, X_testUN2, y_testUN2, cv=5)
    y_pred2 = clf.predict(X_testUN2)
    # View best hyperparameters
    print('Best Penalty:', clf.best_estimator_.get_params()['penalty'])
    print('Best C:', clf.best_estimator_.get_params()['C'])
    print('Overall AUC:', roc_auc_score(y_testUN2, y_pred2))
    print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
        clf.score(X_testUN2, y_testUN2)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testUN2, y_pred2)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testUN2, y_pred2))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)

    y_pred3 = clf.predict(X_testUN3)
    # View best hyperparameters
    print('Best Penalty:', clf.best_estimator_.get_params()['penalty'])
    print('Best C:', clf.best_estimator_.get_params()['C'])
    print('Overall AUC:', roc_auc_score(y_testUN3, y_pred3))
    print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
        clf.score(X_testUN3, y_testUN3)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testUN3, y_pred3)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testUN3, y_pred3))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)
    # ROC Curve
    logit1_roc_auc = roc_auc_score(y_testUN1, y_pred1)
    logit2_roc_auc = roc_auc_score(y_testUN2, y_pred2)
    logit3_roc_auc = roc_auc_score(y_testUN3, y_pred3)
    fpr1, tpr1, thresholds1 = roc_curve(y_testUN1,
                                        cross_val_predict(clf, X_testUN1, y_testUN1, cv=5, method='predict_proba')[:,
                                        1])
    fpr2, tpr2, thresholds2 = roc_curve(y_testUN2,
                                        clf.predict_proba(X_testUN2)[:, 1])
    fpr3, tpr3, thresholds3 = roc_curve(y_testUN3,
                                        clf.predict_proba(X_testUN3)[:, 1])
    plt.figure()
    plt.plot(fpr2, tpr2, linestyle=':', color='green', label='SC (area = %0.2f)' % logit2_roc_auc)
    plt.plot(fpr3, tpr3, linestyle='-.', color='purple', label='YP (area = %0.2f)' % logit3_roc_auc)
    plt.plot(fpr1, tpr1, linestyle='--', color='blue', label='BS (area = %0.2f)' % logit1_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Log Reg trained on BS using Univariate Analysis')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/logisticRegression/test2/LogROCUNBS_NotScaled%d' % a)
    # plt.show()
    print("Finish running logistic model for feature no. (Univariate FS) %d\n\n\n" % a)

# # X_train = df_train_unbalanced.ix[:, 0:36]
# # X_train = pd.DataFrame(X_train)
#
# X_testXG11 = df1.iloc[:, 0:36]
# y_testXG1 = df1.iloc[:, 36]
# X_testXG21 = df2.iloc[:, 0:36]
# y_testXG2 = df2.iloc[:, 36]
#
# # fit model no training data
# model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                       booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                       subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                       scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
# model.fit(X_trainFeat, y_trainFeat)
# # make predictions for test data
# # y_pred = model.predict(X_test)
# # predictions = [round(value) for value in y_pred]
# # feature importance
# print(model.feature_importances_)
# # plot
# # pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# # pyplot.show()
# # plot feature importance
# # plot_importance(model)
# # pyplot.show()
# # Fit model using each importance as a threshold
# thresholds = sort(model.feature_importances_)
# thresholds[::-1].sort()
# for thresh in thresholds:
#     # select features using threshold
#     selection = SelectFromModel(model, threshold=thresh, prefit=True)
#     select_X_train = selection.transform(X_train)
#     # train model
#     # # selection_model = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
#     selection_model = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
#     selection_model.fit(select_X_train, y_train)
#     print(selection_model.best_params_)
#     print('Best Penalty:', selection_model.best_estimator_.get_params()['penalty'])
#     print('Best C:', selection_model.best_estimator_.get_params()['C'])
#     # # eval model
#     X_testXG1 = selection.transform(X_testXG11)
#     X_testXG2 = selection.transform(X_testXG21)
#     y_pred1 = cross_val_predict(selection_model, X_testXG1, y_testXG1, cv=5)
#     print('Overall AUC:', roc_auc_score(y_testXG1, y_pred1))
#     accuracy = accuracy_score(y_testXG1, y_pred1)
#     from sklearn.metrics import confusion_matrix
#
#     confusion_matrix = confusion_matrix(y_testXG1, y_pred1)
#     print(confusion_matrix)
#     print(classification_report(y_testXG1, y_pred1))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
#         thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
#         sensitivity * specificity * 100.0))
#
#     # select_X_test2 = selection.transform(X_testXG2)
#     # y_pred2 = cross_val_predict(selection_model, X_testXG2, y_testXG2, cv=5)
#     y_pred2 = selection_model.predict(X_testXG2)
#     # predictions = [round(value) for value in y_pred2]
#     print('Overall AUC:', roc_auc_score(y_testXG2, y_pred2))
#     accuracy = accuracy_score(y_testXG2, y_pred2)
#     from sklearn.metrics import confusion_matrix
#
#     confusion_matrix = confusion_matrix(y_testXG2, y_pred2)
#     print(confusion_matrix)
#     print(classification_report(y_testXG2, y_pred2))
#     specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
#     sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
#     print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
#         thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
#         sensitivity * specificity * 100.0))
#     print("Finish running logistic model for feature no. (XGBoost) %d\n\n\n" % X_train.shape[1])
#     # ROC Curve
#     logit1_roc_auc = roc_auc_score(y_testXG1, y_pred1)
#     logit2_roc_auc = roc_auc_score(y_testXG2, y_pred2)
#     fpr1, tpr1, thresholds1 = roc_curve(y_testXG1,
#                                         cross_val_predict(selection_model, X_testXG1, y_testXG1, cv=5,
#                                                         method='predict_proba')[:, 1])
#     fpr2, tpr2, thresholds2 = roc_curve(y_testXG2,
#                                         selection_model.predict_proba(X_testXG2)[:, 1])
#     plt.figure()
#     plt.plot(fpr1, tpr1, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % logit1_roc_auc)
#     plt.plot(fpr2, tpr2, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % logit2_roc_auc)
#     plt.plot([0, 1], [0, 1], 'r--')
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     plt.title('Log Reg trained on SC using XGBoost')
#     plt.legend(loc="lower right")
#     plt.savefig('Pictures/logisticRegression/test/LogROCXGBoostSC_NotScaled%d' % select_X_train.shape[1])
#     # plt.show()

# col_testYPSC = [35, 1, 6, 9, 23, 29, 0, 12, 14, 27, 18, 16, 32, 2, 33, 10]
# col_testYPSC = [35, 1, 6, 9, 12, 0]
col_testYPSC = [35, 1, 6, 12, 9, 14]
# X_testXG = df.iloc[:, col_test1[0:6]]
# y_testXG = df.iloc[:, 36]

X_trainXG = df_train_unbalanced.ix[:, col_testYPSC[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

X_testXG1 = df1.iloc[:, col_testYPSC[0:6]].values
y_testXG1 = df1.iloc[:, 36]
X_testXG2 = df2.iloc[:, col_testYPSC[0:6]].values
y_testXG2 = df2.iloc[:, 36]
X_testXG3 = df3.iloc[:, col_testYPSC[0:6]].values
y_testXG3 = df3.iloc[:, 36]

# # fit model no training data
# model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                       booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                       subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                       scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
# model.fit(X_trainFeat, y_trainFeat)
# # make predictions for test data
# # y_pred = model.predict(X_test)
# # predictions = [round(value) for value in y_pred]
# # feature importance
# print(model.feature_importances_)
# # plot
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# # pyplot.show()
# # plot feature importance
# # plot_importance(model)
# # pyplot.show()
# # Fit model using each importance as a threshold
# thresholds = sort(model.feature_importances_)
# thresholds[::-1].sort()
# for thresh in thresholds:
# # select features using threshold
# selection = SelectFromModel(model, threshold=thresh, prefit=True)
# select_X_train = selection.transform(X_train)
# # train model
selection_model = LogisticRegression()
CV_rfc = GridSearchCV(estimator=selection_model, param_grid=hyperparameters, scoring='roc_auc', cv=5)
CV_rfc.fit(X_trainXG, y_train)
print(CV_rfc.best_params_)
# # eval model
# select_X_test1 = selection.transform(X_testXG1)
y_pred1 = cross_val_predict(CV_rfc, X_testXG1, y_testXG1, cv=5)
print('Overall AUC:', roc_auc_score(y_testXG1, y_pred1))
accuracy = accuracy_score(y_testXG1, y_pred1)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG1, y_pred1)
print(confusion_matrix)
print(classification_report(y_testXG1, y_pred1))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# select_X_test2 = selection.transform(X_testXG2)
# y_pred2 = cross_val_predict(CV_rfc, X_testXG2, y_testXG2, cv=5)
y_pred2 = CV_rfc.predict(X_testXG2)
# predictions = [round(value) for value in y_pred2]
print('Overall AUC:', roc_auc_score(y_testXG2, y_pred2))
accuracy = accuracy_score(y_testXG2, y_pred2)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG2, y_pred2)
print(confusion_matrix)
print(classification_report(y_testXG2, y_pred2))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))
y_pred3 = CV_rfc.predict(X_testXG3)
# predictions = [round(value) for value in y_pred3]
print('Overall AUC:', roc_auc_score(y_testXG3, y_pred3))
accuracy = accuracy_score(y_testXG3, y_pred3)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG3, y_pred3)
print(confusion_matrix)
print(classification_report(y_testXG3, y_pred3))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))
print("Finish running logistic model for feature no. (XGBoost) %d\n\n\n" % X_trainXG.shape[1])
# ROC Curve
logit1_roc_auc = roc_auc_score(y_testXG1, y_pred1)
logit2_roc_auc = roc_auc_score(y_testXG2, y_pred2)
logit3_roc_auc = roc_auc_score(y_testXG3, y_pred3)
fpr1, tpr1, thresholds1 = roc_curve(y_testXG1,
                                    cross_val_predict(CV_rfc, X_testXG1, y_testXG1, cv=5,
                                                      method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testXG2,
                                    CV_rfc.predict_proba(X_testXG2)[:, 1])
plt.figure()
plt.plot(fpr2, tpr2, linestyle=':', color='green', label='SC (area = %0.2f)' % logit2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='-.', color='purple', label='YP (area = %0.2f)' % logit3_roc_auc)
plt.plot(fpr1, tpr1, linestyle='--', color='blue', label='BS (area = %0.2f)' % logit1_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Log Reg trained on BS using XGBoost')
plt.legend(loc="lower right")
plt.savefig('Pictures/logisticRegression/test2/LogROCXGBoostBS_NotScaled%d' % 6)
# plt.show()
