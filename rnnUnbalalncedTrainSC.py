#!/usr/bin/env python

import math as mt

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from numpy import sort
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest, RFE, f_classif
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, roc_curve
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_predict
from sklearn.neighbors import RadiusNeighborsClassifier
from xgboost import XGBClassifier

plt.rc("font", size=14)

filename = "data/featuresYPYS.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresYS.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYP.csv"
df2 = pd.read_csv(filename2, header=None)
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 1.25), random_state=123456)
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

radius = [1.0, 2.0, 3.0]
# neighbors2 = [39]
selectedFeatures = list(range(6, 11, 1))
weights = ['distance']
algorithm = ['auto']
metric = ['minkowski']
leaf_size = [1000]
param_grid = dict(radius=radius, weights=weights, algorithm=algorithm, metric=metric, leaf_size=leaf_size)

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test1 = df1.iloc[:, 0:36]
y_test1 = df1.iloc[:, 36]

X_test2 = df2.iloc[:, 0:36]
y_test2 = df2.iloc[:, 36]

cv = StratifiedKFold(random_state=123456, n_splits=5)

# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_trainFeat, y_trainFeat)
# feature importance
print(model.feature_importances_)
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = RadiusNeighborsClassifier()
    # selection_model.fit(select_X_train, y_train)
    # eval model
    select_X_test1 = selection.transform(X_test1)
    select_X_test2 = selection.transform(X_test2)
    CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='recall_weighted', cv=5)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred1 = cross_val_predict(CV_rfc, select_X_test1, y_test1, cv=5)
    print('Overall AUC:', roc_auc_score(y_test1, y_pred1))

    predictions = [round(value) for value in y_pred1]
    accuracy = accuracy_score(y_test1, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test1, y_pred1)
    print(confusion_matrix)
    print(classification_report(y_test1, y_pred1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

    y_pred2 = CV_rfc.predict(select_X_test2)
    print('Overall AUC:', roc_auc_score(y_test2, y_pred2))

    predictions = [round(value) for value in y_pred2]
    accuracy = accuracy_score(y_test2, y_pred2)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test2, y_pred2)
    print(confusion_matrix)
    print(classification_report(y_test2, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

    # ROC Curve
    xf1_roc_auc = roc_auc_score(y_test1, y_pred1)
    xf2_roc_auc = roc_auc_score(y_test2, y_pred2)
    fpr1, tpr1, thresholds1 = roc_curve(y_test1, y_pred1)
    fpr2, tpr2, thresholds2 = roc_curve(y_test2, y_pred2)
    plt.figure()
    plt.plot(fpr1, tpr1, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf1_roc_auc)
    plt.plot(fpr2, tpr2, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf2_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('KNN trained on SC using XGBoost')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/knn/test/knnROCXGBoostSC%d' % select_X_train.shape[1])
    print("Finished running KNN for feature no. (XGBoost) %d\n\n\n" % select_X_train.shape[1])

# col_YPSC = [35, 1, 6, 9, 23, 29, 0, 12]
# # X_testXG = df.iloc[:, col_test1[0:6]]
# # y_testXG = df.iloc[:, 36]
#
# X_trainXG = df_train_unbalanced.ix[:, col_YPSC[0:8]].values
# X_trainXG = pd.DataFrame(X_trainXG)
#
# X_testXG1 = df1.iloc[:, col_YPSC[0:8]].values
# X_testXG1 = pd.DataFrame(X_testXG1)
# y_testXG1 = df1.iloc[:, 36]
# X_testXG2 = df2.iloc[:, col_YPSC[0:8]].values
# X_testXG2 = pd.DataFrame(X_testXG2)
# y_testXG2 = df2.iloc[:, 36]
#
# # # train model
# selection_model = KNeighborsClassifier()
# # selection_model.fit(select_X_train, y_train)
# # eval model
# CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc.fit(X_trainXG, y_train)
# print(CV_rfc.best_params_)
# # # eval model
# # select_X_test1 = selection.transform(X_testXG1)
# y_pred1 = cross_val_predict(CV_rfc, X_testXG1, y_testXG1, cv=5)
# # y_pred1 = CV_rfc.predict(X_testXG1)
# print('Overall AUC:', roc_auc_score(y_testXG1, y_pred1))
# accuracy = accuracy_score(y_testXG1, y_pred1)
# from sklearn.metrics import confusion_matrix
#
# confusion_matrix = confusion_matrix(y_testXG1, y_pred1)
# print(confusion_matrix)
# print(classification_report(y_testXG1, y_pred1))
# specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
# sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
# print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
#     X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
#     sensitivity * specificity * 100.0))
#
# # select_X_test2 = selection.transform(X_testXG2)
# # y_pred2 = cross_val_predict(CV_rfc, X_testXG2, y_testXG2, cv=5)
# y_pred2 = CV_rfc.predict(X_testXG2)
# # predictions = [round(value) for value in y_pred2]
# print('Overall AUC:', roc_auc_score(y_testXG2, y_pred2))
# accuracy = accuracy_score(y_testXG2, y_pred2)
# from sklearn.metrics import confusion_matrix
#
# confusion_matrix = confusion_matrix(y_testXG2, y_pred2)
# print(confusion_matrix)
# print(classification_report(y_testXG2, y_pred2))
# specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
# sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
# print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
#     X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
#     sensitivity * specificity * 100.0))
# print("Finish running logistic model for feature no. (XGBoost) %d\n\n\n" % X_trainXG.shape[1])
# # ROC Curve
# xf1_roc_auc = roc_auc_score(y_testXG1, y_pred1)
# xf2_roc_auc = roc_auc_score(y_testXG2, y_pred2)
# fpr1, tpr1, thresholds1 = roc_curve(y_testXG1,
#                                     cross_val_predict(CV_rfc, X_testXG1, y_testXG1, method='predict_proba',
#                                                       cv=5)[:, 1])
# fpr2, tpr2, thresholds2 = roc_curve(y_testXG2, CV_rfc.predict_proba(X_testXG2)[:, 1])
# plt.figure()
# plt.plot(fpr1, tpr1, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf1_roc_auc)
# plt.plot(fpr2, tpr2, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf2_roc_auc)
# plt.plot([0, 1], [0, 1], 'r--')
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel('False Positive Rate')
# plt.ylabel('True Positive Rate')
# plt.title('KNN trained on SC using XGBoost')
# plt.legend(loc="lower right")
# plt.savefig('Pictures/knn/test/knnROCXGBoostSC%d' % X_trainXG.shape[1])

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

for a in selectedFeatures:
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(f_classif, k=a)
    fit = X_new.fit(X_trainFeat, y_trainFeat)
    np.set_printoptions(precision=3)
    arr = fit.scores_

    features = fit.transform(X_trainFeat)

    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test1 = df1.loc[:, col_test[0:a]].values
    y_test1 = df1.loc[:, 36].values

    X_test2 = df2.loc[:, col_test[0:a]].values
    y_test2 = df2.loc[:, 36].values

    X_train1 = df_train_unbalanced.loc[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    trained_model = RadiusNeighborsClassifier()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions1 = cross_val_predict(CV_rfc, X_test1, y_test1, cv=5)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test1, predictions1)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test1, predictions1))
    print('Overall AUC:', roc_auc_score(y_test1, predictions1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)

    predictions2 = CV_rfc.predict(X_test2)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test2, predictions2)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test2, predictions2))
    print('Overall AUC:', roc_auc_score(y_test2, predictions2))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    xf1_roc_auc = roc_auc_score(y_test1, predictions1)
    xf2_roc_auc = roc_auc_score(y_test2, predictions2)
    fpr1, tpr1, thresholds1 = roc_curve(y_test1, predictions1)
    fpr2, tpr2, thresholds2 = roc_curve(y_test2, predictions2)
    plt.figure()
    plt.plot(fpr1, tpr1, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf1_roc_auc)
    plt.plot(fpr2, tpr2, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf2_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('KNN trained on SC using Univariate')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/knn/test/knnROCUNSC%d' % a)
    print("Finished running KNN for feature no. (Univariate FS) %d\n\n\n" % a)

for b in selectedFeatures:
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)

    featuresRFE = fitRFE.transform(X_trainFeat)

    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_test1 = df1.loc[:, col_test[0:b]].values
    y_test1 = df1.loc[:, 36].values

    X_test2 = df2.loc[:, col_test[0:b]].values
    y_test2 = df2.loc[:, 36].values

    X_train1 = df_train_unbalanced.loc[:, col_test[0:b]].values
    X_train1 = pd.DataFrame(X_train1)

    trained_model = RadiusNeighborsClassifier()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions1 = cross_val_predict(CV_rfc, X_test1, y_test1, cv=5)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test1, predictions1)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test1, predictions1))
    print('Overall AUC:', roc_auc_score(y_test1, predictions1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)

    predictions2 = CV_rfc.predict(X_test2)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test2, predictions2)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test2, predictions2))
    print('Overall AUC:', roc_auc_score(y_test2, predictions2))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    xf1_roc_auc = roc_auc_score(y_test1, predictions1)
    xf2_roc_auc = roc_auc_score(y_test2, predictions2)
    fpr1, tpr1, thresholds1 = roc_curve(y_test1, predictions1)
    fpr2, tpr2, thresholds2 = roc_curve(y_test2, predictions2)
    plt.figure()
    plt.plot(fpr1, tpr1, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf1_roc_auc)
    plt.plot(fpr2, tpr2, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf2_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('KNN trained on SC using RFE')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/knn/test/knnROCRFESC%d' % b)
    print("Finished running KNN for feature no. (RFE) %d\n\n\n" % b)
