import math as mt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.feature_selection import SelectKBest, chi2, RFE, f_classif
from sklearn.linear_model import LogisticRegression
from xgboost import XGBClassifier
from matplotlib import pyplot
from xgboost import plot_importance
from sklearn.decomposition import PCA

plt.rc("font", size=14)

filename = "data/featuresYPYS_NotScaled.csv"
dfFeat1 = pd.read_csv(filename, header=None)

df_OneFeat1 = dfFeat1.loc[dfFeat1[36] == 1]
df_MinusOne1Feat1 = dfFeat1.loc[dfFeat1[36] == -1]
df_train_OneFeat1 = df_OneFeat1.sample(n=mt.floor(len(df_OneFeat1.index) / 1))
df_train_MinusOneFeat1 = df_MinusOne1Feat1.sample(
    n=mt.floor(len(df_train_OneFeat1.index) * (len(df_MinusOne1Feat1.index) / len(df_OneFeat1.index))))
df_train_unbalancedFeat1 = df_train_OneFeat1.append(df_train_MinusOneFeat1)

X_trainFeat = df_train_unbalancedFeat1.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat1.iloc[:, 36]
X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_new = SelectKBest(f_classif, k=36)
fit = X_new.fit(X_trainFeat, y_trainFeat)
np.set_printoptions(precision=4)
arr = fit.scores_
features = fit.transform(X_trainFeat)
col_test = arr.argsort()[-36:][::-1]
print(arr)
print(col_test)
x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
              11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
              21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
              31, 32, 33, 34, 35])

plt.figure()

plt.bar(x, arr)

plt.title("univariate feature scores for YP and SC")
plt.xlabel('Features')
plt.ylabel('Univariate score')
plt.show()

model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25,
                      random_state=7777, seed=None, missing=None)
model.fit(X_trainFeat, y_trainFeat)
# feature importance
featureImportance1 = model.feature_importances_
print(featureImportance1)
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
plot_importance(model, title='Feature importance for YP, SC and BS', color='mediumblue')

# Plot the PCA spectrum
pca = PCA()
Xtrainpca = pca.fit_transform(X_trainFeat)

plt.figure(1, figsize=(4, 3))
plt.clf()
plt.axes([.2, .2, .7, .7])
plt.plot(pca.explained_variance_ratio_, linewidth=2)
plt.axis('tight')
plt.xlabel('n_components')
plt.ylabel('explained_variance_ratio_')
plt.title("PCA for BS")


model = LogisticRegression()
rfe = RFE(model, 1)
fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
print("Num Features: %d" % (fitRFE.n_features_,))
print("Selected Features: %s" % (fitRFE.support_,))
print("Feature Ranking: %s" % (fitRFE.ranking_,))
arrRFE = fitRFE.ranking_
featuresRFE = fitRFE.transform(X_trainFeat)
col_test = arrRFE.argsort()[-1:][::-1]
print(col_test)

plt.figure()

plt.bar(x, arrRFE, color='mediumblue')

plt.title("RFE feature rankings for YP, SC and BS")
plt.xlabel('Features')
plt.ylabel('Feature ranking')
plt.show()
