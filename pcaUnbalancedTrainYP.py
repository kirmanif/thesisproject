import math as mt

import matplotlib.pyplot as plt
import pandas as pd
import xgboost as xgb
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_predict
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from xgboost import XGBClassifier

plt.rc("font", size=14)

filename = "data/featuresYPYS_NotScaled.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1), random_state=1236547)
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))),
    random_state=1236547)
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresYP_NotScaled.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYS_NotScaled.csv"
df2 = pd.read_csv(filename2, header=None)
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 1.25))
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test1 = df1.iloc[:, 0:36]
y_test1 = df1.iloc[:, 36]
X_test1 = X_test1.astype('float')
y_test1 = y_test1.astype('int')

X_test2 = df2.iloc[:, 0:36]
y_test2 = df2.iloc[:, 36]
X_test2 = X_test2.astype('float')
y_test2 = y_test2.astype('int')

skf = StratifiedKFold(random_state=123456, n_splits=5)

# penalty = ['l2']
# C = [1e3]
# class_weight = ['balanced']
# solver = ['lbfgs']
#
# # Create hyperparameter options
# hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight, solver=solver)
#
# print("*****PCA with Logistic Regression*****\n")
#
# # Applying PCA
# from sklearn.decomposition import PCA
#
# # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
# pca = PCA(n_components=6, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
#           random_state=88888)
# Xtrainpca = pca.fit_transform(X_train)
# Xtestpca1 = pca.transform(X_test1)
# Xtestpca2 = pca.transform(X_test2)
# explained_variance = pca.explained_variance_ratio_
# print('\nExplained variance from components: \n', explained_variance)
# # print('\neigenvectors from components: \n', pca.components_)
#
# # Train Regression Model with PCA
# from sklearn.linear_model import LogisticRegression
#
# classifier = GridSearchCV(LogisticRegression(random_state=0), hyperparameters, verbose=0, scoring='roc_auc', cv=skf)
# classifier.fit(Xtrainpca, y_train)
# # View best hyperparameters
# print('Best Penalty:', classifier.best_estimator_.get_params()['penalty'])
# print('Best C:', classifier.best_estimator_.get_params()['C'])
#
# # Predict Results from PCA Model
# # y_pred = classifier.predict(Xtestpca1)
# y_pred = cross_val_predict(classifier, Xtestpca1, y_test1, cv=skf)
#
# # Create Confusion Matrix
# from sklearn.metrics import confusion_matrix
# from sklearn.metrics import classification_report
#
# confusion_matrix = confusion_matrix(y_test1, y_pred)
# print(confusion_matrix)
# # Compute precision, recall, F-measure and support
# print(classification_report(y_test1, y_pred))
# print('Overall AUC:', roc_auc_score(y_test1, y_pred))
# specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
# print('specificity : ', specificity)
# sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
# print('sensitivity : ', sensitivity)
# print('fawadMetric : ', sensitivity * specificity)
#
# y_pred2 = classifier.predict(Xtestpca2)
# # y_pred = cross_val_predict(classifier, Xtestpca2, y_test2, cv=skf)
#
# # Create Confusion Matrix
# from sklearn.metrics import confusion_matrix
# from sklearn.metrics import classification_report
#
# confusion_matrix = confusion_matrix(y_test2, y_pred2)
# print(confusion_matrix)
# # Compute precision, recall, F-measure and support
# print(classification_report(y_test2, y_pred2))
# print('Overall AUC:', roc_auc_score(y_test2, y_pred2))
# specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
# print('specificity : ', specificity)
# sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
# print('sensitivity : ', sensitivity)
# print('fawadMetric : ', sensitivity * specificity)
# print("Finished running PCA with Logistic Regression for components no. %d\n\n\n" % 6)
# # ROC Curve
# xf1_roc_auc = roc_auc_score(y_test1, y_pred)
# xf2_roc_auc = roc_auc_score(y_test2, y_pred2)
# fpr1, tpr1, thresholds1 = roc_curve(y_test1, cross_val_predict(classifier, Xtestpca1, y_test1, cv=skf,
#                                                                method='predict_proba')[:, 1])
# fpr2, tpr2, thresholds2 = roc_curve(y_test2, classifier.predict_proba(Xtestpca2)[:, 1])
# plt.figure()
# plt.plot(fpr2, tpr2, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
# plt.plot(fpr1, tpr1, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf1_roc_auc)
# plt.plot([0, 1], [0, 1], 'r--')
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel('False Positive Rate')
# plt.ylabel('True Positive Rate')
# plt.title('Logistic Regression trained on YP using PCA')
# plt.legend(loc="lower right")
# plt.savefig('Pictures/logisticRegression/logisticRegressionROCPCAYP_NotScaled')
# # plt.show()

print("*****PCA with XGBoost*****\n")

# Applying PCA
from sklearn.decomposition import PCA

pca = PCA(n_components=6, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
          random_state=88888)
Xtrainpca = pca.fit_transform(X_train)
Xtestpca1 = pca.transform(X_test1)
Xtestpca2 = pca.transform(X_test2)
explained_variance = pca.explained_variance_ratio_
print('\nExplained variance from components: \n', explained_variance)
# print('\neigenvectors from components: \n', pca.components_)

y_train3 = y_train.map({1: 1, -1: 0})
y_test3 = y_test1.map({1: 1, -1: 0})
y_test4 = y_test2.map({1: 1, -1: 0})
dTrain = xgb.DMatrix(Xtrainpca, label=y_train3)

# Create XGBoost instance

clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                    booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                    subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                    scale_pos_weight=len(df_MinusOne1.index) / len(df_One.index), base_score=0.25,
                    random_state=7777, seed=None, missing=None)
xgb_param = clf.get_xgb_params()

# selection_model.fit(select_X_train, y_train)
cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=10, stratified=True, folds=10, metrics=(),
                  obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
                  as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
# eval model
print('Best number of trees = {}'.format(cvresult.shape[0]))
clf.set_params(n_estimators=cvresult.shape[0])
print('Fit on the training data')
clf.fit(Xtrainpca, y_train3, eval_metric='auc')
print('Predict the probabilities based on features in the test1 set')
# pred = clf.predict(Xtestpca1, ntree_limit=cvresult.shape[0])
pred = cross_val_predict(clf, Xtestpca1, y_test3, cv=skf)
print('Overall AUC:', roc_auc_score(y_test3, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_test3, predictions)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test3, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test3, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Predict the probabilities based on features in the test2 set')
pred2 = clf.predict(Xtestpca2, ntree_limit=cvresult.shape[0])
# pred2 = cross_val_predict(clf, Xtestpca2, y_test4, cv=skf)
print('Overall AUC:', roc_auc_score(y_test4, pred2))

predictions2 = [round(value) for value in pred2]
accuracy = accuracy_score(y_test4, predictions2)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test4, predictions2)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test4, predictions2))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running PCA with XGBoost for components no. %d\n\n\n" % 6)
# ROC Curve
xf1_roc_auc = roc_auc_score(y_test3, predictions)
xf2_roc_auc = roc_auc_score(y_test4, predictions2)
fpr1, tpr1, thresholds1 = roc_curve(y_test3, cross_val_predict(clf, Xtestpca1, y_test3, cv=skf, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_test4, clf.predict_proba(Xtestpca2, ntree_limit=cvresult.shape[0])[:, 1])
plt.figure()
plt.plot(fpr2, tpr2, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr1, tpr1, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf1_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('XGBoost trained on YP using PCA')
plt.legend(loc="lower right")
plt.savefig('Pictures/xgBoost/xgboostROCPCAYP_NotScaled')
# plt.show()

print("*****PCA with Random Forest*****\n")

n_estimators = [50]
class_weight = ['balanced']
criterion = ['entropy']
min_samples_split = [2]
min_samples_leaf = [35]
# max_depth = [2, 5, 10]
# max_leaf_nodes = [3, 7, 10, 15, 20, 30]
min_impurity_decrease = [0.0]
min_weight_fraction_leaf = [0]
n_jobs = [-1]
# oob_score = [True]
random_state = [3486875]
param_grid = dict(n_estimators=n_estimators, criterion=criterion, class_weight=class_weight,
                  min_samples_split=min_samples_split, min_samples_leaf=min_samples_leaf, n_jobs=n_jobs,
                  min_impurity_decrease=min_impurity_decrease, random_state=random_state,
                  min_weight_fraction_leaf=min_weight_fraction_leaf)

from sklearn.decomposition import PCA

pca = PCA(n_components=6, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
          random_state=88888)
Xtrainpca = pca.fit_transform(X_train)
Xtestpca1 = pca.transform(X_test1)
Xtestpca2 = pca.transform(X_test2)
explained_variance = pca.explained_variance_ratio_
print('\nExplained variance from components: \n', explained_variance)
# print('\neigenvectors from components: \n', pca.components_)

# Create random forest classifier instance
trained_model = RandomForestClassifier()

CV_rfc1 = GridSearchCV(estimator=trained_model, param_grid=dict(param_grid, max_features=[6]), cv=skf,
                       scoring='roc_auc')
CV_rfc1.fit(Xtrainpca, y_train)
print(CV_rfc1.best_params_)
# predictions1 = CV_rfc1.predict(Xtestpca1)
predictions1 = cross_val_predict(CV_rfc1, Xtestpca1, y_test1, cv=skf)

predictions2 = CV_rfc1.predict(Xtestpca2)
# predictions2 = cross_val_predict(CV_rfc1, Xtestpca2, y_test2, cv=skf)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test1, predictions1)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test1, predictions1))
print('Overall AUC Test1:', roc_auc_score(y_test1, predictions1))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity Test1: ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity Test1: ', sensitivity)
print('fawadmetric Test1: ', sensitivity * specificity * 100.0)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test2, predictions2)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test2, predictions2))
print('Overall AUC Test2:', roc_auc_score(y_test2, predictions2))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity Test2: ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity Test2: ', sensitivity)
print('fawadmetric Test2: ', sensitivity * specificity * 100.0)
print("Finished running PCA with Random Forest for components no. %d\n\n\n" % 6)
# ROC Curve
xf1_roc_auc = roc_auc_score(y_test1, predictions1)
xf2_roc_auc = roc_auc_score(y_test2, predictions2)
fpr1, tpr1, thresholds1 = roc_curve(y_test1,
                                    cross_val_predict(CV_rfc1, Xtestpca1, y_test1, cv=skf, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_test2,
                                    CV_rfc1.predict_proba(Xtestpca2)[:, 1])
plt.figure()
plt.plot(fpr2, tpr2, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr1, tpr1, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf1_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Random Forest trained on YP using PCA')
plt.legend(loc="lower right")
plt.savefig('Pictures/randomForest/randomForestROCPCAYP_NotScaled')
# plt.show()

print("*****PCA with SVC*****\n")

C_range = [1]
gamma_range = ['auto']
class_weight = ['balanced']
kernel = ['rbf']
decision_function_shape = ['ovo']
probability = [True]
param_grid = dict(gamma=gamma_range, C=C_range, kernel=kernel, class_weight=class_weight,
                  decision_function_shape=decision_function_shape, probability=probability)

# Applying PCA
from sklearn.decomposition import PCA

# pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
pca = PCA(n_components=6, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
          random_state=88888)
Xtrainpca = pca.fit_transform(X_train)
Xtestpca1 = pca.transform(X_test1)
Xtestpca2 = pca.transform(X_test2)
explained_variance = pca.explained_variance_ratio_
print("\nExplained variance from components: \n", explained_variance)
# print('\neigenvectors from components: \n', pca.components_)

# Create SVC instance
trained_model = SVC()

CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=skf)
CV_rfc.fit(Xtrainpca, y_train)
print(CV_rfc.best_params_)
# predictions = CV_rfc.predict(Xtestpca1)
predictions1 = cross_val_predict(CV_rfc, Xtestpca1, y_test1, cv=skf)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test1, predictions1)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test1, predictions1))
print('Overall AUC:', roc_auc_score(y_test1, predictions1))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

predictions2 = CV_rfc.predict(Xtestpca2)
# predictions2 = cross_val_predict(CV_rfc, Xtestpca2, y_test2, cv=skf)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test2, predictions2)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test2, predictions2))
print('Overall AUC:', roc_auc_score(y_test2, predictions2))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running PCA with SVC for components no. %d\n\n\n" % 6)
# ROC Curve
xf1_roc_auc = roc_auc_score(y_test1, predictions1)
xf2_roc_auc = roc_auc_score(y_test2, predictions2)
fpr1, tpr1, thresholds1 = roc_curve(y_test1,
                                    cross_val_predict(CV_rfc, Xtestpca1, y_test1, cv=skf, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_test2,
                                    CV_rfc.predict_proba(Xtestpca2)[:, 1])
plt.figure()
plt.plot(fpr2, tpr2, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr1, tpr1, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf1_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('SVC trained on YP using PCA')
plt.legend(loc="lower right")
plt.savefig('Pictures/svc/svcROCPCAYP_NotScaled')
# plt.show()

print("*****PCA with K-nearest neighbor*****\n")

n_neighbors = list(range(5, 30, 1))
weights = ['distance']
algorithm = ['auto']
param_grid = dict(n_neighbors=n_neighbors, weights=weights, algorithm=algorithm)

# Applying PCA
from sklearn.decomposition import PCA

# pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
pca = PCA(n_components=6, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
          random_state=88888)
Xtrainpca = pca.fit_transform(X_train)
Xtestpca1 = pca.transform(X_test1)
Xtestpca2 = pca.transform(X_test2)
explained_variance = pca.explained_variance_ratio_
print("\nExplained variance from components: \n", explained_variance)
# print('\neigenvectors from components: \n', pca.components_)

# Create SVC instance
trained_model = KNeighborsClassifier()

CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=skf)
CV_rfc.fit(Xtrainpca, y_train)
print(CV_rfc.best_params_)
# predictions = CV_rfc.predict(Xtestpca1)
predictions1 = cross_val_predict(CV_rfc, Xtestpca1, y_test1, cv=skf)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test1, predictions1)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test1, predictions1))
print('Overall AUC:', roc_auc_score(y_test1, predictions1))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

predictions2 = CV_rfc.predict(Xtestpca2)
# predictions2 = cross_val_predict(CV_rfc, Xtestpca2, y_test2, cv=skf)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test2, predictions2)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_test2, predictions2))
print('Overall AUC:', roc_auc_score(y_test2, predictions2))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running PCA with SVC for components no. %d\n\n\n" % 6)
# ROC Curve
xf1_roc_auc = roc_auc_score(y_test1, predictions1)
xf2_roc_auc = roc_auc_score(y_test2, predictions2)
fpr1, tpr1, thresholds1 = roc_curve(y_test1,
                                    cross_val_predict(CV_rfc, Xtestpca1, y_test1, cv=skf, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_test2,
                                    CV_rfc.predict_proba(Xtestpca2)[:, 1])
plt.figure()
plt.plot(fpr2, tpr2, linestyle='dashdot', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr1, tpr1, linestyle='dotted', color='blue', label='YP (area = %0.2f)' % xf1_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('k-nearest neighbor trained on YP using PCA')
plt.legend(loc="lower right")
plt.savefig('Pictures/knn/knnROCPCAYP_NotScaled')
plt.show()
