import math as mt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import normalize
from sklearn.svm import SVC
from xgboost import XGBClassifier
from sklearn.pipeline import Pipeline
from sklearn import linear_model

plt.rc("font", size=14)

filename1 = "data/featuresYS_NotScaled.csv"
df = pd.read_csv(filename1, header=None)
df_sc = normalize(df.iloc[:, 0:36], norm='l1', axis=1, copy=False, return_norm=False)
df1 = pd.DataFrame(df_sc)
df1[36] = df.iloc[:, 36]
filename2 = "data/featuresYP_NotScaled.csv"
df = pd.read_csv(filename2, header=None)
df_sc = normalize(df.iloc[:, 0:36], norm='l1', axis=1, copy=False, return_norm=False)
df2 = pd.DataFrame(df_sc)
df2[36] = df.iloc[:, 36]
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 1))
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
X_test = X_test.astype('float')
y_test = y_test.astype('int')

skf = StratifiedKFold(random_state=123456, n_splits=5)

print("*****PCA with Random Forest*****\n")

n_estimators = [500]
class_weight = ['balanced_subsample']
criterion = ['gini']
min_samples_split = [2]
min_samples_leaf = [10, 30, 60, 90, 120, 150, 180, 210]
# max_depth = [2, 5, 10]
# max_leaf_nodes = [3, 7, 10]
# min_impurity_decrease = [0.0]
# min_weight_fraction_leaf = [0]
n_jobs = [-1]
# oob_score = [True]
random_state = [3486875]
param_grid = dict(n_estimators=n_estimators, criterion=criterion, class_weight=class_weight,
                  min_samples_split=min_samples_split, min_samples_leaf=min_samples_leaf, n_jobs=n_jobs,
                  random_state=random_state)

for b in range(6, 10):
    print("For components: %d" % b)

    # Applying PCA
    from sklearn.decomposition import PCA

    # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
    pca = PCA(n_components=b, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
              random_state=88888)
    Xtrainpca = pca.fit_transform(X_train)
    Xtestpca = pca.transform(X_test)
    explained_variance = pca.explained_variance_ratio_
    print('\nExplained variance from components: \n', explained_variance)
    print('\neigenvectors from components: \n', pca.components_)

    # Create random forest classifier instance
    trained_model = RandomForestClassifier()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=dict(param_grid, max_features=[b]), cv=skf,
                          scoring='roc_auc')
    CV_rfc.fit(Xtrainpca, y_train)
    print(CV_rfc.best_params_)
    predictions = CV_rfc.predict(Xtestpca)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, predictions))
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(Xtestpca)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, CV_rfc.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test, CV_rfc.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Random Forest (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running PCA with Random Forest for components no. %d\n\n\n" % b)

print("*****PCA with XGBoost*****\n")

for b in range(5, 10):
    print("For components: %d" % b)

    # Applying PCA
    from sklearn.decomposition import PCA

    # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
    pca = PCA(n_components=b, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
              random_state=88888)
    Xtrainpca = pca.fit_transform(X_train)
    Xtestpca = pca.transform(X_test)
    explained_variance = pca.explained_variance_ratio_
    print('\nExplained variance from components: \n', explained_variance)
    print('\neigenvectors from components: \n', pca.components_)

    y_train1 = y_train.map({1: 1, -1: 0})
    y_test1 = y_test.map({1: 1, -1: 0})
    dTrain = xgb.DMatrix(Xtrainpca, label=y_train1)

    # Create XGBoost instance

    clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                        booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                        subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                        scale_pos_weight=len(df_MinusOne1.index) / len(df_One.index), base_score=0.25,
                        random_state=7777, seed=None, missing=None)
    xgb_param = clf.get_xgb_params()

    # selection_model.fit(select_X_train, y_train)
    cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=10, stratified=True, folds=10, metrics=(),
                      obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
                      as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
    # eval model
    print('Best number of trees = {}'.format(cvresult.shape[0]))
    clf.set_params(n_estimators=cvresult.shape[0])
    print('Fit on the training data')
    clf.fit(Xtrainpca, y_train1, eval_metric='auc')
    print('Predict the probabilities based on features in the test set')
    pred = clf.predict_proba(Xtestpca, ntree_limit=cvresult.shape[0])[:, 1]
    print('Overall AUC:', roc_auc_score(y_test1, clf.predict_proba(Xtestpca)[:, 1]))

    predictions = [round(value) for value in pred]
    accuracy = accuracy_score(y_test1, predictions)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test1, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test1, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test1, clf.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test1, clf.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='XGBoost (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running PCA with XGBoost for components no. %d\n\n\n" % b)

# Plot the PCA spectrum
pca = PCA()
XGBoost = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                        booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                        subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                        scale_pos_weight=len(df_MinusOne1.index) / len(df_One.index), base_score=0.25,
                        random_state=7777, seed=None, missing=None)
pipe = Pipeline(steps=[('pca', pca), ('XGBoost', XGBoost)])

Xtrainpca = pca.fit_transform(X_train)
Xtestpca = pca.transform(X_test)

plt.figure(1, figsize=(4, 3))
plt.clf()
plt.axes([.2, .2, .7, .7])
plt.plot(pca.explained_variance_ratio_, linewidth=2)
plt.axis('tight')
plt.xlabel('n_components')
plt.ylabel('explained_variance_ratio_')

# Prediction
n_components = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
svd_solver = ['auto']
random_state = [88888]

# Parameters of pipelines can be set using ‘__’ separated parameter names:
estimator = GridSearchCV(pipe, dict(pca__n_components=n_components, pca__svd_solver=svd_solver,
                                    pca__random_state=random_state), scoring='roc_auc')
estimator.fit(Xtrainpca, y_train)

plt.axvline(estimator.best_estimator_.named_steps['pca'].n_components,
            linestyle=':', label='n_components chosen')
plt.legend(prop=dict(size=12))
plt.savefig('PCA XGBoost spectrum')
# plt.show()

print("*****PCA with SVC*****\n")

C_range = [1]
gamma_range = ['auto']
class_weight = ['balanced']
kernel = ['rbf']
decision_function_shape = ['ovo']
probability = [True]
param_grid = dict(gamma=gamma_range, C=C_range, kernel=kernel, class_weight=class_weight,
                  decision_function_shape=decision_function_shape, probability=probability)

for b in range(5, 10):
    print("For components: %d" % b)

    # Applying PCA
    from sklearn.decomposition import PCA

    # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
    pca = PCA(n_components=b, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
              random_state=88888)
    Xtrainpca = pca.fit_transform(X_train)
    Xtestpca = pca.transform(X_test)
    explained_variance = pca.explained_variance_ratio_
    print("\nExplained variance from components: \n", explained_variance)
    print('\neigenvectors from components: \n', pca.components_)

    # Create SVC instance
    trained_model = SVC()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=skf)
    CV_rfc.fit(Xtrainpca, y_train)
    print(CV_rfc.best_params_)
    predictions = CV_rfc.predict(Xtestpca)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, predictions))
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(Xtestpca)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, CV_rfc.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test, CV_rfc.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='SVC (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running PCA with SVC for components no. %d\n\n\n" % b)

# Plot the PCA spectrum
pca = PCA()
SVC = SVC()
pipe = Pipeline(steps=[('pca', pca), ('SVC', SVC)])

Xtrainpca = pca.fit_transform(X_train)
Xtestpca = pca.transform(X_test)

plt.figure(1, figsize=(4, 3))
plt.clf()
plt.axes([.2, .2, .7, .7])
plt.plot(pca.explained_variance_ratio_, linewidth=2)
plt.axis('tight')
plt.xlabel('n_components')
plt.ylabel('explained_variance_ratio_')

# Prediction
n_components = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
svd_solver = ['auto']
random_state = [88888]

# Parameters of pipelines can be set using ‘__’ separated parameter names:
estimator = GridSearchCV(pipe, dict(pca__n_components=n_components, pca__svd_solver=svd_solver,
                                    pca__random_state=random_state, SVC__C=C_range, SVC__gamma=gamma_range,
                                    SVC__kernel=kernel, SVC__class_weight=class_weight,
                                    SVC__decision_function_shape=decision_function_shape, SVC__probability=probability),
                         scoring='roc_auc')
estimator.fit(Xtrainpca, y_train)

plt.axvline(estimator.best_estimator_.named_steps['pca'].n_components,
            linestyle=':', label='n_components chosen')
plt.legend(prop=dict(size=12))
plt.savefig('PCA SVC spectrum')
# plt.show()

penalty = ['l1', 'l2']

# solver = ['newton-cg', 'lbfgs', 'liblinear']

# C = [0.00001, 0.00003, 0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1, 0.03, 1, 3, 10, 30, 100, 300, 1000, 3000, 10000,
#     30000, 100000, 300000, 1000000]
C = [1]

class_weight = ['balanced']

# multi_class = ['ovr', 'multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight)

print("*****PCA with Logistic Regression*****\n")

for a in range(5, 10):
    print("For components: %d" % a)

    # Applying PCA
    from sklearn.decomposition import PCA

    # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
    pca = PCA(n_components=a, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
              random_state=88888)
    Xtrainpca = pca.fit_transform(X_train)
    Xtestpca = pca.transform(X_test)
    explained_variance = pca.explained_variance_ratio_
    print('\nExplained variance from components: \n', explained_variance)
    print('\neigenvectors from components: \n', pca.components_)

    # Train Regression Model with PCA
    from sklearn.linear_model import LogisticRegression

    classifier = GridSearchCV(LogisticRegression(random_state=0), hyperparameters, verbose=0, scoring='roc_auc', cv=skf)
    classifier.fit(Xtrainpca, y_train)
    # View best hyperparameters
    print('Best Penalty:', classifier.best_estimator_.get_params()['penalty'])
    print('Best C:', classifier.best_estimator_.get_params()['C'])

    # Predict Results from PCA Model
    y_pred = classifier.predict(Xtestpca)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, y_pred))
    print('Overall AUC:', roc_auc_score(y_test, classifier.predict(Xtestpca)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, classifier.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test, classifier.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running PCA with Logistic Regression for components no. %d\n\n\n" % a)

# Plot the PCA spectrum
pca = PCA()
logistic = linear_model.LogisticRegression()
pipe = Pipeline(steps=[('pca', pca), ('logistic', logistic)])

Xtrainpca = pca.fit_transform(X_train)
Xtestpca = pca.transform(X_test)

plt.figure(1, figsize=(4, 3))
plt.clf()
plt.axes([.2, .2, .7, .7])
plt.plot(pca.explained_variance_ratio_, linewidth=2)
plt.axis('tight')
plt.xlabel('n_components')
plt.ylabel('explained_variance_ratio_')

# Prediction
n_components = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
svd_solver = ['auto']

# Parameters of pipelines can be set using ‘__’ separated parameter names:
estimator = GridSearchCV(pipe, dict(pca__n_components=n_components, pca__svd_solver=svd_solver,
                                    pca__random_state=random_state, logistic__C=C), scoring='roc_auc')
estimator.fit(Xtrainpca, y_train)

plt.axvline(estimator.best_estimator_.named_steps['pca'].n_components,
            linestyle=':', label='n_components chosen')
plt.legend(prop=dict(size=12))
plt.savefig('PCA Logistic spectrum')
# plt.show()
