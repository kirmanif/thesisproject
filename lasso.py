import math as mt
# from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from numpy import sort
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import Lasso, LassoCV, RandomizedLasso
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC

plt.rc("font", size=14)

filename = "data/featuresYP_2D.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 4))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresYS_2D.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYP_2D.csv"
df2 = pd.read_csv(filename2, header=None)
print(df1.shape)
print(df2.shape)

test = 1

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 4))
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]

param_grid = {
    # 'C': [0.94, 1, 10, 100, 1000, 10000, 100000, 1000000],
    # 'C': [0.30, 0.33, 0.36, 0.39, 0.42, 0.45, 0.48, 0.51, 0.54, 0.57, 0.60, 0.63, 0.66, 0.69, 0.72],
    'C': [1],
    'kernel': ['linear'],
    'class_weight': ['balanced'],
    'decision_function_shape': ['ovo']
}


def rmse_cv(model):
    rmse = np.sqrt(-cross_val_score(model, X_train, y_train, scoring="mean_squared_error", cv=10))
    return rmse


# Run RandomizedLasso
print("\n Running RandomizedLasso:")
model_rlasso = RandomizedLasso(alpha='bic', verbose=False, n_resampling=5000, random_state=100001, n_jobs=1)
model_rlasso.fit(X_trainFeat, y_trainFeat)
# Collect the scores into a dataframe and save
coef = pd.DataFrame(model_rlasso.scores_, columns=['RandomizedLasso_score'])
coef['Feature'] = X_trainFeat.columns
coef['Relative score'] = coef['RandomizedLasso_score'] / coef['RandomizedLasso_score'].sum()
coef = coef.sort_values('Relative score', ascending=False)
coef = coef[['Feature', 'RandomizedLasso_score', 'Relative score']]
# coef.to_csv("feature_importance_randomizedlasso.csv", index=False)
print(" feature_importance_randomizedlasso:\n" % coef)
# Select scores to plot
imp_coef = pd.concat((coef.head(25), coef.tail(5)))
imp_coef.plot(kind="barh", x='Feature', y='Relative score', legend=False, figsize=(8, 10))
plt.title('5 Least and 25 Most Important RandomizedLasso Features')
plt.xlabel('Relative RandomizedLasso score')
# plt.savefig('feature_importance_randomizedlasso.png', bbox_inches='tight', pad_inches=0.5)
# plt.show(block=False)
# plt.show()
train_new = model_rlasso.transform(X_train)
test_new = model_rlasso.transform(X_test)
print(" Running LassoCV using features selected by RandomizedLasso:")
model_lasso = LassoCV(eps=0.0000001, n_alphas=200, max_iter=1000000, cv=10, precompute=True, random_state=100001)
model_lasso.fit(train_new, y_train)
print(" Best alpha value: %f" % model_lasso.alpha_)
lasso = Lasso(alpha=model_lasso.alpha_, max_iter=1000000, precompute=True, random_state=1000001)
score = round((rmse_cv(lasso).mean()), 6)
print(score)
lasso.fit(train_new, y_train)
# preds = np.expm1(lasso.predict(test_new))
# solution = pd.DataFrame({"Id": test.Id, "SalePrice": preds})
# now = datetime.now()
# sub_file = 'submission_RandomizedLasso_' + str(score) + '_' + str(now.strftime("%Y-%m-%d-%H-%M")) + '.csv'
# print(" Writing submission file: %s\n" % sub_file)
# solution.to_csv(sub_file, index=False)
y_pred = np.expm1(lasso.predict(test_new))
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_test, predictions)
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_test, y_pred)
print(confusion_matrix)
print(classification_report(y_test, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    accuracy * 100.0, specificity * 100.0, sensitivity * 100.0, sensitivity * specificity * 100.0))

# Fit model using each importance as a threshold
thresholds = sort(coef)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model_rlasso, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = SVC()
    # eval model
    select_X_test = selection.transform(X_test)

    CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, cv=5)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred = CV_rfc.predict(select_X_test)
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(select_X_test)))

    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report
    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    print(classification_report(y_test, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
