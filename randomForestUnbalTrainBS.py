#!/usr/bin/env python
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import SelectKBest, RFE, f_classif
from sklearn.feature_selection import chi2
import matplotlib.pyplot as plt
import numpy as np
import math as mt
from numpy import sort
import pandas as pd
from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score, roc_curve
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import normalize

plt.rc("font", size=14)

filename = "data/featuresYPYS_NotScaled.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresBS50_NotScaled.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYS_NotScaled.csv"
df2 = pd.read_csv(filename2, header=None)
filename3 = "data/featuresYP_NotScaled.csv"
df3 = pd.read_csv(filename3, header=None)
print(df1.shape)
print(df2.shape)
print(df3.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 1.25))
# print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
# print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
# print(len(df_train_unbalanced))

n_estimators = [50]
# max_features = [1]
class_weight = ['balanced_subsample']
criterion = ['gini']
min_samples_split = [2]
min_samples_leaf = [10]
# max_depth = [None]
# max_leaf_nodes = [3, 7, None]
# min_impurity_decrease = [0.0]
# min_weight_fraction_leaf = [0]
n_jobs = [-1]
# oob_score = [True]
random_state = [888888]
param_grid = dict(n_estimators=n_estimators, criterion=criterion, min_samples_split=min_samples_split,
                  n_jobs=n_jobs, random_state=random_state, class_weight=class_weight,
                  min_samples_leaf=min_samples_leaf)

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
# X_trainFeat = normalize(X_trainFeat, norm='l2', axis=0)
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

for b in range(6, 7):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)

    featuresRFE = fitRFE.transform(X_trainFeat)

    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_test1 = df1.loc[:, col_test[0:b]].values
    y_test1 = df1.loc[:, 36].values

    X_test2 = df2.loc[:, col_test[0:b]].values
    y_test2 = df2.loc[:, 36].values

    X_test3 = df3.loc[:, col_test[0:b]].values
    y_test3 = df3.loc[:, 36].values

    X_train1 = df_train_unbalanced.loc[:, col_test[0:b]].values
    X_train1 = pd.DataFrame(X_train1)

    trained_model = RandomForestClassifier()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=dict(param_grid, max_features=[b]), scoring='roc_auc',
                          cv=5)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions1 = cross_val_predict(CV_rfc, X_test1, y_test1, cv=5)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test1, predictions1)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test1, predictions1))
    print('Overall AUC:', roc_auc_score(y_test1, predictions1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)

    predictions2 = CV_rfc.predict(X_test2)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test2, predictions2)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test2, predictions2))
    print('Overall AUC:', roc_auc_score(y_test2, predictions2))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)

    predictions3 = CV_rfc.predict(X_test3)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test3, predictions3)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test3, predictions3))
    print('Overall AUC:', roc_auc_score(y_test3, predictions3))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    xf1_roc_auc = roc_auc_score(y_test1, predictions1)
    xf2_roc_auc = roc_auc_score(y_test2, predictions2)
    xf3_roc_auc = roc_auc_score(y_test3, predictions3)
    fpr1, tpr1, thresholds1 = roc_curve(y_test1,
                                        cross_val_predict(CV_rfc, X_test1, y_test1, method='predict_proba', cv=5)[:, 1])
    fpr2, tpr2, thresholds2 = roc_curve(y_test2, CV_rfc.predict_proba(X_test2)[:, 1])
    fpr3, tpr3, thresholds3 = roc_curve(y_test3, CV_rfc.predict_proba(X_test3)[:, 1])
    plt.figure()
    plt.plot(fpr2, tpr2, linestyle=':', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
    plt.plot(fpr3, tpr3, linestyle='-.', color='purple', label='YP (area = %0.2f)' % xf3_roc_auc)
    plt.plot(fpr1, tpr1, linestyle='--', color='blue', label='BS (area = %0.2f)' % xf1_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Random Forest trained on BS using RFE')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/randomForest/test2/randomForestROCRFEBS%d' % b)
    print("Finished running SVC for feature no. (RFE) %d\n\n\n" % b)

for a in range(6, 7):
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(f_classif, k=a)
    fit = X_new.fit(X_trainFeat, y_trainFeat)
    np.set_printoptions(precision=3)
    arr = fit.scores_

    features = fit.transform(X_trainFeat)

    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test1 = df1.loc[:, col_test[0:a]].values
    y_test1 = df1.loc[:, 36].values

    X_test2 = df2.loc[:, col_test[0:a]].values
    y_test2 = df2.loc[:, 36].values

    X_test3 = df3.loc[:, col_test[0:a]].values
    y_test3 = df3.loc[:, 36].values

    X_train1 = df_train_unbalanced.loc[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    trained_model = RandomForestClassifier()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=dict(param_grid, max_features=[a]),
                          scoring='recall_weighted',
                          cv=5)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions1 = cross_val_predict(CV_rfc, X_test1, y_test1, cv=5)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test1, predictions1)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test1, predictions1))
    print('Overall AUC:', roc_auc_score(y_test1, predictions1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)

    predictions2 = CV_rfc.predict(X_test2)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test2, predictions2)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test2, predictions2))
    print('Overall AUC:', roc_auc_score(y_test2, predictions2))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)

    predictions3 = CV_rfc.predict(X_test3)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test3, predictions3)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test3, predictions3))
    print('Overall AUC:', roc_auc_score(y_test3, predictions3))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    # ROC Curve
    xf1_roc_auc = roc_auc_score(y_test1, predictions1)
    xf2_roc_auc = roc_auc_score(y_test2, predictions2)
    xf3_roc_auc = roc_auc_score(y_test3, predictions3)
    fpr1, tpr1, thresholds1 = roc_curve(y_test1,
                                        cross_val_predict(CV_rfc, X_test1, y_test1, method='predict_proba', cv=5)[:, 1])
    fpr2, tpr2, thresholds2 = roc_curve(y_test2, CV_rfc.predict_proba(X_test2)[:, 1])
    fpr3, tpr3, thresholds3 = roc_curve(y_test3, CV_rfc.predict_proba(X_test3)[:, 1])
    plt.figure()
    plt.plot(fpr2, tpr2, linestyle=':', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
    plt.plot(fpr3, tpr3, linestyle='-.', color='purple', label='YP (area = %0.2f)' % xf3_roc_auc)
    plt.plot(fpr1, tpr1, linestyle='--', color='blue', label='BS (area = %0.2f)' % xf1_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Random Forest trained on BS using Univariate')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/randomForest/test2/randomForestROCUNBS%d' % a)
    print("Finished running SVC for feature no. (Univariate FS) %d\n\n\n" % a)

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test1 = df1.iloc[:, 0:36]
y_test1 = df1.iloc[:, 36]

X_test2 = df2.iloc[:, 0:36]
y_test2 = df2.iloc[:, 36]

X_test3 = df3.iloc[:, 0:36]
y_test3 = df3.iloc[:, 36]

# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_trainFeat, y_trainFeat)
# feature importance
print(model.feature_importances_)
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = RandomForestClassifier()
    # selection_model.fit(select_X_train, y_train)
    # eval model
    select_X_test1 = selection.transform(X_test1)
    select_X_test2 = selection.transform(X_test2)
    select_X_test3 = selection.transform(X_test3)
    CV_rfc = GridSearchCV(estimator=selection_model,
                          param_grid=dict(param_grid, max_features=[select_X_train.shape[1]]), scoring='recall_weighted', cv=5)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred1 = cross_val_predict(CV_rfc, select_X_test1, y_test1, cv=5)
    print('Overall AUC:', roc_auc_score(y_test1, y_pred1))

    predictions = [round(value) for value in y_pred1]
    accuracy = accuracy_score(y_test1, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test1, y_pred1)
    print(confusion_matrix)
    print(classification_report(y_test1, y_pred1))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

    y_pred2 = CV_rfc.predict(select_X_test2)
    print('Overall AUC:', roc_auc_score(y_test2, y_pred2))

    predictions = [round(value) for value in y_pred2]
    accuracy = accuracy_score(y_test2, y_pred2)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test2, y_pred2)
    print(confusion_matrix)
    print(classification_report(y_test2, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

    y_pred3 = CV_rfc.predict(select_X_test3)
    print('Overall AUC:', roc_auc_score(y_test3, y_pred3))

    predictions = [round(value) for value in y_pred3]
    accuracy = accuracy_score(y_test3, y_pred3)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test3, y_pred3)
    print(confusion_matrix)
    print(classification_report(y_test3, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

    # ROC Curve
    xf1_roc_auc = roc_auc_score(y_test1, y_pred1)
    xf2_roc_auc = roc_auc_score(y_test2, y_pred2)
    xf3_roc_auc = roc_auc_score(y_test3, y_pred3)
    fpr1, tpr1, thresholds1 = roc_curve(y_test1,
                                        cross_val_predict(CV_rfc, select_X_test1, y_test1, method='predict_proba',
                                                          cv=5)[:, 1])
    fpr2, tpr2, thresholds2 = roc_curve(y_test2, CV_rfc.predict_proba(select_X_test2)[:, 1])
    fpr3, tpr3, thresholds3 = roc_curve(y_test3, CV_rfc.predict_proba(select_X_test3)[:, 1])
    plt.figure()
    plt.plot(fpr2, tpr2, linestyle=':', color='green', label='SC (area = %0.2f)' % xf2_roc_auc)
    plt.plot(fpr3, tpr3, linestyle='-.', color='purple', label='YP (area = %0.2f)' % xf3_roc_auc)
    plt.plot(fpr1, tpr1, linestyle='--', color='blue', label='BS (area = %0.2f)' % xf1_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Random Forest trained on BS using XGBoost')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/randomForest/test2/randomForestROCXGBoostBS%d' % select_X_train.shape[1])
    print("Finished running SVC for feature no. (XGBoost) %d\n\n\n" % select_X_train.shape[1])
