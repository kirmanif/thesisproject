import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from numpy import sort
from matplotlib import pyplot
from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest, RFE
from sklearn.feature_selection import chi2
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

plt.rc("font", size=14)

filename = "data/featuresYP.csv"
df1 = pd.read_csv(filename, header=None)
filename = "data/featuresYS.csv"
df2 = pd.read_csv(filename, header=None)
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

X = df1.iloc[:, 0:36]
y = df1.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df1.sample(frac=1), [int(0.8 * len(df1))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

penalty = ['l1', 'l2']

C = list(np.power(5.0, np.arange(-100, 100)))

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty)

print('Logisitic Regression Classification')

for a in range(6, 10):
    print("For feature no. %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X_train, y_train)
    np.set_printoptions(precision=3)
    arr = fit.scores_
    # print(arr)
    features = fit.transform(X_train)
    # print(features[0:5,:])
    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test = df.ix[:, col_test[0:a]].values
    y_test = df.ix[:, 36].values

    data = df_train_balanced.ix[:, col_test[0:a]].values
    data = pd.DataFrame(data)

    # Applying PCA
    from sklearn.decomposition import PCA

    # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
    pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
              random_state=88888)
    Xtrainpca = pca.fit_transform(data)
    Xtestpca = pca.transform(X_test)
    explained_variance = pca.explained_variance_ratio_

    # Train Regression Model with PCA
    from sklearn.linear_model import LogisticRegression

    classifier = GridSearchCV(LogisticRegression(random_state=0), hyperparameters, verbose=0, scoring='roc_auc', cv=5)
    classifier.fit(Xtrainpca, y_train)
    # View best hyperparameters
    print('Best Penalty:', classifier.best_estimator_.get_params()['penalty'])
    print('Best C:', classifier.best_estimator_.get_params()['C'])

    # Predict Results from PCA Model
    y_pred = classifier.predict(Xtestpca)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, y_pred))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, classifier.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test, classifier.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running PCA for feature no. (Univariate FS) %d\n\n\n" % a)

for b in range(6, 10):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_train, y_train)
    print("Num Features: %d" % (fitRFE.n_features_,))
    # print("Selected Features: %s" % (fitRFE.support_,))
    # print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X_train)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df.iloc[:, col_test[0:b]]
    y_testRFE = df.iloc[:, 36]

    dataRFE = df_train_balanced.ix[:, col_test[0:b]].values
    dataRFE = pd.DataFrame(dataRFE)

    # Applying PCA
    from sklearn.decomposition import PCA

    # pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='randomized', tol=0.0, iterated_power='auto', random_state=777)
    pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
              random_state=88888)
    XtrainRFEpca = pca.fit_transform(dataRFE)
    XtestRFEpca = pca.transform(X_testRFE)
    explained_variance = pca.explained_variance_ratio_

    # Train Regression Model with PCA
    from sklearn.linear_model import LogisticRegression

    classifier = GridSearchCV(LogisticRegression(random_state=0), hyperparameters, verbose=0, scoring='roc_auc', cv=5)
    classifier.fit(XtrainRFEpca, y_train)
    # View best hyperparameters
    print('Best Penalty:', classifier.best_estimator_.get_params()['penalty'])
    print('Best C:', classifier.best_estimator_.get_params()['C'])

    # Predict Results from PCA Model
    y_predRFE = classifier.predict(XtestRFEpca)

    # for i in xrange(0, 5):
    #    print("Actual outcome :: {} and Predicted outcome :: {}".format(list(y_validate)[i], predictions[i]))

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE, y_predRFE))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, classifier.predict(XtestRFEpca))
    fpr, tpr, thresholds = roc_curve(y_test, classifier.predict_proba(XtestRFEpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running PCA for feature no. (RFE) %d\n\n\n" % b)

########################################################################################################################

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_train, y_train)
# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()

print('Logisitic Regression Classification')

# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    select_X_test = selection.transform(X_test)

    # dataXG = df_train_balanced.ix[:, col_test[0:select_X_train.shape[1]]].values
    # dataXG = pd.DataFrame(dataXG)

    # Applying PCA
    from sklearn.decomposition import PCA

    selection_model = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
                          random_state=88888)
    XtrainXGpca = selection_model.fit_transform(select_X_train)
    XtestXGpca = selection_model.transform(select_X_test)
    explained_variance = selection_model.explained_variance_ratio_

    # Train Regression Model with PCA
    from sklearn.linear_model import LogisticRegression

    classifier = GridSearchCV(LogisticRegression(random_state=0), hyperparameters, verbose=0, scoring='roc_auc', cv=5)
    classifier.fit(XtrainXGpca, y_train)
    # View best hyperparameters
    print('Best Penalty:', classifier.best_estimator_.get_params()['penalty'])
    print('Best C:', classifier.best_estimator_.get_params()['C'])

    # Predict Results from PCA Model
    y_pred = classifier.predict(XtestXGpca)
    # eval model

    # y_pred = selection_model.predict(XtestXG)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, classifier.predict(XtestXGpca))
    fpr, tpr, thresholds = roc_curve(y_test, classifier.predict_proba(XtestXGpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

########################################################################################################################

param_grid = {
    # 'n_estimators': list(range(300,1000,200)),
    'n_estimators': [500],
    'max_features': ['log2'],
    'criterion': ['gini'],
    'min_samples_split': [5],
    'bootstrap': [True],
    'oob_score': [True],
    'n_jobs': [-1],
    'random_state': [888888],
    'warm_start': [False],
    'class_weight': ['balanced_subsample']

}
X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_train, y_train)
# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()

print('Random Forest Classification')

thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    select_X_test = selection.transform(X_test)

    # dataXG = df_train_balanced.ix[:, col_test[0:select_X_train.shape[1]]].values
    # dataXG = pd.DataFrame(dataXG)

    # Applying PCA
    from sklearn.decomposition import PCA

    selection_model = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
                          random_state=88888)
    Xtrainpca = selection_model.fit_transform(X_train)
    Xtestpca = selection_model.transform(X_test)
    explained_variance = selection_model.explained_variance_ratio_

    # Train Random Forest Model with PCA
    trained_model = RandomForestClassifier()
    classifier = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    classifier.fit(Xtrainpca, y_train)

    # Predict Results from PCA Model
    y_pred = classifier.predict(Xtestpca)
    # eval model

    # y_pred = selection_model.predict(XtestXG)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    sensitivity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    specificity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    # ROC Curve
    rf_roc_auc = roc_auc_score(y_test, classifier.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test, classifier.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Random Forest (area = %0.2f)' % rf_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, sensitivity: %.2f%%, specificity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, sensitivity * 100.0, specificity * 100.0,
        sensitivity * specificity * 100.0))

########################################################################################################################

param_grid_DT = {
    'splitter': ['best'],
    'criterion': ['gini'],
    'min_samples_split': [5],
    'random_state': [888888],
    'class_weight': ['balanced']

}
X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_train, y_train)
# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()

print('Decision Tree Classification')

thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    select_X_test = selection.transform(X_test)

    # dataXG = df_train_balanced.ix[:, col_test[0:select_X_train.shape[1]]].values
    # dataXG = pd.DataFrame(dataXG)

    # Applying PCA
    from sklearn.decomposition import PCA

    selection_model = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto',
                          random_state=88888)
    Xtrainpca = selection_model.fit_transform(X_train)
    Xtestpca = selection_model.transform(X_test)
    explained_variance = selection_model.explained_variance_ratio_

    # Train Random Forest Model with PCA
    trained_model = DecisionTreeClassifier()
    classifier = GridSearchCV(estimator=trained_model, param_grid=param_grid_DT, scoring='roc_auc', cv=5)
    classifier.fit(Xtrainpca, y_train)

    # Predict Results from PCA Model
    y_pred = classifier.predict(Xtestpca)
    # eval model

    # y_pred = selection_model.predict(XtestXG)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    sensitivity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    specificity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    # ROC Curve
    dt_roc_auc = roc_auc_score(y_test, classifier.predict(Xtestpca))
    fpr, tpr, thresholds = roc_curve(y_test, classifier.predict_proba(Xtestpca)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Decision Tree (area = %0.2f)' % dt_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, sensitivity: %.2f%%, specificity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, sensitivity * 100.0, specificity * 100.0,
        sensitivity * specificity * 100.0))
