prot_dict = {}
fasta = open("data.fasta", "r")
fastaLines = fasta.readlines()
nlines = len(fastaLines)
val = ""
fullline = ""
for i in range(nlines):
    line = fastaLines[i]
    if line.startswith(">"):
        if val:
            prot_dict[lbl] = fullline + val
        val = ""
        splts = line.split(" ")
        first_token = splts[0]
        splts = first_token.split(">")
        second_token = splts[1]
        lbl = second_token
        fullline = line

    else:
        val = val + line

if val:
    prot_dict[lbl] = val

f = open("peptidesResultsUniqBlastShortwithDNDigest.out", "r")
lines = f.readlines()
nlines = len(lines)
for i in range(nlines):
    line = lines[i]
    if line.startswith(">"):
        splts = line.split(" ")
        first_token = splts[0]
        splts = first_token.split(">")
        second_token = splts[1]
        lbl = second_token
    if "(100%), Positives =" in line and " (0%)\n" in line:
        splts = lines[i+2].split()
        if lbl in prot_dict:
            print("%s" % (prot_dict[lbl].strip()))
