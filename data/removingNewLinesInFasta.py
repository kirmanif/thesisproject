fastaFile="dataOriginal.fasta"

with open(fastaFile) as f:
    fastaLines = f.readlines()
fastaLines = [x.strip() for x in fastaLines] 

proteins=[]

protein=""
for fastaLine in fastaLines:
    if not fastaLine.startswith(">"):
        #Concatenate line to make a protein string
        protein += fastaLine.strip()
    else:

        if protein:
            proteins.append(protein)
        lbl = fastaLine.split(" ")[0].split(">")[1]
        protein = ">" + lbl + " --\n"

proteins_copy = []
proteins_copy.extend(proteins)
#print("Num of proteins = " + str(len(proteins)))
#print("Num of proteins copy = " + str(len(proteins_copy)))
for idx, val in enumerate(proteins):
    print(proteins_copy[idx])
    #print(val.strip())

