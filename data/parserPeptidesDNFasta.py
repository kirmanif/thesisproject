fastaFile="dataOriginal.fasta"
csvFile="gpmdbPeptides.csv"

with open(fastaFile) as f:
    fastaLines = f.readlines()
fastaLines = [x.strip() for x in fastaLines] 

with open(csvFile) as f:
    csvLines = f.readlines()
csvLines = [x.strip() for x in csvLines] 

proteins=[]

protein=""
for fastaLine in fastaLines:
    if not fastaLine.startswith(">"):
        #Concatenate line to make a protein string
        protein += fastaLine.strip()
    else:
        
        if protein:
            
            proteins.append(protein)
        protein = ""

for substr in csvLines:
    for protein in proteins:
        if substr in protein:
            #print ("debug: " + protein + " has " + substr)
            print(substr)

