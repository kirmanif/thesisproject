f = open("peptidesResultsUniqBlastShortwithDNDigest.out", "r")
lines = f.readlines()
nlines = len(lines)
for i in range(nlines):
    line = lines[i]
    if line.startswith(">"):
        splts = line.split(" ")
        first_token = splts[0]
        splts = first_token.split(">")
        second_token = splts[1]
        lbl = second_token
    if "(100%), Positives =" in line and " (0%)\n" in line:
        splts = lines[i+2].split()
        print("%s,%s,%s,%s" % (lbl, splts[1], splts[2], splts[3]))
