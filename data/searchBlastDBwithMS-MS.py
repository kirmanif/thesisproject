import pandas as pd

##create two dataframes to merge
filename1 = "/home/fkirmani/blastdb/bacillus3/bacillusDNDigest.csv"
df1 = pd.read_csv(filename1, header=None)
dff1 = df1.drop_duplicates(df1, keep="first")
filename2 = "/home/fkirmani/blastdb/bacillus3/peptidesShortBlastUniq.csv"
df2 = pd.read_csv(filename2, header=None)
dff2 = df2.drop_duplicates(df2, keep="first")
filename3 = "/home/fkirmani/blastdb/bacillus3/bacillusDNDigest.csv"
df22 = pd.read_csv(filename3, header=None)
dff22 = df22.drop_duplicates(df22, keep="first")

#filename3 = "/home/fkirmani/blastdb/bacillus/bacillusPeptidesResultsCleaned.csv"
#df22 = pd.read_csv(filename3, header=None)
#dff22 = df22.drop_duplicates(df22, keep="first")
#dff22.to_csv('/home/fkirmani/blastdb/bacillus/bacillusPeptidesResultsCleaned.csv', sep=',')

##merge on columns
dff3 = pd.merge(dff1, dff2, on=[0, 0], how="inner", indicator=True)
dff4 = dff3[dff3['_merge'] == 'both']
dff5 = dff4.iloc[:, 0]


dff4.to_csv('/home/fkirmani/blastdb/bacillus3/peptidesMatchedInBlast.csv', sep=',')
dff6 = pd.merge(dff22, dff2, on=[0, 0], how="outer", indicator=True)
dff7 = dff6[dff6['_merge'] == 'left_only']
dff8 = dff7.iloc[:, 0]

dff7.to_csv('/home/fkirmani/blastdb/bacillus3/peptidesNotFoundInBlast.csv', sep=',')

# filename = "/home/fkirmani/blastdb/work/peptidesIDCleanedCount_2.txt"
# df1 = pd.read_csv(filename, header=None)
# filename = "/home/fkirmani/blastdb/longPeptidesProteo.csv"
# df2 = pd.read_csv(filename, header=None)
# filename = "/home/fkirmani/blastdb/longPeptidesNonProteo.csv"
# df3 = pd.read_csv(filename, header=None)


# dff1 = df1.drop_duplicates(df1, keep="first")
# dff1.to_csv('/home/fkirmani/blastdb/work/peptidesIDCleanedCount_2.txt')
# dff2 = df2.drop_duplicates(df1, keep=False)
# dff2.to_csv('/home/fkirmani/blastdb/longPeptidesProteoFinal.txt')
# dff3 = df3.drop_duplicates(df1, keep=False)
# dff3.to_csv('/home/fkirmani/blastdb/longPeptidesNonProteoFinal.txt')
