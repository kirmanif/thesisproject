prot_dict = {}
fasta = open("data.fasta", "r")
fastaLines = fasta.readlines()
nlines = len(fastaLines)
val = ""
fullline = ""
for i in range(nlines):
    line = fastaLines[i]
    if line.startswith(">"):
        if val:
            prot_dict[lbl] = fullline + val
        val = ""
        splts = line.split(" ")
        first_token = splts[0]
        splts = first_token.split(">")
        second_token = splts[1]
        lbl = second_token
        fullline = line
    else:
        val = val + line
if val:
    prot_dict[lbl] = val

f = open("peptidesResultsUniqBlastShortwithDNDigest.out", "r")
lines = f.readlines()
nlines = len(lines)
for i in range(nlines):
    line = lines[i]
    if line.startswith(">"):
        splts = line.split(" ")
        first_token = splts[0]
        splts = first_token.split(">")
        second_token = splts[1]
        lbl = second_token
    if "(100%), Positives =" in line and " (0%)\n" in line:
        splts = lines[i+2].split()
        if lbl in prot_dict:
            print("%s" % (prot_dict[lbl].strip()))

print("End of searching proteins from proteom (fasta) file in BlastDB")

fastaFile="dataOriginal.fasta"

with open(fastaFile) as f:
    fastaLines = f.readlines()
fastaLines = [x.strip() for x in fastaLines] 

proteins=[]

protein=""
for fastaLine in fastaLines:
    if not fastaLine.startswith(">"):
        #Concatenate line to make a protein string
        protein += fastaLine.strip()
    else:

        if protein:
            proteins.append(protein)
        lbl = fastaLine.split(" ")[0].split(">")[1]
        protein = ">" + lbl + " --\n"

proteins_copy = []
proteins_copy.extend(proteins)
#print("Num of proteins = " + str(len(proteins)))
#print("Num of proteins copy = " + str(len(proteins_copy)))
for idx, val in enumerate(proteins):
    print(proteins_copy[idx])
    #print(val.strip())

print("End of removing new lines from proteom (fasta) file")

fastaFile="dataOriginal.fasta"
csvFile="gpmdbPeptides.csv"

with open(fastaFile) as f:
    fastaLines = f.readlines()
fastaLines = [x.strip() for x in fastaLines] 

with open(csvFile) as f:
    csvLines = f.readlines()
csvLines = [x.strip() for x in csvLines] 

proteins=[]

protein=""
for fastaLine in fastaLines:
    if not fastaLine.startswith(">"):
        protein += fastaLine.strip()
    else:        
        if protein:    
            proteins.append(protein)
        protein = ""

for substr in csvLines:
    for protein in proteins:
        if substr in protein:
            print ("debug: " + protein + " has " + substr)
            print(substr)

print("End of searching in GPMDB/DeepNovo peptides in proteom (fasta) file")
