#!/usr/bin/env python
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve
from sklearn.model_selection import cross_val_predict

plt.rc("font", size=14)

filename = "data/featuresYP.csv"
df = pd.read_csv(filename, header=None)

X = df.iloc[:, 0:36].values
y = df.iloc[:, 36].values

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

C_range = [1e3]
gamma_range = ['auto']
class_weight = ['balanced']
kernel = ['linear']
decision_function_shape = ['ovo']
probability = [True]
param_grid = dict(gamma=gamma_range, C=C_range, kernel=kernel, class_weight=class_weight,
                  decision_function_shape=decision_function_shape, probability=probability)

col_test = [6, 29, 0, 2, 3, 5, 35]

X_trainSVM = df_train_balanced.iloc[:, col_test[0:7]]
X_testSVM = df_test.iloc[:, col_test[0:7]]
y_testSVM = df_test.iloc[:, 36]

print('Running model for selected features from published paper')

trained_model = SVC()

CV_rfc4 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='recall', cv=5)
CV_rfc4.fit(X_trainSVM, y_train)
print(CV_rfc4.best_params_)
# predictions = CV_rfc4.predict(X_testSVM)
predictions = cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testSVM, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testSVM, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('Specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Finshed running model for selected features from published paper')

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.iloc[:, col_test1[0:6]]
y_testXG = df_test.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.iloc[:, col_test2[0:6]]
y_testRFE = df_test.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.iloc[:, col_test3[0:6]]
y_testUN = df_test.iloc[:, 36]

X_train1 = df_train_balanced.ix[:, col_test3[0:6]].values
X_train1 = pd.DataFrame(X_train1)

trained_model = SVC()

CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc.fit(X_train1, y_train)
print(CV_rfc.best_params_)
# predictions = CV_rfc.predict(X_testUN)
predictions = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (Univariate FS)\n\n\n")

X_trainRFE = df_train_balanced.ix[:, col_test2[0:6]].values
X_trainRFE = pd.DataFrame(X_trainRFE)

# Create random forest classifier instance
trained_model = SVC()

CV_rfc2 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc2.fit(X_trainRFE, y_train)
print(CV_rfc2.best_params_)
# predictionsRFE = CV_rfc2.predict(X_testRFE)
predictionsRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (RFE)\n\n\n")

X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

selection_model = SVC()
CV_rfc3 = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc3.fit(X_trainXG, y_train)
print(CV_rfc3.best_params_)

# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5)

predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5))
xf4_roc_auc = roc_auc_score(y_testSVM, cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN,
                                    cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE,
                                    cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG,
                                    cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5, method='predict_proba')[:, 1])
fpr4, tpr4, thresholds4 = roc_curve(y_testSVM,
                                    cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr4, tpr4, linestyle='dashed', color='orange', label='Ahmed Alqurri (area = %0.2f)' % xf4_roc_auc)
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Random Forest trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/svc/svcROC20TestYP')
# plt.show()


col_test = [6, 29, 0, 2, 3, 5, 35]

# X_trainSVM = df_train_balanced.iloc[:, col_test[0:7]]
X_testSVM = df.iloc[:, col_test[0:7]]
y_testSVM = df.iloc[:, 36]

print('Running model for selected features from published paper')

# trained_model = SVC()
#
# CV_rfc4 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='recall', cv=5)
# CV_rfc4.fit(X_trainSVM, y_train)
# print(CV_rfc4.best_params_)
# predictions = CV_rfc4.predict(X_testSVM)
predictions = cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testSVM, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testSVM, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('Specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Finshed running model for selected features from published paper')

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.iloc[:, col_test1[0:6]]
y_testXG = df.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.iloc[:, col_test2[0:6]]
y_testRFE = df.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.iloc[:, col_test3[0:6]]
y_testUN = df.iloc[:, 36]

# X_train1 = df_train_balanced.ix[:, col_test3[0:6]].values
# X_train1 = pd.DataFrame(X_train1)
#
# trained_model = SVC()
#
# CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc.fit(X_train1, y_train)
# print(CV_rfc.best_params_)
# predictions = CV_rfc.predict(X_testUN)
predictions = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (Univariate FS)\n\n\n")

# X_trainRFE = df_train_balanced.ix[:, col_test2[0:6]].values
# X_trainRFE = pd.DataFrame(X_trainRFE)
#
# # Create random forest classifier instance
# trained_model = SVC()
#
# CV_rfc2 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc2.fit(X_trainRFE, y_train)
# print(CV_rfc2.best_params_)
# predictionsRFE = CV_rfc2.predict(X_testRFE)
predictionsRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (RFE)\n\n\n")

# X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
# X_trainXG = pd.DataFrame(X_trainXG)
#
# selection_model = SVC()
# CV_rfc3 = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc3.fit(X_trainXG, y_train)
# print(CV_rfc3.best_params_)

# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5)

predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5))
xf4_roc_auc = roc_auc_score(y_testSVM, cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN,
                                    cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE,
                                    cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG,
                                    cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5, method='predict_proba')[:, 1])
fpr4, tpr4, thresholds4 = roc_curve(y_testSVM,
                                    cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr4, tpr4, linestyle='dashed', color='orange', label='Ahmed Alqurri (area = %0.2f)' % xf4_roc_auc)
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Random Forest trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/svc/svcROCFullTestYP')
# plt.show()

X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]

col_test = [6, 29, 0, 2, 3, 5, 35]

X_trainSVM = df_train.iloc[:, col_test[0:7]]
X_testSVM = df_test.iloc[:, col_test[0:7]]
y_testSVM = df_test.iloc[:, 36]

print('Running model for selected features from published paper')

trained_model = SVC()

CV_rfc4 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='recall', cv=5)
CV_rfc4.fit(X_trainSVM, y_train)
print(CV_rfc4.best_params_)
# predictions = CV_rfc4.predict(X_testSVM)
predictions = cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testSVM, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testSVM, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('Specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Finshed running model for selected features from published paper')

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.iloc[:, col_test1[0:6]]
y_testXG = df_test.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.iloc[:, col_test2[0:6]]
y_testRFE = df_test.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.iloc[:, col_test3[0:6]]
y_testUN = df_test.iloc[:, 36]

X_train1 = df_train.ix[:, col_test3[0:6]].values
X_train1 = pd.DataFrame(X_train1)

trained_model = SVC()

CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc.fit(X_train1, y_train)
print(CV_rfc.best_params_)
# predictions = CV_rfc.predict(X_testUN)
predictions = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (Univariate FS)\n\n\n")

X_trainRFE = df_train.ix[:, col_test2[0:6]].values
X_trainRFE = pd.DataFrame(X_trainRFE)

# Create random forest classifier instance
trained_model = SVC()

CV_rfc2 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc2.fit(X_trainRFE, y_train)
print(CV_rfc2.best_params_)
# predictionsRFE = CV_rfc2.predict(X_testRFE)
predictionsRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (RFE)\n\n\n")

X_trainXG = df_train.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

selection_model = SVC()
CV_rfc3 = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc3.fit(X_trainXG, y_train)
print(CV_rfc3.best_params_)

# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5)

predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5))
xf4_roc_auc = roc_auc_score(y_testSVM, cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN,
                                    cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE,
                                    cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG,
                                    cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5, method='predict_proba')[:, 1])
fpr4, tpr4, thresholds4 = roc_curve(y_testSVM,
                                    cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr4, tpr4, linestyle='dashed', color='orange', label='Ahmed Alqurri (area = %0.2f)' % xf4_roc_auc)
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Random Forest trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/svc/svcROC20TestUnbalYP')
# plt.show()


col_test = [6, 29, 0, 2, 3, 5, 35]

# X_trainSVM = df_train.iloc[:, col_test[0:7]]
X_testSVM = df.iloc[:, col_test[0:7]]
y_testSVM = df.iloc[:, 36]

print('Running model for selected features from published paper')

# trained_model = SVC()
#
# CV_rfc4 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='recall', cv=5)
# CV_rfc4.fit(X_trainSVM, y_train)
# print(CV_rfc4.best_params_)
# predictions = CV_rfc4.predict(X_testSVM)
predictions = cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testSVM, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testSVM, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('Specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Finshed running model for selected features from published paper')

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.iloc[:, col_test1[0:6]]
y_testXG = df.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.iloc[:, col_test2[0:6]]
y_testRFE = df.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.iloc[:, col_test3[0:6]]
y_testUN = df.iloc[:, 36]

# X_train1 = df_train.ix[:, col_test3[0:6]].values
# X_train1 = pd.DataFrame(X_train1)
#
# trained_model = SVC()
#
# CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc.fit(X_train1, y_train)
# print(CV_rfc.best_params_)
# predictions = CV_rfc.predict(X_testUN)
predictions = cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (Univariate FS)\n\n\n")

# X_trainRFE = df_train.ix[:, col_test2[0:6]].values
# X_trainRFE = pd.DataFrame(X_trainRFE)
#
# # Create random forest classifier instance
# trained_model = SVC()
#
# CV_rfc2 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc2.fit(X_trainRFE, y_train)
# print(CV_rfc2.best_params_)
# predictionsRFE = CV_rfc2.predict(X_testRFE)
predictionsRFE = cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)
print("Finished running SVC (RFE)\n\n\n")

# X_trainXG = df_train.ix[:, col_test1[0:6]].values
# X_trainXG = pd.DataFrame(X_trainXG)
#
# selection_model = SVC()
# CV_rfc3 = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
# CV_rfc3.fit(X_trainXG, y_train)
# print(CV_rfc3.best_params_)

# y_pred = CV_rfc3.predict(X_testXG)
y_pred = cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5)

predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5))
xf4_roc_auc = roc_auc_score(y_testSVM, cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN,
                                    cross_val_predict(CV_rfc, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE,
                                    cross_val_predict(CV_rfc2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG,
                                    cross_val_predict(CV_rfc3, X_testXG, y_testXG, cv=5, method='predict_proba')[:, 1])
fpr4, tpr4, thresholds4 = roc_curve(y_testSVM,
                                    cross_val_predict(CV_rfc4, X_testSVM, y_testSVM, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr4, tpr4, linestyle='dashed', color='orange', label='Ahmed Alqurri (area = %0.2f)' % xf4_roc_auc)
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Random Forest trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/svc/svcROCFullTestUnbalYP')
# plt.show()

