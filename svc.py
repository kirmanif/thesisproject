import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import SelectKBest, RFE
from sklearn.feature_selection import chi2

plt.rc("font", size=14)

filename = "data/featuresYP.csv"
df = pd.read_csv(filename, header=None)

X = df.iloc[:, 0:36].values
y = df.iloc[:, 36].values

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

C_range = [1e1, 1e2, 1e3]
gamma_range = [1e1, 1e2, 1e3]
# class_weight = ['balanced']
kernel = ['poly']
decision_function_shape = ['ovo']
param_grid = dict(gamma=gamma_range, C=C_range, kernel=kernel,
                  decision_function_shape=decision_function_shape)

col_test = [6, 29, 0, 2, 3, 5, 35]

X_trainSVM = df_train_balanced.iloc[:, col_test[0:7]]
X_testSVM = df_test.iloc[:, col_test[0:7]]
y_testSVM = df_test.iloc[:, 36]

print('Running model for selected features from published paper')

trained_model = SVC()

CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc.fit(X_trainSVM, y_train)
print(CV_rfc.best_params_)
predictions = CV_rfc.predict(X_testSVM)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testSVM, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testSVM, predictions))
specificity= confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('Specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('Sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Finshed running model for selected features from published paper')

for a in range(6, 15):
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X_train, y_train)
    np.set_printoptions(precision=3)
    arr = fit.scores_

    features = fit.transform(X_train)

    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test = df_test.ix[:, col_test[0:a]].values
    y_test = df_test.ix[:, 36].values

    X_train1 = df_train_balanced.ix[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    trained_model = SVC()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions = CV_rfc.predict(X_test)

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    print("Finished running Random Forest for feature no. (Univariate FS) %d\n\n\n" % a)

for b in range(6, 15):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_train, y_train)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)

    featuresRFE = fitRFE.transform(X_train)

    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df_test.ix[:, col_test[0:b]].values
    y_testRFE = df_test.ix[:, 36].values

    X_trainRFE = df_train_balanced.ix[:, col_test[0:b]].values
    X_trainRFE = pd.DataFrame(X_trainRFE)

    # Create random forest classifier instance
    trained_model = SVC()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(X_trainRFE, y_train)
    print(CV_rfc.best_params_)
    predictionsRFE = CV_rfc.predict(X_testRFE)

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE, predictionsRFE))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    print("Finished running Random Forest for feature no. (RFE) %d\n\n\n" % b)

import matplotlib.pyplot as plt
import numpy as np
from numpy import sort
import pandas as pd

from matplotlib import pyplot
from xgboost import XGBClassifier
from sklearn.ensemble import RandomForestClassifier

from sklearn.metrics import accuracy_score

from sklearn.feature_selection import SelectFromModel

plt.rc("font", size=14)

filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/featuresYPYS.csv"
df = pd.read_csv(filename, header=None)

X = df.iloc[:, 0:36]
y = df.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]
X_test = df_test.iloc[:, 0:36]
y_test = df_test.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_train, y_train)
# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = SVC()
    # eval model
    select_X_test = selection.transform(X_test)

    CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred = CV_rfc.predict(select_X_test)

    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
