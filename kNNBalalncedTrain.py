from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectKBest, RFE
from sklearn.feature_selection import chi2
import matplotlib.pyplot as plt
import numpy as np
from numpy import sort
import pandas as pd
import math as mt
from xgboost import XGBClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.cross_validation import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

plt.rc("font", size=14)

filename = "data/featuresYPYS_2D.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresYS_2D.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYP_2D.csv"
df2 = pd.read_csv(filename2, header=None)
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 4), random_state=123456)
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index)))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]

fawadScoreUN = []
fawadScoreRFE = []
cv = StratifiedKFold(y_train, n_folds=5)
neighbors2 = list(range(5, 70, 1))
selectedFeatures = list(range(6, 20, 1))

for a in selectedFeatures:
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X_trainFeat, y_trainFeat)
    np.set_printoptions(precision=4)
    arr = fit.scores_
    # print(arr)
    features = fit.transform(X_trainFeat)
    # print(features[0:5,:])
    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test = df.ix[:, col_test[0:a]].values
    y_test = df.ix[:, 36].values

    X_train1 = df_train_unbalanced.ix[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    # empty list that will hold cv scores
    cv_scores = []

    # perform 10-fold cross validation
    for k in neighbors2:
        knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=2, leaf_size=30)
        scores = cross_val_score(knn, X_train1, y_train, cv=cv, scoring='roc_auc')
        cv_scores.append(scores.mean())

    # changing to misclassification error
    MSE = [1 - x for x in cv_scores]

    # determining best k
    optimal_k_UN = neighbors2[MSE.index(min(MSE))]
    print("The optimal number of neighbors is %d" % optimal_k_UN)

    knnUN = KNeighborsClassifier(n_neighbors=optimal_k_UN)
    predictions = knnUN.fit(X_train1, y_train).predict(X_test)
    print('Overall AUC:', roc_auc_score(y_test, knnUN.predict(X_test)))

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)

    fawadMatricUN = sensitivity * specificity
    fawadScoreUN.append(fawadMatricUN)

    # ROC Curve
    kn_roc_auc = roc_auc_score(y_test, knnUN.predict(X_test))
    fpr, tpr, thresholds = roc_curve(y_test, knnUN.predict(X_test))
    plt.figure()
    plt.plot(fpr, tpr, label='K-Nearest Neighbour (area = %0.2f)' % kn_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()

    print("Finished running KNN for feature no. (Univariate FS) %d\n\n\n" % a)

# Determining optimal number of features
optimalNoFeaturesUN = selectedFeatures[fawadScoreUN.index(max(fawadScoreUN))]
print("******The optimal number of features is %d******" % optimalNoFeaturesUN)

for b in selectedFeatures:
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X_trainFeat)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df.iloc[:, col_test[0:b]]
    y_testRFE = df.iloc[:, 36]

    X_trainRFE = df_train_unbalanced.ix[:, col_test[0:b]].values
    X_trainRFE = pd.DataFrame(X_trainRFE)

    # empty list that will hold cv scores
    cv_scores = []

    # perform 10-fold cross validation
    for k in neighbors2:
        knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=2, leaf_size=30)
        scores = cross_val_score(knn, X_trainRFE, y_train, cv=cv, scoring='roc_auc')
        cv_scores.append(scores.mean())

    # changing to misclassification error
    MSE = [1 - x for x in cv_scores]

    # determining best k
    optimal_k_RFE = neighbors2[MSE.index(min(MSE))]
    print("The optimal number of neighbors is %d" % optimal_k_RFE)

    knnRFE = KNeighborsClassifier(n_neighbors=optimal_k_RFE)
    predictionsRFE = knnRFE.fit(X_trainRFE, y_train).predict(X_testRFE)
    print('Overall AUC:', roc_auc_score(y_test, knnRFE.predict(X_testRFE)))

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, predictionsRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, predictionsRFE))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)

    # ROC Curve
    kn_roc_auc = roc_auc_score(y_test, knnRFE.predict(X_testRFE))
    fpr, tpr, thresholds = roc_curve(y_test, knnRFE.predict(X_testRFE))
    plt.figure()
    plt.plot(fpr, tpr, label='K-Nearest Neighbour (area = %0.2f)' % kn_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()

    print("Finished running KNN for feature no. (RFE) %d\n\n\n" % b)

    fawadMatricRFE = sensitivity * specificity
    fawadScoreRFE.append(fawadMatricRFE)

# Determing optimal number of features
optimalNoFeaturesRFE = selectedFeatures[fawadScoreRFE.index(max(fawadScoreRFE))]
print("******The optimal number of features is %d******" % optimalNoFeaturesRFE)

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_trainFeat, y_trainFeat)
# make predictions for test data
# y_pred = model.predict(X_test)
# predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model

    # empty list that will hold cv scores
    cv_scores = []

    for k in neighbors2:
        knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=2, leaf_size=30)
        scores = cross_val_score(knn, select_X_train, y_train, cv=cv, scoring='roc_auc')
        cv_scores.append(scores.mean())

    # changing to misclassification error
    MSE = [1 - x for x in cv_scores]

    # determining best k
    optimal_k = neighbors2[MSE.index(min(MSE))]
    print("The optimal number of neighbors is %d" % optimal_k)

    selection_model = KNeighborsClassifier(n_neighbors=optimal_k)
    selection_model.fit(select_X_train, y_train)
    # eval model
    select_X_test = selection.transform(X_test)
    y_pred = selection_model.predict(select_X_test)
    predictions = [round(value) for value in y_pred]
    print('Overall AUC:', roc_auc_score(y_test, selection_model.predict(select_X_test)))
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
    # ROC Curve
    kn_roc_auc = roc_auc_score(y_test, selection_model.predict(select_X_test))
    fpr, tpr, thresholds = roc_curve(y_test, selection_model.predict(select_X_test))
    plt.figure()
    plt.plot(fpr, tpr, label='K-Nearest Neighbour (area = %0.2f)' % kn_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = RandomForestClassifier(n_estimators=500, criterion='gini', max_depth=None, min_samples_split=5,
                               min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='log2',
                               max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, bootstrap=True,
                               oob_score=True, n_jobs=-1, random_state=888888, verbose=0, warm_start=False,
                               class_weight='balanced')
model.fit(X_trainFeat, y_trainFeat)
# make predictions for test data
# y_pred = model.predict(X_test)
# predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model

    # empty list that will hold cv scores
    cv_scores = []

    for k in neighbors2:
        knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=2, leaf_size=30)
        scores = cross_val_score(knn, select_X_train, y_train, cv=cv, scoring='roc_auc')
        cv_scores.append(scores.mean())

    # changing to misclassification error
    MSE = [1 - x for x in cv_scores]

    # determining best k
    optimal_k = neighbors2[MSE.index(min(MSE))]
    print("The optimal number of neighbors is %d" % optimal_k)

    selection_model = KNeighborsClassifier(n_neighbors=optimal_k)
    selection_model.fit(select_X_train, y_train)
    # eval model
    select_X_test = selection.transform(X_test)
    y_pred = selection_model.predict(select_X_test)
    predictions = [round(value) for value in y_pred]
    print('Overall AUC:', roc_auc_score(y_test, selection_model.predict(select_X_test)))
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
    # ROC Curve
    kn_roc_auc = roc_auc_score(y_test, selection_model.predict(select_X_test))
    fpr, tpr, thresholds = roc_curve(y_test, selection_model.predict(select_X_test))
    plt.figure()
    plt.plot(fpr, tpr, label='K-Nearest Neighbour (area = %0.2f)' % kn_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
