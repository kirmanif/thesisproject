import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.neighbors import KNeighborsClassifier

plt.rc("font", size=14)

filename = "data/featuresYS.csv"
df = pd.read_csv(filename, header=None)

X = df.iloc[:, 0:36]
y = df.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))

X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

# col_test1 = [35, 1, 9, 6, 0, 14]
col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.iloc[:, col_test1[0:6]]
y_testXG = df_test.iloc[:, 36]

# col_test2 = [35, 7, 12, 9, 8, 2]
col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.iloc[:, col_test2[0:6]]
y_testRFE = df_test.iloc[:, 36]

# col_test3 = [29, 35, 16, 6, 1, 23]
col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.iloc[:, col_test3[0:6]]
y_testUN = df_test.iloc[:, 36]

fawadScoreUN = []
fawadScoreRFE = []

X_train1 = df_train_balanced.ix[:, col_test3[0:6]].values
X_train1 = pd.DataFrame(X_train1)

# creating odd list of K for KNN
# subsetting just the odd ones
neighbors = list(range(5, 30, 1))

# empty list that will hold cv scores
cv_scores = []

# perform 10-fold cross validation
for k in neighbors:
    knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
    scores = cross_val_score(knn, X_train1, y_train, cv=4, scoring='roc_auc')
    cv_scores.append(scores.mean())

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k_UN = neighbors[MSE.index(min(MSE))]
print("The optimal number of neighbors is %d" % optimal_k_UN)

knnUN = KNeighborsClassifier(n_neighbors=optimal_k_UN)
predictions = knnUN.fit(X_train1, y_train).predict(X_testUN)
# predictions = knnUN.predict(X_testUN)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testUN, predictions))
confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (Univariate FS)\n\n\n")

X_trainRFE = df_train_balanced.ix[:, col_test2[0:6]].values
X_trainRFE = pd.DataFrame(X_trainRFE)

# creating odd list of K for KNN
# subsetting just the odd ones
neighbors = list(range(5, 30, 1))

# empty list that will hold cv scores
cv_scores = []

# perform 10-fold cross validation
for k in neighbors:
    knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
    scores = cross_val_score(knn, X_trainRFE, y_train, cv=4, scoring='roc_auc')
    cv_scores.append(scores.mean())

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k_RFE = neighbors[MSE.index(min(MSE))]
print("The optimal number of neighbors is %d" % optimal_k_RFE)

knnRFE = KNeighborsClassifier(n_neighbors=optimal_k_RFE)
predictionsRFE = knnRFE.fit(X_trainRFE, y_train).predict(X_testRFE)
# predictionsRFE = knnRFE.predict(X_testRFE)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testRFE, predictionsRFE))
confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (RFE)\n\n\n")

X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)
# train model
neighbors2 = list(range(5, 30, 1))

# empty list that will hold cv scores
cv_scores = []

for k in neighbors2:
    knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
    scores = cross_val_score(knn, X_trainXG, y_train, cv=4, scoring='roc_auc')
    cv_scores.append(scores.mean())

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k = neighbors2[MSE.index(min(MSE))]
print("The optimal number of neighbors is %d" % optimal_k)

selection_model = KNeighborsClassifier(n_neighbors=optimal_k)
selection_model.fit(X_trainXG, y_train)
# eval model
y_pred = selection_model.predict(X_testXG)
# y_pred = selection_model.predict(X_testXG)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

print('Overall AUC:', roc_auc_score(y_testXG, predictions))
confusion_matrix = confusion_matrix(y_testXG, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, predictions)
xf2_roc_auc = roc_auc_score(y_testRFE, predictionsRFE)
xf3_roc_auc = roc_auc_score(y_testXG, y_pred)
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, knnUN.predict_proba(X_testUN)[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, knnRFE.predict_proba(X_testRFE)[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, selection_model.predict_proba(X_testXG)[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('k-nearest neighbor trained on SC')
plt.legend(loc="lower right")
plt.savefig('Pictures/knn/knnROC20TestSC')
# plt.show()

X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

# col_test1 = [35, 1, 9, 6, 0, 14]
col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.iloc[:, col_test1[0:6]]
y_testXG = df.iloc[:, 36]

# col_test2 = [35, 7, 12, 9, 8, 2]
col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.iloc[:, col_test2[0:6]]
y_testRFE = df.iloc[:, 36]

# col_test3 = [29, 35, 16, 6, 1, 23]
col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.iloc[:, col_test3[0:6]]
y_testUN = df.iloc[:, 36]

# fawadScoreUN = []
# fawadScoreRFE = []
#
# X_train1 = df_train_balanced.ix[:, col_test3[0:6]].values
# X_train1 = pd.DataFrame(X_train1)
#
# # creating odd list of K for KNN
# # subsetting just the odd ones
# neighbors = list(range(5, 30, 1))
#
# # empty list that will hold cv scores
# cv_scores = []
#
# # perform 10-fold cross validation
# for k in neighbors:
#     knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
#     scores = cross_val_score(knn, X_train1, y_train, cv=10, scoring='roc_auc')
#     cv_scores.append(scores.mean())
#
# # changing to misclassification error
# MSE = [1 - x for x in cv_scores]
#
# # determining best k
# optimal_k_UN = neighbors[MSE.index(min(MSE))]
# print("The optimal number of neighbors is %d" % optimal_k_UN)
#
# knnUN = KNeighborsClassifier(n_neighbors=optimal_k_UN)
# predictions = knnUN.fit(X_train1, y_train).predict(X_testUN)
predictions = cross_val_predict(knnUN, X_testUN, y_testUN, cv=4)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testUN, knnUN.predict(X_testUN)))
confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (Univariate FS)\n\n\n")

# X_trainRFE = df_train_balanced.ix[:, col_test2[0:6]].values
# X_trainRFE = pd.DataFrame(X_trainRFE)
#
# # creating odd list of K for KNN
# # subsetting just the odd ones
# neighbors = list(range(5, 30, 1))
#
# # empty list that will hold cv scores
# cv_scores = []
#
# # perform 10-fold cross validation
# for k in neighbors:
#     knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
#     scores = cross_val_score(knn, X_trainRFE, y_train, cv=10, scoring='roc_auc')
#     cv_scores.append(scores.mean())
#
# # changing to misclassification error
# MSE = [1 - x for x in cv_scores]
#
# # determining best k
# optimal_k_RFE = neighbors[MSE.index(min(MSE))]
# print("The optimal number of neighbors is %d" % optimal_k_RFE)
#
# knnRFE = KNeighborsClassifier(n_neighbors=optimal_k_RFE)
# predictionsRFE = knnRFE.fit(X_trainRFE, y_train).predict(X_testRFE)
predictionsRFE = cross_val_predict(knnRFE, X_testRFE, y_testRFE, cv=4)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testRFE, knnRFE.predict(X_testRFE)))
confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (RFE)\n\n\n")

# X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
# X_trainXG = pd.DataFrame(X_trainXG)
# # train model
# neighbors2 = list(range(5, 30, 1))
#
# # empty list that will hold cv scores
# cv_scores = []
#
# for k in neighbors2:
#     knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
#     scores = cross_val_score(knn, X_trainXG, y_train, cv=10, scoring='roc_auc')
#     cv_scores.append(scores.mean())
#
# # changing to misclassification error
# MSE = [1 - x for x in cv_scores]
#
# # determining best k
# optimal_k = neighbors2[MSE.index(min(MSE))]
# print("The optimal number of neighbors is %d" % optimal_k)
#
# selection_model = KNeighborsClassifier(n_neighbors=optimal_k)
# selection_model.fit(X_trainXG, y_train)
# # eval model
# y_pred = selection_model.predict(X_testXG)
y_pred = cross_val_predict(selection_model, X_testXG, y_testXG, cv=4)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

print('Overall AUC:', roc_auc_score(y_testXG, selection_model.predict(X_testXG)))
confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, predictions)
xf2_roc_auc = roc_auc_score(y_testRFE, predictionsRFE)
xf3_roc_auc = roc_auc_score(y_testXG, y_pred)
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(knnUN, X_testUN, y_testUN, cv=4, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(knnRFE, X_testRFE, y_testRFE, cv=4, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(selection_model, X_testXG, y_testRFE, cv=4, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('k-nearest neighbor trained on SC')
plt.legend(loc="lower right")
plt.savefig('Pictures/knn/knnROCFullTestSC')
# plt.show()


X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]

# col_test1 = [35, 1, 9, 6, 0, 14]
col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.iloc[:, col_test1[0:6]]
y_testXG = df_test.iloc[:, 36]

# col_test2 = [35, 7, 12, 9, 8, 2]
col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.iloc[:, col_test2[0:6]]
y_testRFE = df_test.iloc[:, 36]

# col_test3 = [29, 35, 16, 6, 1, 23]
col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.iloc[:, col_test3[0:6]]
y_testUN = df_test.iloc[:, 36]

fawadScoreUN = []
fawadScoreRFE = []

X_train1 = df_train.ix[:, col_test3[0:6]].values
X_train1 = pd.DataFrame(X_train1)

# creating odd list of K for KNN
# subsetting just the odd ones
neighbors = list(range(5, 30, 1))

# empty list that will hold cv scores
cv_scores = []

# perform 10-fold cross validation
for k in neighbors:
    knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
    scores = cross_val_score(knn, X_train1, y_train, cv=4, scoring='roc_auc')
    cv_scores.append(scores.mean())

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k_UN = neighbors[MSE.index(min(MSE))]
print("The optimal number of neighbors is %d" % optimal_k_UN)

knnUN = KNeighborsClassifier(n_neighbors=optimal_k_UN)
predictions = knnUN.fit(X_train1, y_train).predict(X_testUN)
# predictions = knnUN.predict(X_testUN)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testUN, predictions))
confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (Univariate FS)\n\n\n")

X_trainRFE = df_train.ix[:, col_test2[0:6]].values
X_trainRFE = pd.DataFrame(X_trainRFE)

# creating odd list of K for KNN
# subsetting just the odd ones
neighbors = list(range(5, 30, 1))

# empty list that will hold cv scores
cv_scores = []

# perform 10-fold cross validation
for k in neighbors:
    knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
    scores = cross_val_score(knn, X_trainRFE, y_train, cv=4, scoring='roc_auc')
    cv_scores.append(scores.mean())

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k_RFE = neighbors[MSE.index(min(MSE))]
print("The optimal number of neighbors is %d" % optimal_k_RFE)

knnRFE = KNeighborsClassifier(n_neighbors=optimal_k_RFE)
predictionsRFE = knnRFE.fit(X_trainRFE, y_train).predict(X_testRFE)
# predictionsRFE = knnRFE.predict(X_testRFE)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testRFE, predictionsRFE))
confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (RFE)\n\n\n")

X_trainXG = df_train.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)
# train model
neighbors2 = list(range(5, 30, 1))

# empty list that will hold cv scores
cv_scores = []

for k in neighbors2:
    knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
    scores = cross_val_score(knn, X_trainXG, y_train, cv=4, scoring='roc_auc')
    cv_scores.append(scores.mean())

# changing to misclassification error
MSE = [1 - x for x in cv_scores]

# determining best k
optimal_k = neighbors2[MSE.index(min(MSE))]
print("The optimal number of neighbors is %d" % optimal_k)

selection_model = KNeighborsClassifier(n_neighbors=optimal_k)
selection_model.fit(X_trainXG, y_train)
# eval model
y_pred = selection_model.predict(X_testXG)
# y_pred = selection_model.predict(X_testXG)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

print('Overall AUC:', roc_auc_score(y_testXG, predictions))
confusion_matrix = confusion_matrix(y_testXG, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, predictions)
xf2_roc_auc = roc_auc_score(y_testRFE, predictionsRFE)
xf3_roc_auc = roc_auc_score(y_testXG, y_pred)
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, knnUN.predict_proba(X_testUN)[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, knnRFE.predict_proba(X_testRFE)[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, selection_model.predict_proba(X_testXG)[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('k-nearest neighbor trained on SC')
plt.legend(loc="lower right")
plt.savefig('Pictures/knn/knnROC20TestUnbalSC')
# plt.show()

X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]

# col_test1 = [35, 1, 9, 6, 0, 14]
col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.iloc[:, col_test1[0:6]]
y_testXG = df.iloc[:, 36]

# col_test2 = [35, 7, 12, 9, 8, 2]
col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.iloc[:, col_test2[0:6]]
y_testRFE = df.iloc[:, 36]

# col_test3 = [29, 35, 16, 6, 1, 23]
col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.iloc[:, col_test3[0:6]]
y_testUN = df.iloc[:, 36]

# fawadScoreUN = []
# fawadScoreRFE = []
#
# X_train1 = df_train.ix[:, col_test3[0:6]].values
# X_train1 = pd.DataFrame(X_train1)
#
# # creating odd list of K for KNN
# # subsetting just the odd ones
# neighbors = list(range(5, 30, 1))
#
# # empty list that will hold cv scores
# cv_scores = []
#
# # perform 10-fold cross validation
# for k in neighbors:
#     knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
#     scores = cross_val_score(knn, X_train1, y_train, cv=10, scoring='roc_auc')
#     cv_scores.append(scores.mean())
#
# # changing to misclassification error
# MSE = [1 - x for x in cv_scores]
#
# # determining best k
# optimal_k_UN = neighbors[MSE.index(min(MSE))]
# print("The optimal number of neighbors is %d" % optimal_k_UN)
#
# knnUN = KNeighborsClassifier(n_neighbors=optimal_k_UN)
# predictions = knnUN.fit(X_train1, y_train).predict(X_testUN)
predictions = cross_val_predict(knnUN, X_testUN, y_testUN, cv=4)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testUN, knnUN.predict(X_testUN)))
confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testUN, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (Univariate FS)\n\n\n")

# X_trainRFE = df_train.ix[:, col_test2[0:6]].values
# X_trainRFE = pd.DataFrame(X_trainRFE)
#
# # creating odd list of K for KNN
# # subsetting just the odd ones
# neighbors = list(range(5, 30, 1))
#
# # empty list that will hold cv scores
# cv_scores = []
#
# # perform 10-fold cross validation
# for k in neighbors:
#     knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
#     scores = cross_val_score(knn, X_trainRFE, y_train, cv=10, scoring='roc_auc')
#     cv_scores.append(scores.mean())
#
# # changing to misclassification error
# MSE = [1 - x for x in cv_scores]
#
# # determining best k
# optimal_k_RFE = neighbors[MSE.index(min(MSE))]
# print("The optimal number of neighbors is %d" % optimal_k_RFE)
#
# knnRFE = KNeighborsClassifier(n_neighbors=optimal_k_RFE)
# predictionsRFE = knnRFE.fit(X_trainRFE, y_train).predict(X_testRFE)
predictionsRFE = cross_val_predict(knnRFE, X_testRFE, y_testRFE, cv=4)

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

print('Overall AUC:', roc_auc_score(y_testRFE, knnRFE.predict(X_testRFE)))
confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testRFE, predictionsRFE))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print("Finished running Random Forest (RFE)\n\n\n")

# X_trainXG = df_train.ix[:, col_test1[0:6]].values
# X_trainXG = pd.DataFrame(X_trainXG)
# # train model
# neighbors2 = list(range(5, 30, 1))
#
# # empty list that will hold cv scores
# cv_scores = []
#
# for k in neighbors2:
#     knn = KNeighborsClassifier(n_neighbors=k, weights='distance', algorithm='brute', p=3, leaf_size=30)
#     scores = cross_val_score(knn, X_trainXG, y_train, cv=10, scoring='roc_auc')
#     cv_scores.append(scores.mean())
#
# # changing to misclassification error
# MSE = [1 - x for x in cv_scores]
#
# # determining best k
# optimal_k = neighbors2[MSE.index(min(MSE))]
# print("The optimal number of neighbors is %d" % optimal_k)
#
# selection_model = KNeighborsClassifier(n_neighbors=optimal_k)
# selection_model.fit(X_trainXG, y_train)
# # eval model
# y_pred = selection_model.predict(X_testXG)
y_pred = cross_val_predict(selection_model, X_testXG, y_testXG, cv=4)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

print('Overall AUC:', roc_auc_score(y_testXG, selection_model.predict(X_testXG)))
confusion_matrix = confusion_matrix(y_testXG, y_pred)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, predictions)
xf2_roc_auc = roc_auc_score(y_testRFE, predictionsRFE)
xf3_roc_auc = roc_auc_score(y_testXG, y_pred)
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(knnUN, X_testUN, y_testUN, cv=4, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(knnRFE, X_testRFE, y_testRFE, cv=4, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(selection_model, X_testXG, y_testRFE, cv=4, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('k-nearest neighbor trained on SC')
plt.legend(loc="lower right")
plt.savefig('Pictures/knn/knnROCFullTestUnbalSC')
# plt.show()
