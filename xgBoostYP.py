import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from xgboost import XGBClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import GridSearchCV

plt.rc("font", size=14)

filename = "data/featuresYP.csv"
df = pd.read_csv(filename, header=None)
df[36] = df[36].map({1: 1, -1: 0})
print(df.shape)

X = df.iloc[:, 0:36]
y = df.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == 0]
print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

max_depth = [3]
learning_rate = [0.08]
n_estimators = [236]
silent = ['True']
objective = ['binary:logistic']
booster = ['gbtree']
n_jobs = [-1]
nthread = [4]
gamma = [5]
min_child_weight = [3]
max_delta_step = [5]
subsample = [0.5]
colsample_bytree = [1]
colsample_bylevel = [1]
reg_alpha = [1]
reg_lambda = [1]
# scale_pos_weight = [len(df_train_balanced_MinusOne1.index) / len(df_train_balanced_One.index)]
scale_pos_weight = [len(df_train_balanced_MinusOne1.index) / len(df_train_balanced_One.index)]
base_score = [0.25]
random_state = [7777]
param_grid = dict(max_depth=max_depth, learning_rate=learning_rate, n_estimators=n_estimators, silent=silent,
                  objective=objective, booster=booster, n_jobs=n_jobs, nthread=nthread, gamma=gamma,
                  min_child_weight=min_child_weight, max_delta_step=max_delta_step, subsample=subsample,
                  colsample_bytree=colsample_bytree, colsample_bylevel=colsample_bylevel, reg_alpha=reg_alpha,
                  reg_lambda=reg_lambda, scale_pos_weight=scale_pos_weight, base_score=base_score,
                  random_state=random_state)

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.ix[:, col_test1[0:6]].values
X_testXG = pd.DataFrame(X_testXG)
y_testXG = df_test.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.ix[:, col_test2[0:6]].values
X_testRFE = pd.DataFrame(X_testRFE)
y_testRFE = df_test.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.ix[:, col_test3[0:6]].values
X_testUN = pd.DataFrame(X_testUN)
y_testUN = df_test.iloc[:, 36]

print("********Univariate Analysis********")
X_train1 = df_train_balanced.ix[:, col_test3[0:6]].values
X_train1 = pd.DataFrame(X_train1)

dTrain = xgb.DMatrix(X_train1, label=y_train)
trained_model = XGBClassifier()
# xgb_param = trained_model.get_xgb_params()

clf = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
clf.fit(X_train1, y_train)

# eval model
# print('Best number of trees = {}'.format(cvresult.shape[0]))
# clf.set_params(n_estimators=cvresult.shape[0])
print('Fit on the trainingsdata')
# clf.fit(X_train1, y_train, eval_metric='auc')
# print('Overall AUC:', roc_auc_score(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5)))
print('Predict the probabilities based on features in the test set')
# pred = clf.predict_proba(X_testUN, ntree_limit=cvresult.shape[0])[:, 1]
pred = cross_val_predict(clf, X_testUN, y_testUN, cv=5)
print('Overall AUC:', roc_auc_score(y_testUN, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testUN, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_train1.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********RFE Analysis********")
X_trainRFE = df_train_balanced.ix[:, col_test2[0:6]].values
X_trainRFE = pd.DataFrame(X_trainRFE)

dTrain = xgb.DMatrix(X_trainRFE, label=y_train)
trained_model = XGBClassifier()
# xgb_param = trained_model.get_xgb_params()

clf2 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)

# eval model
# print('Best number of trees = {}'.format(cvresult2.shape[0]))
# clf2.set_params(n_estimators=cvresult2.shape[0])
print('Fit on the trainingsdata')
clf2.fit(X_trainRFE, y_train)
print('Predict the probabilities based on features in the test set')
# pred = clf2.predict_proba(X_testRFE, ntree_limit=cvresult2.shape[0])[:, 1]
pred = cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5)
print('Overall AUC:', roc_auc_score(y_testRFE, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testRFE, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainRFE.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********XGBoost Feature Importance********")
X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

dTrain = xgb.DMatrix(X_trainXG, label=y_train)
trained_model = XGBClassifier()
# xgb_param = trained_model.get_xgb_params()

clf3 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)

# eval model
# print('Best number of trees = {}'.format(cvresult3.shape[0]))
# clf3.set_params(n_estimators=cvresult3.shape[0])
print('Fit on the trainingsdata')
clf3.fit(X_trainXG, y_train)
print('Predict the probabilities based on features in the test set')
# pred = clf3.predict_proba(X_testXG, ntree_limit=cvresult3.shape[0])[:, 1]
pred = cross_val_predict(clf3, X_testXG, y_testXG, cv=5)
# pred = cvp(clf, select_X_test, y_test, cv=10, n_jobs = 1)
print('Overall AUC:', roc_auc_score(y_testXG, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('XGBoost trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/xgBoost/xgBoostROC20TestYP')
# plt.show()

X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.ix[:, col_test1[0:6]].values
X_testXG = pd.DataFrame(X_testXG)
y_testXG = df.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.ix[:, col_test2[0:6]].values
X_testRFE = pd.DataFrame(X_testRFE)
y_testRFE = df.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.ix[:, col_test3[0:6]].values
X_testUN = pd.DataFrame(X_testUN)
y_testUN = df.iloc[:, 36]

print("********Univariate Analysis********")
# X_train1 = df_train_balanced.ix[:, col_test3[0:6]].values
# X_train1 = pd.DataFrame(X_train1)
#
# dTrain = xgb.DMatrix(X_train1, label=y_train)
# clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                     booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                     subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                     scale_pos_weight=1,
#                     base_score=0.25, random_state=7777, seed=None, missing=None)
# xgb_param = clf.get_xgb_params()
#
# # selection_model.fit(select_X_train, y_train)
# cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#
# # eval model
# print('Best number of trees = {}'.format(cvresult.shape[0]))
# clf.set_params(n_estimators=cvresult.shape[0])
# print('Fit on the trainingsdata')
# clf.fit(X_train1, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf.predict_proba(X_testUN, ntree_limit=cvresult.shape[0])[:, 1]
pred = cross_val_predict(clf, X_testUN, y_testUN, cv=5)
print('Overall AUC:', roc_auc_score(y_testUN, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testUN, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_train1.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********RFE Analysis********")
# X_trainRFE = df_train_balanced.ix[:, col_test2[0:6]].values
# X_trainRFE = pd.DataFrame(X_trainRFE)
#
# dTrain = xgb.DMatrix(X_trainRFE, label=y_train)
# clf2 = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                      scale_pos_weight=1,
#                      base_score=0.25, random_state=7777, seed=None, missing=None)
# xgb_param = clf2.get_xgb_params()
#
# # selection_model.fit(select_X_train, y_train)
# cvresult2 = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#
# # eval model
# print('Best number of trees = {}'.format(cvresult2.shape[0]))
# clf2.set_params(n_estimators=cvresult2.shape[0])
# print('Fit on the trainingsdata')
# clf2.fit(X_trainRFE, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf2.predict_proba(X_testRFE, ntree_limit=cvresult2.shape[0])[:, 1]
pred = cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5)
print('Overall AUC:', roc_auc_score(y_testRFE, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testRFE, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainRFE.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********XGBoost Feature Importance********")
# X_trainXG = df_train_balanced.ix[:, col_test1[0:6]].values
# X_trainXG = pd.DataFrame(X_trainXG)
#
# dTrain = xgb.DMatrix(X_trainXG, label=y_train)
# clf3 = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                      scale_pos_weight=1,
#                      base_score=0.25, random_state=7777, seed=None, missing=None)
# xgb_param = clf3.get_xgb_params()
#
# # selection_model.fit(select_X_train, y_train)
# cvresult3 = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#
# # eval model
# print('Best number of trees = {}'.format(cvresult3.shape[0]))
# clf3.set_params(n_estimators=cvresult3.shape[0])
# print('Fit on the trainingsdata')
# clf3.fit(X_trainXG, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf3.predict_proba(X_testXG, ntree_limit=cvresult3.shape[0])[:, 1]
pred = cross_val_predict(clf3, X_testXG, y_testXG, cv=5)
# pred = cvp(clf, select_X_test, y_test, cv=10, n_jobs = 1)
print('Overall AUC:', roc_auc_score(y_testXG, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('XGBoost trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/xgBoost/xgBoostROCFullTestYP')
# plt.show()

X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df_test.ix[:, col_test1[0:6]].values
X_testXG = pd.DataFrame(X_testXG)
y_testXG = df_test.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df_test.ix[:, col_test2[0:6]].values
X_testRFE = pd.DataFrame(X_testRFE)
y_testRFE = df_test.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df_test.ix[:, col_test3[0:6]].values
X_testUN = pd.DataFrame(X_testUN)
y_testUN = df_test.iloc[:, 36]

print("********Univariate Analysis********")
X_train1 = df_train.ix[:, col_test3[0:6]].values
X_train1 = pd.DataFrame(X_train1)

dTrain = xgb.DMatrix(X_train1, label=y_train)
trained_model = XGBClassifier()
# xgb_param = trained_model.get_xgb_params()

clf = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
clf.fit(X_train1, y_train)

# eval model
# print('Best number of trees = {}'.format(cvresult.shape[0]))
# clf.set_params(n_estimators=cvresult.shape[0])
print('Fit on the trainingsdata')
print('Predict the probabilities based on features in the test set')
# pred = clf.predict_proba(X_testUN, ntree_limit=cvresult.shape[0])[:, 1]
pred = cross_val_predict(clf, X_testUN, y_testUN, cv=5)
print('Overall AUC:', roc_auc_score(y_testUN, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testUN, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_train1.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********RFE Analysis********")
X_trainRFE = df_train.ix[:, col_test2[0:6]].values
X_trainRFE = pd.DataFrame(X_trainRFE)

dTrain = xgb.DMatrix(X_trainRFE, label=y_train)
trained_model = XGBClassifier()
# xgb_param = trained_model.get_xgb_params()

clf2 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
clf2.fit(X_trainRFE, y_train)
print(clf2.best_params_)

# eval model
# print('Best number of trees = {}'.format(cvresult2.shape[0]))
# clf2.set_params(n_estimators=cvresult2.shape[0])
print('Fit on the trainingsdata')
# clf2.fit(X_trainRFE, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf2.predict_proba(X_testRFE, ntree_limit=cvresult2.shape[0])[:, 1]
pred = cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5)
print('Overall AUC:', roc_auc_score(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5)))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testRFE, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainRFE.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********XGBoost Feature Importance********")
X_trainXG = df_train.ix[:, col_test1[0:6]].values
X_trainXG = pd.DataFrame(X_trainXG)

dTrain = xgb.DMatrix(X_trainXG, label=y_train)
trained_model = XGBClassifier()
# xgb_param = trained_model.get_xgb_params()

clf3 = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
clf3.fit(X_trainXG, y_train)
print(clf3.best_params_)

# eval model
# print('Best number of trees = {}'.format(cvresult3.shape[0]))
# clf3.set_params(n_estimators=cvresult3.shape[0])
print('Fit on the trainingsdata')
print('Predict the probabilities based on features in the test set')
# pred = clf3.predict_proba(X_testXG, ntree_limit=cvresult3.shape[0])[:, 1]
pred = cross_val_predict(clf3, X_testXG, y_testXG, cv=5)
# pred = cvp(clf, select_X_test, y_test, cv=10, n_jobs = 1)
print('Overall AUC:', roc_auc_score(y_testXG, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('XGBoost trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/xgBoost/xgBoostROC20TestUnbalYP')
# plt.show()

X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]

col_test1 = [35, 1, 9, 6, 0, 14]
# col_test1 = [35, 1, 6, 2, 14, 12]
X_testXG = df.ix[:, col_test1[0:6]].values
X_testXG = pd.DataFrame(X_testXG)
y_testXG = df.iloc[:, 36]

col_test2 = [35, 7, 12, 9, 8, 2]
# col_test2 = [35, 9, 14, 12, 23, 8]
X_testRFE = df.ix[:, col_test2[0:6]].values
X_testRFE = pd.DataFrame(X_testRFE)
y_testRFE = df.iloc[:, 36]

col_test3 = [29, 35, 16, 6, 1, 23]
# col_test3 = [35, 0, 27, 23, 6, 29]
X_testUN = df.ix[:, col_test3[0:6]].values
X_testUN = pd.DataFrame(X_testUN)
y_testUN = df.iloc[:, 36]

print("********Univariate Analysis********")
# X_train1 = df_train.ix[:, col_test3[0:6]].values
# X_train1 = pd.DataFrame(X_train1)
#
# dTrain = xgb.DMatrix(X_train1, label=y_train)
# clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                     booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                     subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                     scale_pos_weight=1,
#                     base_score=0.25, random_state=7777, seed=None, missing=None)
# xgb_param = clf.get_xgb_params()
#
# # selection_model.fit(select_X_train, y_train)
# cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#
# # eval model
# print('Best number of trees = {}'.format(cvresult.shape[0]))
# clf.set_params(n_estimators=cvresult.shape[0])
# print('Fit on the trainingsdata')
# clf.fit(X_train1, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf.predict_proba(X_testUN, ntree_limit=cvresult.shape[0])[:, 1]
pred = cross_val_predict(clf, X_testUN, y_testUN, cv=5)
print('Overall AUC:', roc_auc_score(y_testUN, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testUN, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testUN, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_train1.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********RFE Analysis********")
# X_trainRFE = df_train.ix[:, col_test2[0:6]].values
# X_trainRFE = pd.DataFrame(X_trainRFE)
#
# dTrain = xgb.DMatrix(X_trainRFE, label=y_train)
# clf2 = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                      scale_pos_weight=1,
#                      base_score=0.25, random_state=7777, seed=None, missing=None)
# xgb_param = clf2.get_xgb_params()
#
# # selection_model.fit(select_X_train, y_train)
# cvresult2 = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#
# # eval model
# print('Best number of trees = {}'.format(cvresult2.shape[0]))
# clf2.set_params(n_estimators=cvresult2.shape[0])
# print('Fit on the trainingsdata')
# clf2.fit(X_trainRFE, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf2.predict_proba(X_testRFE, ntree_limit=cvresult2.shape[0])[:, 1]
pred = cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5)
print('Overall AUC:', roc_auc_score(y_testRFE, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testRFE, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testRFE, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainRFE.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

print("********XGBoost Feature Importance********")
# X_trainXG = df_train.ix[:, col_test1[0:6]].values
# X_trainXG = pd.DataFrame(X_trainXG)
#
# dTrain = xgb.DMatrix(X_trainXG, label=y_train)
# clf3 = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
#                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
#                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
#                      scale_pos_weight=1,
#                      base_score=0.25, random_state=7777, seed=None, missing=None)
# xgb_param = clf3.get_xgb_params()
#
# # selection_model.fit(select_X_train, y_train)
# cvresult3 = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=5, stratified=True, folds=5, metrics=(),
#                   obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
#                   as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)
#
# # eval model
# print('Best number of trees = {}'.format(cvresult3.shape[0]))
# clf3.set_params(n_estimators=cvresult3.shape[0])
# print('Fit on the trainingsdata')
# clf3.fit(X_trainXG, y_train, eval_metric='auc')
print('Predict the probabilities based on features in the test set')
# pred = clf3.predict_proba(X_testXG, ntree_limit=cvresult3.shape[0])[:, 1]
pred = cross_val_predict(clf3, X_testXG, y_testXG, cv=5)
# pred = cvp(clf, select_X_test, y_test, cv=10, n_jobs = 1)
print('Overall AUC:', roc_auc_score(y_testXG, pred))

predictions = [round(value) for value in pred]
accuracy = accuracy_score(y_testXG, predictions)
from sklearn.metrics import confusion_matrix

confusion_matrix = confusion_matrix(y_testXG, predictions)
print(confusion_matrix)
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    X_trainXG.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

# ROC Curve
xf1_roc_auc = roc_auc_score(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5))
xf2_roc_auc = roc_auc_score(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5))
xf3_roc_auc = roc_auc_score(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5))
fpr1, tpr1, thresholds1 = roc_curve(y_testUN, cross_val_predict(clf, X_testUN, y_testUN, cv=5, method='predict_proba')[:, 1])
fpr2, tpr2, thresholds2 = roc_curve(y_testRFE, cross_val_predict(clf2, X_testRFE, y_testRFE, cv=5, method='predict_proba')[:, 1])
fpr3, tpr3, thresholds3 = roc_curve(y_testXG, cross_val_predict(clf3, X_testXG, y_testRFE, cv=5, method='predict_proba')[:, 1])
plt.figure()
plt.plot(fpr1, tpr1, linestyle='solid', color='green', label='Univariate (area = %0.2f)' % xf1_roc_auc)
plt.plot(fpr2, tpr2, linestyle='dashdot', color='purple', label='RFE (area = %0.2f)' % xf2_roc_auc)
plt.plot(fpr3, tpr3, linestyle='dotted', color='blue', label='XGBoost (area = %0.2f)' % xf3_roc_auc)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('XGBoost trained on YP')
plt.legend(loc="lower right")
plt.savefig('Pictures/xgBoost/xgBoostROCFullTestUnbalYP')
# plt.show()
