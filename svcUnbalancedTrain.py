from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import SelectKBest, RFE
from sklearn.feature_selection import chi2
import matplotlib.pyplot as plt
from matplotlib import pyplot
import numpy as np
import math as mt
from numpy import sort
import pandas as pd
from xgboost import XGBClassifier, plot_importance
from sklearn.metrics import accuracy_score
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import roc_auc_score

plt.rc("font", size=14)

filename = "data/featuresYPYS.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresYS.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYP.csv"
df2 = pd.read_csv(filename2, header=None)
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 4))
# print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
# print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
# print(len(df_train_unbalanced))

C_range = np.logspace(1, 3, 3)
gamma_range = np.logspace(1, 3, 3)
coef0 = [1, 5, 10]
degree = [3, 4]
class_weight = ['balanced']
kernel = ['poly']
decision_function_shape = ['ovo']
param_grid = dict(gamma=gamma_range, C=C_range, kernel=kernel, class_weight=class_weight,
                  decision_function_shape=decision_function_shape, coef0=coef0, degree=degree)

# param_grid = {
#     # 'C': [1, 10, 100, 1000, 10000, 100000, 1000000],
#     # 'C': [0.30, 0.33, 0.36, 0.39, 0.42, 0.45, 0.48, 0.51, 0.54, 0.57, 0.60, 0.63, 0.66, 0.69, 0.72],
#     'C': [100],
#     'kernel': ['rbf'],
#     'class_weight': ['balanced'],
#     # 'gamma': [10],
#     'decision_function_shape': ['ovo']
# }

col_test = [6, 29, 0, 2, 3, 5, 35]

X_trainSVM = df_train_unbalanced.loc[:, col_test[0:6]]
y_trainSVM = df_train_unbalanced.loc[:, 36]
X_testSVM = df.loc[:, col_test[0:6]]
y_testSVM = df.loc[:, 36]

print('Running model for selected features from published paper')

trained_model = SVC()

CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
CV_rfc.fit(X_trainSVM, y_trainSVM)
print(CV_rfc.best_params_)
# print(CV_rfc.cv_results_)
predictions = CV_rfc.predict(X_testSVM)
print('Overall AUC:', roc_auc_score(y_testSVM, CV_rfc.predict(X_testSVM)))

# Create Confusion Matrix
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

confusion_matrix = confusion_matrix(y_testSVM, predictions)
print(confusion_matrix)
# Compute precision, recall, F-measure and support
print(classification_report(y_testSVM, predictions))
specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
print('specificity : ', specificity)
sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
print('sensitivity : ', sensitivity)
print('fawadmetric : ', sensitivity * specificity * 100.0)

print('Finshed running model for selected features from published paper')

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

for a in range(6, 20):
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X_trainFeat, y_trainFeat)
    np.set_printoptions(precision=3)
    arr = fit.scores_

    features = fit.transform(X_trainFeat)

    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test = df.loc[:, col_test[0:a]].values
    y_test = df.loc[:, 36].values

    X_train1 = df_train_unbalanced.loc[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    trained_model = SVC()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions = CV_rfc.predict(X_test)
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(X_test)))

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    print("Finished running SVC for feature no. (Univariate FS) %d\n\n\n" % a)

for b in range(6, 20):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)

    featuresRFE = fitRFE.transform(X_trainFeat)

    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df.loc[:, col_test[0:b]].values
    y_testRFE = df.loc[:, 36].values

    X_trainRFE = df_train_unbalanced.loc[:, col_test[0:b]].values
    X_trainRFE = pd.DataFrame(X_trainRFE)

    # Create random forest classifier instance
    trained_model = SVC()

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(X_trainRFE, y_train)
    print(CV_rfc.best_params_)
    predictionsRFE = CV_rfc.predict(X_testRFE)
    print('Overall AUC:', roc_auc_score(y_testRFE, CV_rfc.predict(X_testRFE)))

    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import classification_report

    confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE, predictionsRFE))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    print("Finished running SVC for feature no. (RFE) %d\n\n\n" % b)

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]

# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_trainFeat, y_trainFeat)
# feature importance
print(model.feature_importances_)
# plot
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = SVC()
    # eval model
    select_X_test = selection.transform(X_test)

    CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred = CV_rfc.predict(select_X_test)
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(select_X_test)))

    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    print(classification_report(y_test, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]

# fit model no training data
model = RandomForestClassifier(n_estimators=500, criterion='gini', max_depth=None, min_samples_split=5,
                               min_samples_leaf=1,
                               min_weight_fraction_leaf=0.0, max_features='log2', max_leaf_nodes=None,
                               min_impurity_decrease=0.0,
                               min_impurity_split=None, bootstrap=True, oob_score=True, n_jobs=-1, random_state=888888,
                               verbose=0,
                               warm_start=False, class_weight='balanced')
model.fit(X_trainFeat, y_trainFeat)
# feature importance
print(model.feature_importances_)
# plot feature importance
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = SVC()
    # eval model
    select_X_test = selection.transform(X_test)

    CV_rfc = GridSearchCV(estimator=selection_model, param_grid=param_grid, scoring='roc_auc', cv=5)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred = CV_rfc.predict(select_X_test)
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(select_X_test)))

    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    print(classification_report(y_test, predictions))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
