import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.linear_model import LogisticRegression

plt.rc("font", size=14)

filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/featuresYT.csv"
df = pd.read_csv(filename, header=None)

df[35] = df[35].map({1: 1, -1: 0})

X = df.iloc[:, 0:34]
y = df.iloc[:, 35]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.80 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[35] == 1]
#print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[35] == 0]
#print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
#print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
#print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:34 ]
y_train = df_train_balanced.iloc[:, 35]

from sklearn.feature_selection import RFE
for b in range(4, 21):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_train, y_train)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X_train)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df_test.iloc[:, col_test[0:b]]
    y_testRFE = df_test.iloc[:, 35]
    # Logistic Regression Model Fitting
    X_trainRFE = df_train_balanced.iloc[:, col_test[0:b]]
    print('\n')
    logreg = sm.Logit(y_train,X_trainRFE)
    print(logreg.fit().summary())
    y_predRFE = logreg.fit().predict(X_testRFE)
    params = logreg.fit().params
    conf = logreg.fit().conf_int()
    conf['OR'] = params
    conf.columns = ['5%', '95%', 'OR']
    print(np.exp(conf))
    confusion_matrix = logreg.fit().pred_table()
    print(confusion_matrix)
    sensitivity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('Sensitivity : ', sensitivity)
    specificity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('Specificity : ', specificity)
    print('fawadMetric : ', sensitivity*specificity)
    print("Finished running logistic model for feature no. (RFE) %d\n\n\n" % b)

