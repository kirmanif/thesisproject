print(__doc__)

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.feature_selection import RFECV
from sklearn.svm import SVR
filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/features.csv"
df = pd.read_csv(filename, header=None)

df_train, df_validate, df_test = np.split(df.sample(frac=1), [int(.6*len(df)), int(.8*len(df))])
print(df_train.shape)
print(df_validate.shape)
print(df_test.shape)

#array = df_train.values
X = df_train.iloc[:,0:34]
y = df_train.iloc[:,35]

################################################################################
# Create the RFE object and compute a cross-validated score

estimator = SVR(kernel="linear")
selector = RFECV(estimator, step=1, cv=5)
selector = selector.fit(X, y)
print(selector.support_)
print('\n')
print(selector.ranking_)
print("Optimal number of features : %d" % selector.n_features_)


# Plot number of features VS. cross-validation scores
plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score (nb of correct classifications)")
plt.plot(range(1, len(selector.grid_scores_) + 1), selector.grid_scores_)
plt.show()
