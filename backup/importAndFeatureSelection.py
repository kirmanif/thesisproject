import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectFromModel

filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/featuresBalanced.csv"
df = pd.read_csv(filename, header=None)

print(df[:5])
print(df.shape)
print('\n')

df_train, df_validate, df_test = np.split(df.sample(frac=1), [int(.6*len(df)), int(.8*len(df))])
print(df_train.shape)
print(df_validate.shape)
print(df_test.shape)

#array = df_train.values
X = df_train.iloc[:,0:34]
y = df_train.iloc[:,35]

#print(X)
#print(y)
print('\n')

X=X.astype('float')
y=y.astype('int')

# feature selection Univariate
X_new = SelectKBest(chi2, k=7).fit_transform(X, y)
print(X_new)
print(X_new.shape)

# feature extraction RFE
model = LogisticRegression()
rfe = RFE(model, 7)
fit = rfe.fit(X, y)
print("Num Features: %d" % (fit.n_features_,))
print("Selected Features: %s" % (fit.support_,))
print("Feature Ranking: %s" % (fit.ranking_,))
print('\n')

from sklearn.decomposition import PCA

# feature extraction PCA
pca = PCA(n_components=7)
fit = pca.fit(X)
# summarize components
print("Explained Variance: %s" % (fit.explained_variance_ratio_,))
print('\n')
#print(fit.components_)

# feature extraction Feature Importance
model = ExtraTreesClassifier()
model.fit(X, y)
print(model.feature_importances_)
print('\n')

# feature extraction L1 based
lsvc = LinearSVC(C=1e5, penalty="l1", dual=False).fit(X, y)
model = SelectFromModel(lsvc, prefit=True)
X_new = model.transform(X)
print(X_new.shape)
