import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import LogisticRegressionCV
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc

plt.rc("font", size=14)
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/featuresYT.csv"
df = pd.read_csv(filename, header=None)

X = df.iloc[:, 0:34]
y = df.iloc[:, 35]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[35] == 1]
#print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[35] == -1]
#print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
#print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
#print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:34 ]
y_train = df_train_balanced.iloc[:, 35]

penalty = ['l1', 'l2']

C = [0.000001, 0.00001, 0.00003, 0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1, 0.03, 1, 3, 10, 30, 100, 300, 1000, 3000, 10000, 30000, 100000, 300000, 1000000]

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty)

# feature extraction RFE
from sklearn.feature_selection import RFE
for b in range(6, 14):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_train, y_train)
    print("Num Features: %d" % (fitRFE.n_features_,))
    #print("Selected Features: %s" % (fitRFE.support_,))
    #print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X_train)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df_test.iloc[:, col_test[0:b]]
    y_testRFE = df_test.iloc[:, 35]
    # Logistic Regression Model Fitting
    X_trainRFE = df_train_balanced.iloc[:, col_test[0:b]]
    logreg = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
    logreg.fit(X_trainRFE, y_train)
    y_predRFE = logreg.predict(X_testRFE)
    print('Best Penalty:', logreg.best_estimator_.get_params()['penalty'])
    print('Best C:', logreg.best_estimator_.get_params()['C'])
    print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(logreg.score(X_testRFE, y_testRFE)))
    y_pred = logreg.predict_proba(X_testRFE)[:,1]
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE, y_predRFE))
    sensitivity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('Sensitivity : ', sensitivity)
    specificity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('Specificity : ', specificity)
    print('fawadMetric : ', sensitivity*specificity)
    print('\n')

    lr_fpr, lr_tpr, _ = roc_curve(y_testRFE, y_pred)
    lr_roc_auc = auc(lr_fpr, lr_tpr)

    plt.figure()
    plt.plot(lr_fpr, lr_tpr, color='darkgreen',
             lw=2, label='Logistic Regression (area = %0.2f)' % lr_roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc="lower right")
    #plt.show()
