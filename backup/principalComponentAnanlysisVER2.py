import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

plt.rc("font", size=14)


filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/featuresYS.csv"
df = pd.read_csv(filename, header=None)

X = df.iloc[:, 0:36].values
y = df.iloc[:, 36].values

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
#print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
#print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
#print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
#print(len(df_train_balanced))
X_train = df_train_balanced.iloc[:, 0:36 ]
y_train = df_train_balanced.iloc[:, 36]

X_test = df_test.iloc[:, 0:36 ]
y_test = df_test.iloc[:, 36]

pca = PCA(n_components=1, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto', random_state=88888)
pca.fit(X_train)

X_pca = pca.transform(X)
print("original shape:   ", X.shape)
print("transformed shape:", X_pca.shape)

X_new = pca.inverse_transform(X_pca)
plt.scatter(X[:, 0], X[:, 1], alpha=0.2)
plt.scatter(X_new[:, 0], X_new[:, 1], alpha=0.8)
plt.axis('equal');
#plt.show()

pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto', random_state=88888)
projected = pca.fit_transform(X_train)
print(X_train.shape)
print(projected.shape)

plt.scatter(projected[:, 0], projected[:, 1],
            c=y_train, edgecolor='none', alpha=0.5,
            cmap=plt.cm.get_cmap('nipy_spectral', 10))
plt.xlabel('component 1')
plt.ylabel('component 2')
plt.colorbar();
plt.show()

pca = PCA().fit(X_train)
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance');
plt.show()

param_grid = {
    #'n_estimators': list(range(300,1000,200)),
    'n_estimators': [500],
    'max_features': ['log2'],
    'criterion': ['gini'],
    'min_samples_split': [5],
    'bootstrap': [True],
    'oob_score': [True],
    'n_jobs': [-1],
    'random_state': [888888],
    'warm_start': [False],
    'class_weight': ['balanced_subsample']

}

for components in range(1, 25):
    # Applying PCA
    from sklearn.decomposition import PCA
    selection_model = PCA(n_components=components, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto', random_state=88888)
    Xtrainpca = pca.fit_transform(X_train)
    Xtestpca = pca.transform(X_test)
    explained_variance = pca.explained_variance_ratio_

    # Train Model with PCA
    trained_model = RandomForestClassifier()
    classifier = GridSearchCV(estimator=trained_model, param_grid=param_grid, cv= 5)
    classifier.fit(Xtrainpca, y_train)

    # Predict Results from PCA Model
    y_pred = classifier.predict(Xtestpca)
    # eval model

    #y_pred = selection_model.predict(XtestXG)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_test, y_pred)
    sensitivity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    specificity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("n=%d, components=%d, Accuracy: %.2f%%, sensitivity: %.2f%%, specificity: %.2f%%, fawadMetric: %.2f%%" % (X_train.shape[1], components, accuracy*100.0, sensitivity*100.0, specificity*100.0, sensitivity*specificity*100.0))

