import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectFromModel


filename = "/home/fkirmani/Documents/CSCE798/Reproduce/cpp/features.csv"
df = pd.read_csv(filename, header=None)

df_train, df_validate, df_test = np.split(df.sample(frac=1), [int(.6*len(df)), int(.8*len(df))])
print(df_train.shape)
print(df_validate.shape)
print(df_test.shape)

#array = df_train.values
X = df_train.iloc[:,0:34]
y = df_train.iloc[:,35]

# feature extraction L1 based
lsvc = LinearSVC(C=1e5, penalty="l1", dual=False).fit(X, y)
model = SelectFromModel(lsvc, prefit=True)
X_new = model.transform(X)
print(X_new.shape)
