import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from matplotlib import pyplot
from numpy import sort
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest, RFE
from sklearn.feature_selection import chi2
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from xgboost import XGBClassifier
from xgboost import plot_importance

plt.rc("font", size=14)

filename = "data/featuresYS.csv"
df = pd.read_csv(filename, header=None)
df[36] = df[36].map({1: 1, -1: 0})
print(df.shape)

X = df.iloc[:, 0:36]
y = df.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == 0]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))
X_train = df_train.iloc[:, 0:36]
y_train = df_train.iloc[:, 36]
X_test = df_test.iloc[:, 0:36]
y_test = df_test.iloc[:, 36]

for a in range(6, 7):
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X, y)
    np.set_printoptions(precision=3)
    arr = fit.scores_
    # print(arr)
    features = fit.transform(X)
    # print(features[0:5,:])
    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_test = df_test.ix[:, col_test[0:a]].values
    X_test = pd.DataFrame(X_test)
    y_test = df_test.ix[:, 36].values

    X_train1 = df_train.ix[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    dTrain = xgb.DMatrix(X_train1, label=y_train)
    clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                        booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                        subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                        scale_pos_weight=len(df_train_balanced_MinusOne1.index) / len(df_train_balanced_One.index), base_score=0.25, random_state=7777, seed=None, missing=None)
    xgb_param = clf.get_xgb_params()

    # selection_model.fit(select_X_train, y_train)
    cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=10, stratified=True, folds=10, metrics=(),
                      obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
                      as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)

    # eval model
    print('Best number of trees = {}'.format(cvresult.shape[0]))
    clf.set_params(n_estimators=cvresult.shape[0])
    print('Fit on the trainingsdata')
    clf.fit(X_train1, y_train, eval_metric='auc')
    print('Overall AUC:', roc_auc_score(y_train, clf.predict(X_train1)))
    print('Predict the probabilities based on features in the test set')
    pred = clf.predict_proba(X_test, ntree_limit=cvresult.shape[0])[:, 1]

    predictions = [round(value) for value in pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        X_train1.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
    # ROC Curve
    xgb_roc_auc = roc_auc_score(y_test, clf.predict(X_test, ntree_limit=cvresult.shape[0]))
    fpr, tpr, thresholds = roc_curve(y_test, clf.predict_proba(X_test, ntree_limit=cvresult.shape[0])[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='XGBoost (area = %0.2f)' % xgb_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Pictures/XGBoost_ROC_20_test')
    # plt.show()

for b in range(6, 7):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X, y)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df_test.ix[:, col_test[0:b]].values
    X_testRFE = pd.DataFrame(X_testRFE)
    y_testRFE = df_test.ix[:, 36].values

    X_trainRFE = df_train.ix[:, col_test[0:b]].values
    X_trainRFE = pd.DataFrame(X_trainRFE)

    dTrain = xgb.DMatrix(X_trainRFE, label=y_train)
    clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                        booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                        subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                        scale_pos_weight=len(df_train_balanced_MinusOne1.index) / len(df_train_balanced_One.index), base_score=0.25, random_state=7777, seed=None, missing=None)
    xgb_param = clf.get_xgb_params()

    # selection_model.fit(select_X_train, y_train)
    cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=10, stratified=True, folds=10, metrics=(),
                      obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
                      as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)

    # eval model
    print('Best number of trees = {}'.format(cvresult.shape[0]))
    clf.set_params(n_estimators=cvresult.shape[0])
    print('Fit on the trainingsdata')
    clf.fit(X_trainRFE, y_train, eval_metric='auc')
    print('Overall AUC:', roc_auc_score(y_train, clf.predict(X_trainRFE)))
    print('Predict the probabilities based on features in the test set')
    pred = clf.predict_proba(X_testRFE, ntree_limit=cvresult.shape[0])[:, 1]

    predictions = [round(value) for value in pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        X_trainRFE.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
    # ROC Curve
    xgb_roc_auc = roc_auc_score(y_test, clf.predict(X_test, ntree_limit=cvresult.shape[0]))
    fpr, tpr, thresholds = roc_curve(y_test, clf.predict_proba(X_test, ntree_limit=cvresult.shape[0])[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % xgb_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
X_test = df_test.iloc[:, 0:36]
y_test = df_test.iloc[:, 36]

# xgb_model = xgb.XGBClassifier()
# parameters = {'nthread':[4], #when use hyperthread, xgboost may become slower
#              'objective':['binary:logistic'],
#              'booster':['gbtree', 'dart'],
#              'learning_rate': [0.08], #so called `eta` value
#              'max_depth': [3],
#              'min_child_weight': [3],
#              'base_score': [0.25],
#              'gamma': [0],
#              'max_delta_step': [1],
#              'n_estimators': [236], #number of trees, change it to 1000 for better results
#             'random_state': [7777]}


# model = GridSearchCV(xgb_model, parameters, n_jobs=5,
#                   scoring='roc_auc',
#                   verbose=2, refit=True)

# model.fit(X_train, y_train)

# trust your CV!
# best_parameters, score, _ = max(model.grid_scores_, key=lambda x: x[1])
# print('Raw AUC score:', score)
# for param_name in sorted(best_parameters.keys()):
#    print("%s: %r" % (param_name, best_parameters[param_name]))

# y_pred = model.predict_proba(X_test)[:,1]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X, y)
# make predictions for test data
# y_pred = model.predict_proba(X_test)[:, 1]
# predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot(feature importance)
plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
# print(thresholds)
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    dTrain = xgb.DMatrix(select_X_train, label=y_train)
    clf = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                        booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                        subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                        scale_pos_weight=len(df_train_balanced_MinusOne1.index) / len(df_train_balanced_One.index), base_score=0.25, random_state=7777, seed=None, missing=None)
    xgb_param = clf.get_xgb_params()

    # selection_model.fit(select_X_train, y_train)
    cvresult = xgb.cv(xgb_param, dTrain, num_boost_round=1000, nfold=10, stratified=True, folds=10, metrics=(),
                      obj=None, feval=None, maximize=True, early_stopping_rounds=None, fpreproc=None,
                      as_pandas=True, verbose_eval=None, show_stdv=True, seed=100001, callbacks=None, shuffle=True)

    # eval model
    select_X_test = selection.transform(X_test)
    print('Best number of trees = {}'.format(cvresult.shape[0]))
    clf.set_params(n_estimators=cvresult.shape[0])
    print('Fit on the trainingsdata')
    clf.fit(select_X_train, y_train, eval_metric='auc')
    print('Overall AUC:', roc_auc_score(y_train, clf.predict(select_X_train)))
    print('Predict the probabilities based on features in the test set')
    pred = clf.predict_proba(select_X_test, ntree_limit=cvresult.shape[0])[:, 1]
    # pred = cvp(clf, select_X_test, y_test, cv=10, n_jobs = 1)

    predictions = [round(value) for value in pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, predictions)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
    # ROC Curve
    xgb_roc_auc = roc_auc_score(y_test, clf.predict(select_X_test, ntree_limit=cvresult.shape[0]))
    fpr, tpr, thresholds = roc_curve(y_test, clf.predict_proba(select_X_test, ntree_limit=cvresult.shape[0])[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % xgb_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
