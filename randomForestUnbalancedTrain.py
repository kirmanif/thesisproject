from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectKBest, RFE
from sklearn.feature_selection import chi2
import matplotlib.pyplot as plt
import numpy as np
import math as mt
from numpy import sort
import pandas as pd
from xgboost import XGBClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import classification_report

plt.rc("font", size=14)

filename = "data/featuresYPYS_2D.csv"
dfFeat = pd.read_csv(filename, header=None)

df_OneFeat = dfFeat.loc[dfFeat[36] == 1]
df_MinusOne1Feat = dfFeat.loc[dfFeat[36] == -1]
df_train_OneFeat = df_OneFeat.sample(n=mt.floor(len(df_OneFeat.index) / 1))
df_train_MinusOneFeat = df_MinusOne1Feat.sample(
    n=mt.floor(len(df_train_OneFeat.index) * (len(df_MinusOne1Feat.index) / len(df_OneFeat.index))))
df_train_unbalancedFeat = df_train_OneFeat.append(df_train_MinusOneFeat)

filename1 = "data/featuresYS_2D.csv"
df1 = pd.read_csv(filename1, header=None)
filename2 = "data/featuresYP_2D.csv"
df2 = pd.read_csv(filename2, header=None)
print(df1.shape)
print(df2.shape)

test = 0

if test:
    df = df1
else:
    df = df2

df_One = df1.loc[df1[36] == 1]
df_MinusOne1 = df1.loc[df1[36] == -1]
df_train_One = df_One.sample(n=mt.floor(len(df_One.index) / 4))
print(len(df_train_One))
df_train_MinusOne = df_MinusOne1.sample(
    n=mt.floor(len(df_train_One.index) * (len(df_MinusOne1.index) / len(df_One.index))))
print(len(df_train_MinusOne))
df_train_unbalanced = df_train_One.append(df_train_MinusOne)
print(len(df_train_unbalanced))

X_trainFeat = df_train_unbalancedFeat.iloc[:, 0:36]
y_trainFeat = df_train_unbalancedFeat.iloc[:, 36]

X_trainFeat = X_trainFeat.astype('float')
y_trainFeat = y_trainFeat.astype('int')

X_train = df_train_unbalanced.iloc[:, 0:36]
y_train = df_train_unbalanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

skf = StratifiedKFold(n_splits=5)

n_estimators = [50]
# max_features = [1]
# class_weight = ['balanced']
criterion = ['entropy']
min_samples_split = [2]
min_samples_leaf = [10]
# max_depth = [2, 5, 10]
# max_leaf_nodes = [3, 7, 10, 15, 20, 30]
min_impurity_decrease = [0.0]
min_weight_fraction_leaf = [0]
n_jobs = [-1]
# oob_score = [True]
random_state = [3486875]
param_grid = dict(n_estimators=n_estimators, criterion=criterion,
                  min_samples_split=min_samples_split, min_samples_leaf=min_samples_leaf, n_jobs=n_jobs,
                  min_impurity_decrease=min_impurity_decrease, random_state=random_state,
                  min_weight_fraction_leaf=min_weight_fraction_leaf)

# param_grid = {
#    #'n_estimators': [500],
#    #'n_estimators': [500],
#    #'max_features': ['sqrt', 'log2'],
#    #'criterion': ['entropy'],
#    #'min_samples_split': [5],
#    #'bootstrap': [True],
#    #'oob_score': [True],
#    #'n_jobs': [-1],
#    #'random_state': [888888],
#    #'warm_start': [False],
#    #'class_weight': ['balanced']
#
# }

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X_trainFeat, y_trainFeat)
# feature importance
print(model.feature_importances_)
# plot
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = RandomForestClassifier()
    # selection_model.fit(select_X_train, y_train)
    # eval model
    select_X_test = selection.transform(X_test)

    CV_rfc = GridSearchCV(estimator=selection_model,
                          param_grid=dict(param_grid, max_features=[select_X_train.shape[1]]), scoring='recall', cv=skf)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred = CV_rfc.predict(select_X_test)

    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    print(classification_report(y_test, predictions))
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(select_X_test)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))

for a in range(6, 37):
    print("For feature no. (Univariate FS) %d" % a)

    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X_trainFeat, y_trainFeat)
    np.set_printoptions(precision=3)
    arr = fit.scores_
    # print(arr)
    features = fit.transform(X_trainFeat)
    # print(features[0:5,:])
    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    X_testUN = df.ix[:, col_test[0:a]].values
    y_testUN = df.ix[:, 36].values

    X_train1 = df_train_unbalanced.ix[:, col_test[0:a]].values
    X_train1 = pd.DataFrame(X_train1)

    # Create random forest classifier instance
    # trained_model = random_forest_classifier(X_train1, y_train)
    # trained_model = RandomForestClassifier(n_estimators=500, criterion='gini', max_depth=None, min_samples_split=5, min_samples_leaf=1,
    # min_weight_fraction_leaf=0.0, max_features='log2', max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None,
    # bootstrap=True, oob_score=True, n_jobs=-1, random_state=888888, verbose=0, warm_start=False, class_weight='balanced_subsample')

    trained_model = RandomForestClassifier()
    # trained_model.fit(X_train1, y_train)
    # print("Trained model :: ", trained_model)

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=dict(param_grid, max_features=[a]), scoring='roc_auc',
                          cv=skf)
    CV_rfc.fit(X_train1, y_train)
    print(CV_rfc.best_params_)
    predictions = CV_rfc.predict(X_testUN)
    # for i in xrange(0, 5):
    #    print("Actual outcome :: {} and Predicted outcome :: {}".format(list(y_test)[i], predictions[i]))

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testUN, predictions)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testUN, predictions))
    print('Overall AUC:', roc_auc_score(y_testUN, CV_rfc.predict(X_testUN)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    print("Finished running Random Forest for feature no. (Univariate FS) %d\n\n\n" % a)

for b in range(6, 37):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X_trainFeat, y_trainFeat)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X_trainFeat)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df.ix[:, col_test[0:b]].values
    y_testRFE = df.ix[:, 36].values

    X_trainRFE = df_train_unbalanced.ix[:, col_test[0:b]].values
    X_trainRFE = pd.DataFrame(X_trainRFE)

    # Create random forest classifier instance
    trained_model = RandomForestClassifier()
    # trained_model.fit(X_trainRFE, y_train)
    # print("Trained model :: ", trained_model)

    CV_rfc = GridSearchCV(estimator=trained_model, param_grid=dict(param_grid, max_features=[b]), scoring='roc_auc',
                          cv=skf)
    CV_rfc.fit(X_trainRFE, y_train)
    print(CV_rfc.best_params_)
    predictionsRFE = CV_rfc.predict(X_testRFE)

    # for i in xrange(0, 5):
    #    print("Actual outcome :: {} and Predicted outcome :: {}".format(list(y_test)[i], predictions[i]))

    # Create Confusion Matrix
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testRFE, predictionsRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE, predictionsRFE))
    print('Overall AUC:', roc_auc_score(y_testRFE, CV_rfc.predict(X_testRFE)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadmetric : ', sensitivity * specificity * 100.0)
    print("Finished running Random Forest for feature no. (RFE) %d\n\n\n" % b)

X_test = df.iloc[:, 0:36]
y_test = df.iloc[:, 36]
# fit model no training data
model = RandomForestClassifier(n_estimators=500, criterion='gini', max_depth=None, min_samples_split=5,
                               min_samples_leaf=1,
                               min_weight_fraction_leaf=0.0, max_features='log2', max_leaf_nodes=None,
                               min_impurity_decrease=0.0,
                               min_impurity_split=None, bootstrap=True, oob_score=True, n_jobs=-1, random_state=888888,
                               verbose=0,
                               warm_start=False, class_weight='balanced')
model.fit(X_trainFeat, y_trainFeat)
# feature importance
print(model.feature_importances_)
# plot
# pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    selection_model = RandomForestClassifier()
    # selection_model.fit(select_X_train, y_train)
    # eval model
    select_X_test = selection.transform(X_test)

    CV_rfc = GridSearchCV(estimator=selection_model,
                          param_grid=dict(param_grid, max_features=[select_X_train.shape[1]]), scoring='roc_auc', cv=skf)
    CV_rfc.fit(select_X_train, y_train)
    print(CV_rfc.best_params_)

    y_pred = CV_rfc.predict(select_X_test)

    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    print(classification_report(y_test, predictions))
    print('Overall AUC:', roc_auc_score(y_test, CV_rfc.predict(select_X_test)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
    thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
    sensitivity * specificity * 100.0))
