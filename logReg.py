from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
import numpy as np
from numpy import sort
import pandas as pd

from matplotlib import pyplot
from xgboost import XGBClassifier

from sklearn.metrics import accuracy_score

from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

plt.rc("font", size=14)

filename = "data/featuresYS.csv"
df = pd.read_csv(filename, header=None)
X = df.iloc[:, 0:36]
y = df.iloc[:, 36]

X = X.astype('float')
y = y.astype('int')

df_train, df_test = np.split(df.sample(frac=1), [int(.8 * len(df))])
print(df_train.shape)
print(df_test.shape)

df_train_balanced_One = df_train.loc[df_train[36] == 1]
# print(len(df_train_balanced_One))
df_train_balanced_MinusOne1 = df_train.loc[df_train[36] == -1]
# print(len(df_train_balanced_MinusOne1))
df_train_balanced_MinusOne = df_train_balanced_MinusOne1.sample(n=len(df_train_balanced_One.index))
# print(len(df_train_balanced_MinusOne))
df_train_balanced = df_train_balanced_One.append(df_train_balanced_MinusOne)
# print(len(df_train_balanced))

X_train = df_train_balanced.iloc[:, 0:36]
y_train = df_train_balanced.iloc[:, 36]

X_train = X_train.astype('float')
y_train = y_train.astype('int')

penalty = ['l1', 'l2']

# solver = ['newton-cg', 'lbfgs', 'liblinear']

# C = [0.000001, 0.00001, 0.00003, 0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1, 0.03, 1, 3, 10, 30, 100, 300, 1000, 3000, 10000, 30000, 100000, 300000, 1000000]
C = list(range(30, 40, 1))

class_weight = ['balanced']

# multi_class = ['ovr', 'multinomial']

# Create hyperparameter options
hyperparameters = dict(C=C, penalty=penalty, class_weight=class_weight)

for a in range(6, 10):
    print("For feature no. %d" % a)
    # feature selection Univariate
    X_new = SelectKBest(chi2, k=a)
    fit = X_new.fit(X, y)
    np.set_printoptions(precision=3)
    arr = fit.scores_
    # print(arr)
    features = fit.transform(X)
    # print(features[0:5,:])
    col_test = arr.argsort()[-a:][::-1]
    print(col_test)
    # print(df_test)
    X_test = df_test.iloc[:, col_test[0:a]]
    # print(X_test)
    y_test = df_test.iloc[:, 36]
    # Logistic Regression Model Fitting
    X_train1 = df_train_balanced.iloc[:, col_test[0:a]]

    clf = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
    clf.fit(X_train1, y_train)
    print(clf.best_params_)
    y_pred = clf.predict(X_test)
    # View best hyperparameters
    print('Best Penalty:', clf.best_estimator_.get_params()['penalty'])
    print('Best C:', clf.best_estimator_.get_params()['C'])
    print('Accuracy of logistic regression classifier on validate set (Univariate FS): {:.2f}'.format(
        clf.score(X_test, y_test)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_test, y_pred))
    print('Overall AUC : ', roc_auc_score(y_test, clf.predict(X_test)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, clf.predict(X_test))
    fpr, tpr, thresholds = roc_curve(y_test, clf.predict(X_test))
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finish running logistic model for feature no. (Univariate FS) %d\n\n\n" % a)

# feature extraction RFE
from sklearn.feature_selection import RFE

for b in range(6, 10):
    model = LogisticRegression()
    rfe = RFE(model, b)
    fitRFE = rfe.fit(X, y)
    print("Num Features: %d" % (fitRFE.n_features_,))
    print("Selected Features: %s" % (fitRFE.support_,))
    print("Feature Ranking: %s" % (fitRFE.ranking_,))
    arrRFE = fitRFE.get_support(indices=False)
    # print(arr)
    featuresRFE = fitRFE.transform(X)
    # print(features[0:5,:])
    col_test = arrRFE.argsort()[-b:][::-1]
    print(col_test)
    X_testRFE = df_test.iloc[:, col_test[0:b]]
    y_testRFE = df_test.iloc[:, 36]
    # Logistic Regression Model Fitting
    X_trainRFE = df_train_balanced.iloc[:, col_test[0:b]]
    print('\n')

    logreg = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
    logreg.fit(X_trainRFE, y_train)
    print(logreg.best_params_)
    y_predRFE = logreg.predict(X_testRFE)
    print('Best Penalty:', logreg.best_estimator_.get_params()['penalty'])
    print('Best C:', logreg.best_estimator_.get_params()['C'])
    print('Accuracy of logistic regression classifier on validate set (RFE): {:.2f}'.format(
        logreg.score(X_testRFE, y_testRFE)))

    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_testRFE, y_predRFE)
    print(confusion_matrix)
    # Compute precision, recall, F-measure and support
    print(classification_report(y_testRFE, y_predRFE))
    print('Overall AUC : ', roc_auc_score(y_testRFE, logreg.predict(X_testRFE)))
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    print('specificity : ', specificity)
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('sensitivity : ', sensitivity)
    print('fawadMetric : ', sensitivity * specificity)
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_testRFE, logreg.predict(X_testRFE))
    fpr, tpr, thresholds = roc_curve(y_testRFE, logreg.predict(X_testRFE))
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
    print("Finished running logistic model for feature no. (RFE) %d\n\n\n" % b)

X_test = df_test.iloc[:, 0:36]
y_test = df_test.iloc[:, 36]
# fit model no training data
model = XGBClassifier(max_depth=3, learning_rate=0.08, n_estimators=236, silent=True, objective='binary:logistic',
                      booster='gbtree', n_jobs=10, nthread=4, gamma=0, min_child_weight=3, max_delta_step=1,
                      subsample=1, colsample_bytree=1, colsample_bylevel=1, reg_alpha=0, reg_lambda=1,
                      scale_pos_weight=1, base_score=0.25, random_state=7777, seed=None, missing=None)
model.fit(X, y)
# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
# feature importance
print(model.feature_importances_)
# plot
pyplot.bar(range(len(model.feature_importances_)), model.feature_importances_)
# pyplot.show()
# plot feature importance
# plot_importance(model)
# pyplot.show()
# Fit model using each importance as a threshold
thresholds = sort(model.feature_importances_)
thresholds[::-1].sort()
for thresh in thresholds:
    # select features using threshold
    selection = SelectFromModel(model, threshold=thresh, prefit=True)
    select_X_train = selection.transform(X_train)
    # train model
    # selection_model = GridSearchCV(LogisticRegression(penalty='l2'), hyperparameters, verbose=0)
    selection_model = GridSearchCV(LogisticRegression(), hyperparameters, verbose=0, scoring='roc_auc', cv=10)
    selection_model.fit(select_X_train, y_train)
    print(selection_model.best_params_)

    print('Best Penalty:', selection_model.best_estimator_.get_params()['penalty'])
    print('Best C:', selection_model.best_estimator_.get_params()['C'])
    # eval model
    select_X_test = selection.transform(X_test)
    y_pred = selection_model.predict(select_X_test)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    from sklearn.metrics import confusion_matrix

    confusion_matrix = confusion_matrix(y_test, y_pred)
    print(confusion_matrix)
    specificity = confusion_matrix[0, 0] / (confusion_matrix[0, 0] + confusion_matrix[0, 1])
    sensitivity = confusion_matrix[1, 1] / (confusion_matrix[1, 0] + confusion_matrix[1, 1])
    print('Overall AUC : ', roc_auc_score(y_test, selection_model.predict(select_X_test)))
    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%, specificity: %.2f%%, sensitivity: %.2f%%, fawadMetric: %.2f%%" % (
        thresh, select_X_train.shape[1], accuracy * 100.0, specificity * 100.0, sensitivity * 100.0,
        sensitivity * specificity * 100.0))
    # ROC Curve
    logit_roc_auc = roc_auc_score(y_test, selection_model.predict(select_X_test))
    fpr, tpr, thresholds = roc_curve(y_test, selection_model.predict(select_X_test))
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    # plt.show()
